import { Application, Components, Fonts, Layers, linearicons } from "tripetto";
import { StudioComponent } from "./components/studio";
import { IStudioStyle } from "./components/studio/style";
import { Loader } from "./components/loader";
import { Locale } from "./helpers/locale";
import { Translations } from "./helpers/translations";
import { FONTS } from "@server/endpoints";

export function bootstrapStudio(style: IStudioStyle): void {
    const loader = Components.Loader.create(style.loader.color, style.loader.size);

    Loader.style = style;

    Application.run({
        /** First try to load the required external resources (fonts, locale data, etc.). */
        first: () => {
            Locale.load(() => Application.observe());
            Translations.load(() => Application.observe(), "en");

            return Fonts.load([linearicons(`${FONTS}linearicons/`), ...(style.fonts ? style.fonts(FONTS) : [])], (bSucceeded: boolean) => {
                if (!bSucceeded) {
                    console.error(`Not all fonts could be loaded!`);
                }

                Application.observe();
            });
        },

        /** Verify if we are ready to start the application. */
        when: () => Locale.isLoaded && Translations.isLoaded && Fonts.isReady,

        /** Start the application when we are ready for it. */
        do: () => Layers.Layer.app.component(new StudioComponent(style)).layer.hook("OnBeforeShow", "synchronous", () => loader.destroy()),

        /** Enable some general styles that allows the application to act more like a native one. */
        desktopClass: true,
    });
}
