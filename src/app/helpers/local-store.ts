import { Components, IDefinition } from "tripetto";
import { LOCAL_STORAGE_KEY, LOCAL_STORAGE_KEY_L10N, LOCAL_STORAGE_KEY_RUNNER, LOCAL_STORAGE_KEY_STYLES } from "@app/settings";
import { StudioComponent } from "@app/components/studio";
import { addTile } from "./workspace";
import { createDefinition } from "./definition";
import { TRunners } from "@server/entities/definitions/runners";
import { TL10n, TStyles } from "tripetto-runner-foundation";

type LocalStoreSubjects = "definition" | "styles" | "l10n" | "runner";

export async function appendLocalStoreToEmptyWorkspace(workspace: Components.Workspace): Promise<void> {
    const definition: IDefinition | undefined = getFromLocalStore();
    if (!definition) {
        return;
    }

    if (workspace.collections.count > 0) {
        return;
    }

    const runner = getValueFromLocalStore<TRunners>("runner") || "autoscroll";
    const styles = getValueFromLocalStore<TStyles>("styles");
    const l10n = getValueFromLocalStore<TL10n>("l10n");

    createDefinition(runner, definition, styles, l10n, StudioComponent.showApiError).then((id?: string) => {
        addTile(workspace.collections.append(), id!);
    });

    clearLocalStore();
}

export function addToLocalStore<T>(value: T, subject: LocalStoreSubjects = "definition"): void {
    setValueToLocalStore(subject, value);
}

export function getFromLocalStore<T>(subject: LocalStoreSubjects = "definition"): T | undefined {
    return getValueFromLocalStore(subject);
}

function setValueToLocalStore<T>(subject: LocalStoreSubjects, value: T): void {
    if (localStorage) {
        localStorage.setItem(getLocalStorageKey(subject), JSON.stringify(value));
    }
}

function getValueFromLocalStore<T>(subject: LocalStoreSubjects): T | undefined {
    const key = getLocalStorageKey(subject);
    return JSON.parse(localStorage.getItem(key) || "null") || undefined;
}

function getLocalStorageKey(subject: LocalStoreSubjects): string {
    switch (subject) {
        case "definition":
            return LOCAL_STORAGE_KEY;
        case "styles":
            return LOCAL_STORAGE_KEY_STYLES;
        case "l10n":
            return LOCAL_STORAGE_KEY_L10N;
        case "runner":
            return LOCAL_STORAGE_KEY_RUNNER;
    }
}

function clearLocalStore(): void {
    if (localStorage) {
        localStorage.removeItem(LOCAL_STORAGE_KEY);
        localStorage.removeItem(LOCAL_STORAGE_KEY_STYLES);
        localStorage.removeItem(LOCAL_STORAGE_KEY_L10N);
        localStorage.removeItem(LOCAL_STORAGE_KEY_RUNNER);
    }
}
