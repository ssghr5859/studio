import { Components, DOM, Debounce, IBuilderEditEvent, IDefinition, each, pgettext } from "tripetto";
import { Export, Import, Instance, L10n, Namespaces, TL10n, TL10nContract, TStyles, TStylesContract } from "tripetto-runner-foundation";
import { TRunnerPreviewData } from "tripetto-runner-react-hook";
import { BuilderComponent } from "@app/components/builder";
import { Loader } from "@app/components/loader";
import { TRunners } from "@server/entities/definitions/runners";
import { UMD } from "@server/endpoints";
import { HASH } from "@app/globals";
import { getLocale } from "@services/locales";
import { getTranslation } from "@services/translations";

interface IRunner {
    definition: IDefinition;
    styles: TStyles;
    l10n: TL10n;
    view: "preview" | "test";
    readonly instance: Instance | undefined;
    readonly restart: () => void;
    readonly doPreview: (data: TRunnerPreviewData) => void;
}

interface IRunnerList<T> {
    [runner: string]: T | undefined;
}

interface INamespace {
    readonly run: (props: {
        readonly element: HTMLElement;
        readonly definition?: IDefinition;
        readonly styles: TStyles;
        readonly l10n: TL10n;
        readonly view: "test" | "preview";
        readonly locale: (locale: "auto" | string) => Promise<L10n.ILocale | undefined>;
        readonly translations: (language: "auto" | string, context: string) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>;
        readonly onReady: () => void;
        readonly onTouch: () => void;
        readonly onEdit?: (type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => void;
    }) => Promise<IRunner>;
}

interface IContracts {
    readonly STYLES_CONTRACT: (pgettext: (context: string, id: string, ...args: string[]) => string) => TStylesContract;
    readonly L10N_CONTRACT: () => TL10nContract;
}

// These are the supported runners
declare const TripettoAutoscroll: INamespace | undefined;
declare const TripettoAutoscrollBuilder: IContracts | undefined;

declare const TripettoChat: INamespace | undefined;
declare const TripettoChatBuilder: IContracts | undefined;

declare const TripettoClassic: INamespace | undefined;
declare const TripettoClassicBuilder: IContracts | undefined;

const RUNNERS = {
    autoscroll: {
        get runner() {
            return (typeof TripettoAutoscroll !== "undefined" && TripettoAutoscroll) || undefined;
        },
        get contracts() {
            return (typeof TripettoAutoscrollBuilder !== "undefined" && TripettoAutoscrollBuilder) || undefined;
        },
    },
    chat: {
        get runner() {
            return (typeof TripettoChat !== "undefined" && TripettoChat) || undefined;
        },
        get contracts() {
            return (typeof TripettoChatBuilder !== "undefined" && TripettoChatBuilder) || undefined;
        },
    },
    classic: {
        get runner() {
            return (typeof TripettoClassic !== "undefined" && TripettoClassic) || undefined;
        },
        get contracts() {
            return (typeof TripettoClassicBuilder !== "undefined" && TripettoClassicBuilder) || undefined;
        },
    },
};

export class Runners {
    private readonly builder: BuilderComponent;
    private elements: IRunnerList<DOM.Element> = {};
    private runners: IRunnerList<IRunner> = {};
    private loaded: IRunnerList<boolean> = {};
    private ready: IRunnerList<boolean> = {};
    private current: TRunners;
    private enabled: boolean;
    private definition?: IDefinition;
    private definitionPending = false;
    private styles: TStyles;
    private stylesPending = false;
    private l10n: TL10n;
    private l10nPending = false;
    private data?: Export.IValues;
    private done?: () => void;
    private requestUpdate = new Debounce(() => this.update(), 150);
    view: "test" | "preview";
    onChangeRunner?: () => void;

    static builderBundleURL(runner: TRunners): string {
        return `${UMD}/${runner}/builder.js?${HASH}`;
    }

    static stylesContract(runner: TRunners): TStylesContract | undefined {
        const namespace = RUNNERS[runner];

        return namespace.contracts?.STYLES_CONTRACT(pgettext);
    }

    static l10nContract(runner: TRunners): TL10nContract | undefined {
        const namespace = RUNNERS[runner];

        return namespace.contracts?.L10N_CONTRACT();
    }

    constructor(builder: BuilderComponent, view: "test" | "preview") {
        this.builder = builder;
        this.current = builder.runner;
        this.styles = builder.styles;
        this.l10n = builder.l10n;
        this.enabled = builder.device !== "off";
        this.view = view;

        if (this.enabled) {
            this.load();
        }

        builder.ref.hook(
            "OnEdit",
            "framed",
            (event: IBuilderEditEvent) => {
                if (event.data.action === "end") {
                    each(
                        RUNNERS,
                        (namespace, name: TRunners) => {
                            const runner = this.runners[name];

                            if (runner && runner.doPreview) {
                                runner.doPreview(event.data);
                            }
                        },
                        {
                            keys: true,
                        }
                    );
                } else {
                    const runner = this.runners[this.current];

                    if (runner && runner.doPreview) {
                        runner.doPreview(event.data);
                    }
                }
            },
            this
        );
    }

    private load(): void {
        if (!RUNNERS[this.current].runner) {
            Loader.show();

            Namespaces.loadURL(`${UMD}/${this.current}/runner.js?${HASH}`, (succeeded) => {
                if (!succeeded) {
                    this.enabled = false;
                }

                this.init();

                Loader.hide();
            });
        } else {
            this.init();
        }
    }

    private init(): void {
        const runnerNamespace = RUNNERS[this.current].runner;
        const element = this.elements[this.current]?.HTMLElement;
        const definition = this.definition;

        if (this.done && !this.enabled) {
            this.done();

            return;
        }

        if (this.enabled && runnerNamespace && definition && element) {
            const controller = this.runners[this.current];
            const loaded = this.loaded[this.current];
            const ready = this.ready[this.current];

            if (!loaded && !controller) {
                this.loaded[this.current] = true;

                runnerNamespace
                    .run({
                        element,
                        definition,
                        styles: this.styles,
                        l10n: this.l10n,
                        view: this.view,
                        locale: (locale) => getLocale("", locale),
                        translations: (language, context) => getTranslation("", language, context),
                        onReady: () => {
                            this.ready[this.current] = true;

                            this.init();
                        },
                        onTouch: () => Components.Menu.close(true),
                        onEdit: (type, id?) => {
                            switch (type) {
                                case "prologue":
                                    this.builder.ref.edit("prologue");
                                    break;
                                case "block":
                                    if (id) {
                                        this.builder.ref.edit("node", id);
                                    }
                                    break;
                                case "epilogue":
                                    this.builder.ref.edit("epilogue", id);
                                    break;
                                case "styles":
                                    this.builder.stylesEditor();
                                    break;
                                case "l10n":
                                    this.builder.l10nEditor();
                                    break;
                            }
                        },
                    })
                    .then((c) => {
                        this.runners[this.current] = c;

                        this.init();
                    });
            } else if (ready && controller) {
                if (!this.done) {
                    const activeRunner = this.runners[this.current];

                    if (activeRunner) {
                        this.definitionPending = false;
                        this.stylesPending = false;
                        this.l10nPending = false;

                        activeRunner.definition = definition;
                        activeRunner.styles = this.styles;
                        activeRunner.l10n = this.l10n;
                        activeRunner.view = this.view;

                        if (this.data && activeRunner.instance) {
                            Import.values(activeRunner.instance, this.data);
                        }
                    }

                    each(
                        this.elements,
                        (e, r: TRunners) => {
                            e!.selectorSafe("active", r === this.current);
                        },
                        {
                            keys: true,
                        }
                    );
                } else {
                    this.done();

                    this.done = undefined;
                }
            }
        }
    }

    private update(): void {
        const runner = this.runners[this.current];

        if (runner) {
            if (this.definitionPending && this.definition) {
                runner.definition = this.definition;

                this.definitionPending = false;
            }

            if (this.stylesPending) {
                runner.styles = this.styles;

                this.stylesPending = false;
            }

            if (this.l10nPending) {
                runner.l10n = this.l10n;

                this.l10nPending = false;
            }
        }
    }

    render(renderHost: () => DOM.Element, done: () => void): void {
        this.done = done;

        each(
            RUNNERS,
            (namespace, runner: TRunners) => {
                const element = renderHost();

                this.elements[runner] = element;

                element.selector("active", runner === this.current);
            },
            {
                keys: true,
            }
        );

        this.init();
    }

    changeRunner(runner: TRunners): void {
        if (runner !== this.current) {
            const current = this.runners[this.current];

            if (this.onChangeRunner) {
                this.onChangeRunner();
            }

            if (current && current.instance) {
                this.data = Export.values(current.instance);
            }

            this.current = runner;

            this.load();
        }
    }

    setDefinition(definition: IDefinition): void {
        const runner = this.runners[this.current];

        this.definition = definition;

        if (runner) {
            this.definitionPending = true;

            if (this.enabled) {
                this.requestUpdate.invoke();
            }
        } else {
            this.init();
        }
    }

    setView(view: "test" | "preview"): void {
        if (view !== this.view) {
            const runner = this.runners[this.current];

            this.view = view;

            if (runner) {
                runner.view = view;
            }
        }
    }

    setState(enabled: boolean): void {
        if (enabled !== this.enabled) {
            this.enabled = enabled;

            if (enabled) {
                this.load();
            }
        }
    }

    setStyles(styles: TStyles): void {
        const runner = this.runners[this.current];

        this.styles = styles;
        this.stylesPending = true;

        if (this.enabled && runner) {
            this.requestUpdate.invoke();
        }
    }

    setL10n(l10n: TL10n) {
        const runner = this.runners[this.current];

        this.l10n = l10n;
        this.l10nPending = true;

        if (this.enabled && runner) {
            this.requestUpdate.invoke();
        }
    }

    restart(): void {
        const runner = this.runners[this.current];

        if (runner) {
            runner.restart();
        }
    }

    destroy(): void {
        this.builder.ref.unhookContext(this);
    }
}
