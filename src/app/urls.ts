import { URL } from "./globals";

export const HELP = "https://tripetto.com/help/studio/";
export const HELP_RUNNERS = "https://tripetto.com/help/articles/how-to-switch-between-form-faces/";
export const HELP_STYLES = "https://tripetto.com/help/articles/how-to-style-your-forms/";
export const HELP_L10N = "https://tripetto.com/help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/";
export const HELP_PREVIEW = "https://tripetto.com/help/articles/how-to-let-the-live-preview-work-for-you/";
export const HELP_SHARE = "https://tripetto.com/help/articles/how-to-run-your-form-from-the-studio/";
export const HELP_SHARE_LINK = "https://tripetto.com/help/articles/how-to-share-a-link-to-your-form-from-the-studio/";
export const HELP_SHARE_EMBED = "https://tripetto.com/help/articles/how-to-embed-a-form-from-the-studio-into-your-website/";
export const HELP_RESULTS = "https://tripetto.com/help/articles/how-to-get-your-results-from-the-studio/";
export const HELP_EXPORT = "https://tripetto.com/help/articles/troubleshooting-seeing-multiple-download-buttons/";
export const HELP_NOTIFICATION = "https://tripetto.com/help/articles/how-to-automate-email-notifications-for-each-new-result/";
export const HELP_SLACK = "https://tripetto.com/help/articles/how-to-automate-slack-notifications-for-each-new-result/";
export const HELP_PROLOGUE = "https://tripetto.com/help/articles/how-to-add-a-welcome-message/";
export const HELP_EPILOGUE = "https://tripetto.com/help/articles/how-to-add-one-or-multiple-closing-messages/";
export const HELP_BRANCHES = "https://tripetto.com/help/articles/discover-the-power-of-branches-for-your-logic/";
export const HELP_CULLING = "https://tripetto.com/help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/";
export const HELP_TERMINATORS = "https://tripetto.com/help/articles/learn-about-different-types-of-branch-endings-for-your-logic/";
export const HELP_CONDITIONS = "https://tripetto.com/help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/";
export const HELP_BLOCK_ERROR = "https://tripetto.com/help/articles/how-to-use-the-raise-error-block/";
export const HELP_BLOCK_HIDDEN_FIELD = "https://tripetto.com/help/articles/how-to-use-the-hidden-field-block/";
export const HELP_BLOCK_CALCULATOR = "https://tripetto.com/help/articles/how-to-use-the-calculator-block/";
export const HELP_BLOCK_MAILER = "https://tripetto.com/help/articles/how-to-use-the-send-email-block/";
export const HELP_BLOCK_MAILER_SENDER = "https://tripetto.com/help/articles/how-to-use-the-send-email-block/#sender";

export const HELP_WEBHOOK =
    "https://tripetto.com/help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/";
export const HELP_WEBHOOK_MAKE = "https://tripetto.com/help/articles/how-to-connect-to-other-services-with-make-formerly-integromat/";
export const HELP_WEBHOOK_ZAPIER = "https://tripetto.com/help/articles/how-to-connect-to-other-services-with-zapier/";
export const HELP_WEBHOOK_PABBLY = "https://tripetto.com/help/articles/how-to-connect-to-other-services-with-pabbly-connect/";
export const HELP_WEBHOOK_CUSTOM = "https://tripetto.com/help/articles/how-to-connect-to-other-services-with-custom-webhook/";
export const HELP_WEBHOOK_CUSTOM_RAW = "https://tripetto.com/help/articles/use-raw-response-data-in-webhooks/";

export const HELP_TRACKING = "https://tripetto.com/help/articles/how-to-automate-form-activity-tracking/";
export const HELP_TRACKING_GA = "https://tripetto.com/help/articles/how-to-track-form-activity-with-google-analytics/";
export const HELP_TRACKING_GTM = "https://tripetto.com/help/articles/how-to-track-form-activity-with-google-tag-manager/";
export const HELP_TRACKING_FB = "https://tripetto.com/help/articles/how-to-track-form-activity-with-facebook-pixel/";
export const HELP_TRACKING_CUSTOM = "https://tripetto.com/help/articles/how-to-track-form-activity-with-custom-tracking-code/";

export const SUPPORT = "https://tripetto.com/support/";
export const TERMS = `${URL}/terms`;
export const SOURCE = "https://gitlab.com/tripetto/studio";
export const DOCUMENTATION = "https://docs.tripetto.com";
export const NPM = {
    BUILDER: "https://www.npmjs.com/package/tripetto",
    SERVICES: "https://www.npmjs.com/package/tripetto-services",
    RUNNER: "https://www.npmjs.com/package/tripetto-runner-foundation",
    RUNNERS: {
        AUTOSCROLL: "https://www.npmjs.com/package/tripetto-runner-autoscroll",
        CHAT: "https://www.npmjs.com/package/tripetto-runner-chat",
        CLASSIC: "https://www.npmjs.com/package/tripetto-runner-classic",
    },
};
