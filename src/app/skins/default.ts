import { DEFAULT, DOM, Spacing, extendImmutable } from "tripetto";
import { IStudioStyle } from "@app/components/studio/style";
import { IWorkspaceStyle } from "@app/components/workspace/style";
import { IHeaderStyle } from "@app/components/header/style";
import { IDialogStyle } from "@app/components/dialog/style";
import { IResultsStyle } from "@app/components/results/style";
import { IPreviewStyle } from "@app/components/preview/style";
import { IRequestPremiumStyle } from "@app/components/request-premium/style";

const HEADER: IHeaderStyle = {
    height: 56,
    style: {
        orientation: "horizontal",
        appearance: {
            backgroundColor: "rgb(255,255,255)",
            borderBottom: "1px solid rgb(197,217,232)",
        },
        scrollable: true,
        scrollbars: {
            track: {
                size: 2,
                appearance: {
                    transition: "opacity .3s ease-out, visibility .3s ease-out",
                },
            },
            thumb: {
                size: 2,
                min: 48,
                appearance: {
                    backgroundColor: "rgba(0,0,0,0.2)",
                },
            },
        },
    },
    separator: {
        width: 1,
        height: 56,
        appearance: {
            backgroundColor: "rgb(216,229,238)",
        },
    },
    divider: {
        width: 1,
        height: 24,
        appearance: {
            backgroundColor: "rgb(216,229,238)",
            marginLeft: 16,
            marginTop: 16,
        },
    },
    spacer: {
        width: 16,
    },
    button: {
        withLabel: {
            width: "auto",
            height: 56,
            appearance: {
                fontFamily: "Roboto Medium",
                fontSize: 16,
                color: "rgb(44,64,90)",
                textAlign: "left",
                padding: "18px 22px",
                transition: "color .1s ease-in-out",
                "> i": {
                    float: "left",
                    fontSize: 20,
                    marginRight: 16,
                    color: "rgb(0,147,238)",
                },
                [DOM.Stylesheet.selector("small")]: {
                    fontFamily: "Roboto Regular",
                    fontSize: 12,
                    color: "rgb(0,147,238)",
                    padding: "21px 10px",
                    "> i": {
                        fontSize: 16,
                        marginRight: 8,
                        color: "rgb(168,198,223)",
                        position: "relative",
                        top: -2,
                    },
                },
                [DOM.Stylesheet.selector("customize")]: {
                    "> i": {
                        color: "#2bcdfd",
                    },
                },
                [DOM.Stylesheet.selector("share")]: {
                    "> i": {
                        color: "#feca2f",
                    },
                },
                [DOM.Stylesheet.selector("automate")]: {
                    "> i": {
                        color: "#30ca89",
                    },
                },
                [DOM.Stylesheet.selector("results")]: {
                    "> i": {
                        color: "#fa3a69",
                    },
                },
            },
            hover: {
                color: "rgb(0,147,238)",
                [DOM.Stylesheet.selector("small")]: {
                    "> i": {
                        color: "rgb(0,147,238)",
                    },
                },
                [DOM.Stylesheet.selector("customize")]: {
                    color: "#2bcdfd",
                },
                [DOM.Stylesheet.selector("share")]: {
                    color: "#feca2f",
                },
                [DOM.Stylesheet.selector("automate")]: {
                    color: "#30ca89",
                },
                [DOM.Stylesheet.selector("results")]: {
                    color: "#fa3a69",
                },
            },
            tap: {
                color: "rgb(0,147,238)",
                transition: "none",
            },
            selected: {
                [DOM.Stylesheet.selector("results")]: {
                    color: "#fa3a69",
                },
            },
            opened: {
                transition: "none",
                [DOM.Stylesheet.selector("customize")]: {
                    color: "#2bcdfd",
                },
            },
            menu: DEFAULT.menu,
        },
        withoutLabel: {
            width: 64,
            height: 56,
            appearance: {
                color: "rgb(168,198,223)",
                transition: "color .1s ease-in-out",
                "> i": {
                    float: "left",
                    fontSize: 20,
                    margin: "18px 23px",
                },

                [DOM.Stylesheet.selector("small")]: {
                    "> i": {
                        fontSize: 16,
                        margin: "20px 24px",
                    },
                },
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
                transition: "none",
            },
        },
    },
    toggle: {
        width: 40,
        height: 40,
        appearance: {
            color: "rgb(168,198,223)",
            marginTop: 8,
            border: "1px solid transparent",
            borderRadius: 8,
            transition: "color .1s ease-in-out, background-color .2s ease-in-out, border-color .2s ease-in-out",
            "> i": {
                float: "left",
                fontSize: 20,
                margin: "9px 9px",
            },
        },
        hover: {
            color: "rgb(0,147,238)",
        },
        tap: {
            color: "rgb(0,147,238)",
            transition: "none",
        },
        selected: {
            backgroundColor: "rgb(250,251,252)",
            borderColor: "rgb(197,217,232)",
            color: "rgb(0,147,238)",
        },
    },
    title: {
        width: "fill",
        height: 56,
        appearance: {
            textAlign: "left",
            padding: "18px 21px",
            "> i": {
                float: "left",
                fontSize: 20,
                marginRight: 14,
                color: "rgb(0,147,238)",
            },
            "> span:first-of-type": {
                fontFamily: "Roboto Medium",
                fontSize: 16,
                color: "rgb(44,64,90)",
                textAlign: "left",
                borderRight: "1px solid rgb(216,229,238)",
                padding: "2px 18px 3px 0",
            },
            "> span:last-child": {
                fontFamily: "Roboto Regular",
                fontSize: 14,
                color: "rgb(141,171,196)",
                transition: "color .1s ease-in-out",
                textAlign: "left",
                paddingLeft: 15,
            },
            [DOM.Stylesheet.selector("workspace")]: {
                "> i": {
                    color: "rgb(44,64,90)",
                },
            },
            [DOM.Stylesheet.selector("build")]: {
                "> i": {
                    color: "#855ffb",
                },
            },
            [DOM.Stylesheet.selector("compact")]: {
                "> span:first-of-type": {
                    display: "none",
                },
                "> span:last-child": {
                    paddingLeft: 4,
                },
            },
        },
        hover: {
            "> span:last-child": {
                color: "rgb(0,147,238)",
            },
        },
    },
    user: {
        width: 64,
        height: 56,
        appearance: {
            color: "rgb(168,198,223)",
            transition: "color .1s ease-in-out",
            "> i": {
                float: "left",
                fontSize: 20,
                margin: "18px 23px",
            },

            [DOM.Stylesheet.selector("authenticated")]: {
                color: "rgb(78,206,61)",
            },
        },
        hover: {
            color: "rgb(0,147,238)",
        },
        tap: {
            color: "rgb(0,147,238)",
            transition: "none",
        },
        opened: {
            color: "rgb(0,147,238)",
            transition: "none",
        },
        menu: DEFAULT.menu,
    },
};

const WORKSPACE: IWorkspaceStyle = {
    rulers: false,
    left: 48,
    right: 48 + 64,
    top: 64,
    bottom: 64,
    ensuing: {
        transition: "width .2s cubic-bezier(0.0, 0.0, 0.2, 1)",
    },
    buttons: {
        add: {
            appearance: {
                position: "absolute",
                right: 48,
                top: 64,
                width: 64,
                height: 64,
                borderRadius: 8,
                border: "1px solid rgb(197,217,232)",
                backgroundColor: "rgb(255,255,255)",
                color: "rgb(0,147,238)",
                fontSize: 28,
                padding: 18,
                transition: "background-color .2s, border-color .2s, color .2s, transform .2s, opacity .2s",
            },
            hover: {
                backgroundColor: "rgb(0,147,238)",
                borderColor: "rgb(0,147,238)",
                color: "rgb(255,255,255)",
            },
            tap: {
                backgroundColor: "rgb(0,147,238)",
                borderColor: "rgb(0,147,238)",
                color: "rgb(255,255,255)",
                transition: "none",
            },
        },
    },
    scrollbars: extendImmutable(DEFAULT.map.scrollbars, {
        track: {
            spacing: undefined,
        },
    }),
    collections: {
        spacing: 64,
        padding: 24,
        appearance: {
            backgroundColor: "rgb(250,251,252)",
            border: "1px solid rgb(197,217,232)",
            borderRadius: 8,
        },
        moving: {
            backgroundColor: "rgba(250,251,252,0.8)",
            boxShadow: `0 0 10px 3px rgba(0,106,194,0.9)`,
            cursor: "move",

            "> div:first-child": {
                backgroundColor: "rgba(255,255,255,0.8)",
                borderColor: "rgba(216,229,238,0.8)",
            },
        },
        ensuing: {
            transition:
                "transform .2s cubic-bezier(0.0, 0.0, 0.2, 1), width .2s cubic-bezier(0.0, 0.0, 0.2, 1), height .2s cubic-bezier(0.0, 0.0, 0.2, 1)",
        },
        header: {
            height: 64,
            appearance: {
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                height: 64,
                backgroundColor: "rgb(255,255,255)",
                borderBottom: "1px solid rgb(216,229,238)",
                borderRadius: "8px 8px 0 0",
            },
            label: {
                appearance: {
                    position: "absolute",
                    left: 0,
                    right: 64 * 2,
                    top: 0,
                    height: 64,
                    padding: "20px 0px 0px 24px",
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                    textOverflow: "ellipsis",
                    fontFamily: "Roboto Medium",
                    fontSize: 20,
                    color: "rgb(44,64,90)",
                },
                unnamed: {
                    color: "rgb(216,229,238)",
                },
            },
            buttons: {
                add: {
                    appearance: {
                        position: "absolute",
                        right: 0,
                        top: 0,
                        width: 64,
                        height: 64,
                        borderLeft: "1px solid rgb(216,229,238)",
                        borderRadius: "0 6px 0 0",
                        color: "rgb(0,147,238)",
                        fontSize: 28,
                        padding: 18,
                        transition: "background-color .2s, border-color .2s, color .2s",
                    },
                    hover: {
                        backgroundColor: "rgb(197,217,232)",
                        borderColor: "rgb(197,217,232)",
                        color: "rgb(255,255,255)",
                    },
                    tap: {
                        backgroundColor: "rgb(197,217,232)",
                        borderColor: "rgb(197,217,232)",
                        color: "rgb(255,255,255)",
                        transition: "none",
                    },
                    opened: {
                        backgroundColor: "rgb(197,217,232)",
                        borderColor: "rgb(197,217,232)",
                        color: "rgb(255,255,255)",
                        transition: "none",
                    },
                },
                context: {
                    appearance: {
                        position: "absolute",
                        right: 64,
                        top: 0,
                        width: 64,
                        height: 64,
                        color: "rgb(168,198,223)",
                        fontSize: 20,
                        padding: "22px 22px",
                        transition: "color .2s",
                    },
                    hover: {
                        color: "rgb(0,147,238)",
                    },
                    tap: {
                        color: "rgb(0,147,238)",
                        transition: "none",
                    },
                    opened: {
                        color: "rgb(0,147,238)",
                        transition: "none",
                    },
                },
            },
        },
        message: {
            appearance: {
                position: "absolute",
                top: 64,
                left: 32,
                right: 32,
                bottom: 0,
                opacity: 0,
                fontFamily: "Roboto Light",
                fontSize: 14,
                color: "rgb(141,171,196)",
                display: "flex",
                textAlign: "center",
                justifyContent: "center",
                alignItems: "center",
            },
            visible: {
                opacity: 1,
            },
        },
    },
    tiles: {
        width: 224,
        height: 224,
        spacing: 16,
        appearance: {
            borderRadius: 8,

            "> div:nth-child(1)": {
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                height: 112,
                backgroundColor: "rgb(216,229,238)",
                borderRadius: "8px 8px 0 0",
            },

            "> div:nth-child(2)": {
                position: "absolute",
                left: 0,
                right: 0,
                bottom: 0,
                height: 112,
                backgroundColor: "rgb(255,255,255)",
                border: "1px solid rgb(216,229,238)",
                borderTop: "none",
                borderRadius: "0 0 8px 8px",
            },

            [DOM.Stylesheet.selector("workspace")]: {
                "> div:nth-child(1)": {
                    backgroundColor: "rgb(44,64,90)",
                },
            },

            [DOM.Stylesheet.selector("form")]: {
                "> div:nth-child(1)": {
                    backgroundColor: "rgb(129,201,38)",
                },
            },
        },
        type: {
            position: "absolute",
            left: 24,
            right: 24,
            top: 80,
            fontFamily: "Roboto Regular",
            fontSize: 13,
            color: "rgb(255,255,255)",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
        },
        icon: {
            position: "absolute",
            left: 24,
            top: 38,
            width: 64,
            height: 64,
            fontSize: 32,
            color: "rgb(255,255,255)",
        },
        label: {
            appearance: {
                position: "absolute",
                left: 24,
                right: 24,
                bottom: 54,
                height: 39,
                fontFamily: "Roboto Regular",
                fontSize: 14,
                color: "rgb(63,83,110)",
                overflow: "hidden",
            },
            unnamed: {
                color: "rgb(216,229,238)",
            },
        },
        counter: {
            appearance: {
                position: "absolute",
                left: 24,
                bottom: 16,
                height: 24,
                border: "1px solid rgb(216,229,238)",
                borderRadius: 12,
                opacity: 0,
                transition: "opacity .3s",

                "> i": {
                    fontSize: 15,
                    color: "rgb(168,198,223)",
                    paddingLeft: 6,
                    position: "relative",
                    top: 3,
                },

                "> span": {
                    fontFamily: "Roboto Regular",
                    fontSize: 12,
                    color: "rgb(141,171,196)",
                    paddingLeft: 5,
                    paddingRight: 8,
                    position: "relative",
                    top: 1,
                },
            },
            visible: {
                opacity: 1,
            },
            hover: {
                borderColor: "rgb(0,147,238)",
                "> i": {
                    color: "rgb(0,147,238)",
                },
                "> span": {
                    color: "rgb(0,147,238)",
                },
            },
            tap: {
                borderColor: "rgb(0,147,238)",
                "> i": {
                    color: "rgb(0,147,238)",
                },
                "> span": {
                    color: "rgb(0,147,238)",
                },
            },
        },
        buttons: {
            context: {
                appearance: {
                    position: "absolute",
                    right: 0,
                    bottom: 0,
                    width: 56,
                    height: 56,
                    color: "rgb(168,198,223)",
                    fontSize: 16,
                    padding: "22px 22px",
                    transition: "color .2s",
                },
                hover: {
                    color: "rgb(0,147,238)",
                },
                tap: {
                    color: "rgb(0,147,238)",
                    transition: "none",
                },
                opened: {
                    color: "rgb(0,147,238)",
                    transition: "none",
                },
            },
        },
        moving: {
            backgroundColor: "rgba(250,251,252,0.8)",
            boxShadow: `0 0 10px 3px rgba(0,106,194,0.9)`,
            cursor: "move",

            "> div:nth-child(1)": {
                opacity: 0.8,
            },

            "> div:nth-child(2)": {
                backgroundColor: "rgba(255,255,255,0.8)",
            },
        },
        following: {
            backgroundColor: "rgba(250,251,252,0.7)",

            "> div:nth-child(1)": {
                opacity: 0.7,
            },

            "> div:nth-child(2)": {
                backgroundColor: "rgba(255,255,255,0.7)",
                borderColor: "rgba(216,229,238,0.7)",
            },
        },
        ensuing: {
            transition: "transform .2s cubic-bezier(0.0, 0.0, 0.2, 1)",
        },
    },
};

const DIALOG: IDialogStyle = {
    width: 480,
    appearance: {
        borderRadius: 8,
        backgroundColor: "rgb(255,255,255)",
    },
    overlay: {
        backgroundColor: "rgba(0,0,0,0.3)",
    },
    title: {
        fontFamily: "Roboto Regular",
        fontSize: 24,
        color: "rgb(44,64,90)",
        position: "absolute",
        left: 40,
        top: 40,
        width: 376,
        height: 40,
    },
    message: {
        fontFamily: "Roboto Regular",
        fontSize: 16,
        color: "rgb(141,171,196)",
        position: "absolute",
        left: 40,
        right: 40,
        top: 80,
        height: 72,
    },
    confirm: {
        height: 256,
        buttons: {
            container: {
                position: "absolute",
                left: 40,
                right: 40,
                bottom: 32,
                height: 48,
            },
            ok: {
                appearance: {
                    fontFamily: "Roboto Regular",
                    fontSize: 16,
                    color: "rgb(255,255,255)",
                    backgroundColor: "rgb(0,147,238)",
                    borderRadius: 4,
                    display: "inline-block",
                    padding: "16px 24px",
                },
                hover: {
                    backgroundColor: "rgb(2,133,215)",
                },
                tap: {
                    backgroundColor: "rgb(0,121,196)",
                },
            },
            okDanger: {
                appearance: {
                    fontFamily: "Roboto Regular",
                    fontSize: 16,
                    color: "rgb(255,255,255)",
                    backgroundColor: "rgb(255,21,31)",
                    borderRadius: 4,
                    display: "inline-block",
                    padding: "16px 24px",
                },
                hover: {
                    backgroundColor: "rgb(215,7,16)",
                },
                tap: {
                    backgroundColor: "rgb(194,4,12)",
                },
            },
            cancel: {
                appearance: {
                    fontFamily: "Roboto Regular",
                    fontSize: 16,
                    color: "rgb(44,64,90)",
                    display: "inline-block",
                    padding: "16px 24px",
                },
                hover: {
                    textDecoration: "underline",
                },
                tap: {
                    textDecoration: "underline",
                },
            },
        },
    },
    alert: {
        height: 184,
        close: {
            appearance: {
                width: 48,
                height: 48,
                position: "absolute",
                right: 0,
                top: 0,
                color: "rgb(168,198,223)",
                transition: "color .1s ease-in-out",
                "> i": {
                    float: "left",
                    fontSize: 20,
                    margin: 14,
                },
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
                transition: "none",
            },
        },
    },
};

const RESULTS: IResultsStyle = {
    list: {
        header: {
            height: 64 + 48,
            appearance: {
                borderBottom: "1px solid rgb(216,229,239)",
                backgroundColor: "rgba(255,255,255,.8)",
                backdropFilter: "blur(3px)",
            },
            column: {
                width: 64,

                appearance: {
                    fontFamily: "Roboto Regular",
                    fontSize: 13,
                    color: "rgb(141,171,196)",
                    padding: "80px 16px 0",
                },

                sortable: {
                    appearance: {
                        paddingLeft: 40,
                        background: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTguODU0IDE0LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMy4xNDYgMy4xNDZ2LTE3LjI5M2MwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41cy0wLjUgMC4yMjQtMC41IDAuNXYxNy4yOTNsLTMuMTQ2LTMuMTQ2Yy0wLjE5NS0wLjE5NS0wLjUxMi0wLjE5NS0wLjcwNyAwcy0wLjE5NSAwLjUxMiAwIDAuNzA3bDQgNGMwLjA5OCAwLjA5OCAwLjIyNiAwLjE0NiAwLjM1NCAwLjE0NnMwLjI1Ni0wLjA0OSAwLjM1NC0wLjE0Nmw0LTRjMC4xOTUtMC4xOTUgMC4xOTUtMC41MTIgMC0wLjcwN3oiIGZpbGw9InJnYigxNDEsMTcxLDE5NikiPjwvcGF0aD4KPHBhdGggZD0iTTE5Ljk2IDcuMzAzbC0zLTdjLTAuMDc5LTAuMTg0LTAuMjYtMC4zMDMtMC40Ni0wLjMwM3MtMC4zODEgMC4xMTktMC40NiAwLjMwM2wtMyA3Yy0wLjEwOSAwLjI1NCAwLjAwOSAwLjU0OCAwLjI2MyAwLjY1N3MwLjU0OC0wLjAwOSAwLjY1Ny0wLjI2M2wxLjE1Ni0yLjY5N2gyLjc2OWwxLjE1NiAyLjY5N2MwLjA4MSAwLjE5IDAuMjY2IDAuMzAzIDAuNDYgMC4zMDMgMC4wNjYgMCAwLjEzMy0wLjAxMyAwLjE5Ny0wLjA0MSAwLjI1NC0wLjEwOSAwLjM3MS0wLjQwMyAwLjI2My0wLjY1N3pNMTUuNTQ0IDRsMC45NTYtMi4yMzEgMC45NTYgMi4yMzFoLTEuOTEyeiIgZmlsbD0icmdiKDE0MSwxNzEsMTk2KSI+PC9wYXRoPgo8cGF0aCBkPSJNMTguNSAyMGgtNGMtMC4xNzggMC0wLjM0My0wLjA5NS0wLjQzMi0wLjI0OXMtMC4wOTAtMC4zNDQtMC4wMDItMC40OTlsMy41NzMtNi4yNTJoLTMuMTM4Yy0wLjI3NiAwLTAuNS0wLjIyNC0wLjUtMC41czAuMjI0LTAuNSAwLjUtMC41aDRjMC4xNzggMCAwLjM0MyAwLjA5NSAwLjQzMiAwLjI0OXMwLjA5MCAwLjM0NCAwLjAwMiAwLjQ5OWwtMy41NzMgNi4yNTJoMy4xMzhjMC4yNzYgMCAwLjUgMC4yMjQgMC41IDAuNXMtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMTQxLDE3MSwxOTYpIj48L3BhdGg+Cjwvc3ZnPgo=") no-repeat left 13px bottom 15px/16px 16px`,
                    },

                    ascending: {
                        color: "rgb(0,147,238)",
                        backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTguODU0IDE0LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMy4xNDYgMy4xNDZ2LTE3LjI5M2MwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41cy0wLjUgMC4yMjQtMC41IDAuNXYxNy4yOTNsLTMuMTQ2LTMuMTQ2Yy0wLjE5NS0wLjE5NS0wLjUxMi0wLjE5NS0wLjcwNyAwcy0wLjE5NSAwLjUxMiAwIDAuNzA3bDQgNGMwLjA5OCAwLjA5OCAwLjIyNiAwLjE0NiAwLjM1NCAwLjE0NnMwLjI1Ni0wLjA0OSAwLjM1NC0wLjE0Nmw0LTRjMC4xOTUtMC4xOTUgMC4xOTUtMC41MTIgMC0wLjcwN3oiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+CjxwYXRoIGQ9Ik0xOS45NiA3LjMwM2wtMy03Yy0wLjA3OS0wLjE4NC0wLjI2LTAuMzAzLTAuNDYtMC4zMDNzLTAuMzgxIDAuMTE5LTAuNDYgMC4zMDNsLTMgN2MtMC4xMDkgMC4yNTQgMC4wMDkgMC41NDggMC4yNjMgMC42NTdzMC41NDgtMC4wMDkgMC42NTctMC4yNjNsMS4xNTYtMi42OTdoMi43NjlsMS4xNTYgMi42OTdjMC4wODEgMC4xOSAwLjI2NiAwLjMwMyAwLjQ2IDAuMzAzIDAuMDY2IDAgMC4xMzMtMC4wMTMgMC4xOTctMC4wNDEgMC4yNTQtMC4xMDkgMC4zNzEtMC40MDMgMC4yNjMtMC42NTd6TTE1LjU0NCA0bDAuOTU2LTIuMjMxIDAuOTU2IDIuMjMxaC0xLjkxMnoiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+CjxwYXRoIGQ9Ik0xOC41IDIwaC00Yy0wLjE3OCAwLTAuMzQzLTAuMDk1LTAuNDMyLTAuMjQ5cy0wLjA5MC0wLjM0NC0wLjAwMi0wLjQ5OWwzLjU3My02LjI1MmgtMy4xMzhjLTAuMjc2IDAtMC41LTAuMjI0LTAuNS0wLjVzMC4yMjQtMC41IDAuNS0wLjVoNGMwLjE3OCAwIDAuMzQzIDAuMDk1IDAuNDMyIDAuMjQ5czAuMDkwIDAuMzQ0IDAuMDAyIDAuNDk5bC0zLjU3MyA2LjI1MmgzLjEzOGMwLjI3NiAwIDAuNSAwLjIyNCAwLjUgMC41cy0wLjIyNCAwLjUtMC41IDAuNXoiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+Cjwvc3ZnPgo=")`,
                    },

                    descending: {
                        color: "rgb(0,147,238)",
                        backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTguODU0IDE0LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMy4xNDYgMy4xNDZ2LTE3LjI5M2MwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41cy0wLjUgMC4yMjQtMC41IDAuNXYxNy4yOTNsLTMuMTQ2LTMuMTQ2Yy0wLjE5NS0wLjE5NS0wLjUxMi0wLjE5NS0wLjcwNyAwcy0wLjE5NSAwLjUxMiAwIDAuNzA3bDQgNGMwLjA5OCAwLjA5OCAwLjIyNiAwLjE0NiAwLjM1NCAwLjE0NnMwLjI1Ni0wLjA0OSAwLjM1NC0wLjE0Nmw0LTRjMC4xOTUtMC4xOTUgMC4xOTUtMC41MTIgMC0wLjcwN3oiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+CjxwYXRoIGQ9Ik0xOS45NiAxOS4zMDNsLTMtN2MtMC4wNzktMC4xODQtMC4yNi0wLjMwMy0wLjQ2LTAuMzAzcy0wLjM4MSAwLjExOS0wLjQ2IDAuMzAzbC0zIDdjLTAuMTA5IDAuMjU0IDAuMDA5IDAuNTQ4IDAuMjYzIDAuNjU3czAuNTQ4LTAuMDA5IDAuNjU3LTAuMjYzbDEuMTU2LTIuNjk3aDIuNzY5bDEuMTU2IDIuNjk3YzAuMDgxIDAuMTkgMC4yNjYgMC4zMDMgMC40NiAwLjMwMyAwLjA2NiAwIDAuMTMzLTAuMDEzIDAuMTk3LTAuMDQxIDAuMjU0LTAuMTA5IDAuMzcxLTAuNDAzIDAuMjYzLTAuNjU3ek0xNS41NDQgMTZsMC45NTYtMi4yMzEgMC45NTYgMi4yMzFoLTEuOTEyeiIgZmlsbD0icmdiKDAsMTQ3LDIzOCkiPjwvcGF0aD4KPHBhdGggZD0iTTE4LjUgOGgtNGMtMC4xNzggMC0wLjM0My0wLjA5NS0wLjQzMi0wLjI0OXMtMC4wOTAtMC4zNDQtMC4wMDItMC40OTlsMy41NzMtNi4yNTJoLTMuMTM4Yy0wLjI3NiAwLTAuNS0wLjIyNC0wLjUtMC41czAuMjI0LTAuNSAwLjUtMC41aDRjMC4xNzggMCAwLjM0MyAwLjA5NSAwLjQzMiAwLjI0OXMwLjA5MCAwLjM0NCAwLjAwMiAwLjQ5OWwtMy41NzMgNi4yNTJoMy4xMzhjMC4yNzYgMCAwLjUgMC4yMjQgMC41IDAuNXMtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMCwxNDcsMjM4KSI+PC9wYXRoPgo8L3N2Zz4K")`,
                    },
                },

                selector: {
                    width: 30,
                    appearance: {
                        background: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHJlY3QgeD0iMC41IiB5PSIwLjUiIHdpZHRoPSIxOSIgaGVpZ2h0PSIxOSIgcng9IjQiIHJ5PSI0IiBmaWxsPSIjZmZmZmZmIiBzdHJva2U9InJnYigyMTYsMjI5LDIzOCkiIHN0cm9rZS13aWR0aD0iMSIgLz4KPC9zdmc+Cg==") no-repeat 10px 77px`,
                        backgroundSize: "20px 20px",
                    },
                    checked: {
                        backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHJlY3QgeD0iMC41IiB5PSIwLjUiIHdpZHRoPSIxOSIgaGVpZ2h0PSIxOSIgcng9IjQiIHJ5PSI0IiBmaWxsPSJyZ2IoMCwxNDcsMjM4KSIgLz4KPHBhdGggZD0iTTguNSAxNC41Yy0wLjEyOCAwLTAuMjU2LTAuMDQ5LTAuMzU0LTAuMTQ2bC0zLTNjLTAuMTk1LTAuMTk1LTAuMTk1LTAuNTEyIDAtMC43MDdzMC41MTItMC4xOTUgMC43MDcgMGwyLjY0NiAyLjY0NiA2LjY0Ni02LjY0NmMwLjE5NS0wLjE5NSAwLjUxMi0wLjE5NSAwLjcwNyAwczAuMTk1IDAuNTEyIDAgMC43MDdsLTcgN2MtMC4wOTggMC4wOTgtMC4yMjYgMC4xNDYtMC4zNTQgMC4xNDZ6IiBmaWxsPSIjZmZmZmZmIj48L3BhdGg+Cjwvc3ZnPgo=")`,
                    },
                },

                types: {
                    datetime: {
                        sortable: {
                            appearance: {
                                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTguODU0IDE0LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMy4xNDYgMy4xNDZ2LTE3LjI5M2MwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41cy0wLjUgMC4yMjQtMC41IDAuNXYxNy4yOTNsLTMuMTQ2LTMuMTQ2Yy0wLjE5NS0wLjE5NS0wLjUxMi0wLjE5NS0wLjcwNyAwcy0wLjE5NSAwLjUxMiAwIDAuNzA3bDQgNGMwLjA5OCAwLjA5OCAwLjIyNiAwLjE0NiAwLjM1NCAwLjE0NnMwLjI1Ni0wLjA0OSAwLjM1NC0wLjE0Nmw0LTRjMC4xOTUtMC4xOTUgMC4xOTUtMC41MTIgMC0wLjcwN3oiIGZpbGw9InJnYigxNDEsMTcxLDE5NikiPjwvcGF0aD4KPHBhdGggZD0iTTE2LjUgMTVjLTAuMDc1IDAtMC4xNTEtMC4wMTctMC4yMjMtMC4wNTNsLTItMWMtMC4yMDctMC4xMDQtMC4zMTYtMC4zMzYtMC4yNjMtMC41NjJzMC4yNTUtMC4zODUgMC40ODctMC4zODVjMi40ODEgMCA0LjUtMi4wMTkgNC41LTQuNXMtMi4wMTktNC41LTQuNS00LjUtNC41IDIuMDE5LTQuNSA0LjVjMCAwLjI3Ni0wLjIyNCAwLjUtMC41IDAuNXMtMC41LTAuMjI0LTAuNS0wLjVjMC0zLjAzMyAyLjQ2Ny01LjUgNS41LTUuNXM1LjUgMi40NjcgNS41IDUuNWMwIDIuNDY3LTEuNjMyIDQuNTU5LTMuODczIDUuMjU0bDAuNTk3IDAuMjk4YzAuMjQ3IDAuMTIzIDAuMzQ3IDAuNDI0IDAuMjI0IDAuNjcxLTAuMDg4IDAuMTc1LTAuMjY0IDAuMjc2LTAuNDQ4IDAuMjc3eiIgZmlsbD0icmdiKDE0MSwxNzEsMTk2KSI+PC9wYXRoPgo8cGF0aCBkPSJNMTQuNSAxMGMtMC4wNzYgMC0wLjE1Mi0wLjAxNy0wLjIyMy0wLjA1M2wtMi0xYy0wLjI0Ny0wLjEyMy0wLjM0Ny0wLjQyNC0wLjIyNC0wLjY3MXMwLjQyNC0wLjM0NyAwLjY3MS0wLjIyNGwxLjY3OCAwLjgzOSAxLjc0NS0xLjc0NWMwLjE5NS0wLjE5NSAwLjUxMi0wLjE5NSAwLjcwNyAwczAuMTk1IDAuNTEyIDAgMC43MDdsLTIgMmMtMC4wOTYgMC4wOTYtMC4yMjQgMC4xNDYtMC4zNTQgMC4xNDZ6IiBmaWxsPSJyZ2IoMTQxLDE3MSwxOTYpIj48L3BhdGg+Cjwvc3ZnPg==")`,
                            },

                            ascending: {
                                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTguODU0IDE0LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMy4xNDYgMy4xNDZ2LTE3LjI5M2MwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41cy0wLjUgMC4yMjQtMC41IDAuNXYxNy4yOTNsLTMuMTQ2LTMuMTQ2Yy0wLjE5NS0wLjE5NS0wLjUxMi0wLjE5NS0wLjcwNyAwcy0wLjE5NSAwLjUxMiAwIDAuNzA3bDQgNGMwLjA5OCAwLjA5OCAwLjIyNiAwLjE0NiAwLjM1NCAwLjE0NnMwLjI1Ni0wLjA0OSAwLjM1NC0wLjE0Nmw0LTRjMC4xOTUtMC4xOTUgMC4xOTUtMC41MTIgMC0wLjcwN3oiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+CjxwYXRoIGQ9Ik0xNi41IDE1Yy0wLjA3NSAwLTAuMTUxLTAuMDE3LTAuMjIzLTAuMDUzbC0yLTFjLTAuMjA3LTAuMTA0LTAuMzE2LTAuMzM2LTAuMjYzLTAuNTYyczAuMjU1LTAuMzg1IDAuNDg3LTAuMzg1YzIuNDgxIDAgNC41LTIuMDE5IDQuNS00LjVzLTIuMDE5LTQuNS00LjUtNC41LTQuNSAyLjAxOS00LjUgNC41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjVzLTAuNS0wLjIyNC0wLjUtMC41YzAtMy4wMzMgMi40NjctNS41IDUuNS01LjVzNS41IDIuNDY3IDUuNSA1LjVjMCAyLjQ2Ny0xLjYzMiA0LjU1OS0zLjg3MyA1LjI1NGwwLjU5NyAwLjI5OGMwLjI0NyAwLjEyMyAwLjM0NyAwLjQyNCAwLjIyNCAwLjY3MS0wLjA4OCAwLjE3NS0wLjI2NCAwLjI3Ni0wLjQ0OCAwLjI3N3oiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+CjxwYXRoIGQ9Ik0xNC41IDEwYy0wLjA3NiAwLTAuMTUyLTAuMDE3LTAuMjIzLTAuMDUzbC0yLTFjLTAuMjQ3LTAuMTIzLTAuMzQ3LTAuNDI0LTAuMjI0LTAuNjcxczAuNDI0LTAuMzQ3IDAuNjcxLTAuMjI0bDEuNjc4IDAuODM5IDEuNzQ1LTEuNzQ1YzAuMTk1LTAuMTk1IDAuNTEyLTAuMTk1IDAuNzA3IDBzMC4xOTUgMC41MTIgMCAwLjcwN2wtMiAyYy0wLjA5NiAwLjA5Ni0wLjIyNCAwLjE0Ni0wLjM1NCAwLjE0NnoiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+Cjwvc3ZnPg==")`,
                            },

                            descending: {
                                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTguODU0IDE0LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMy4xNDYgMy4xNDZ2LTE3LjI5M2MwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41cy0wLjUgMC4yMjQtMC41IDAuNXYxNy4yOTNsLTMuMTQ2LTMuMTQ2Yy0wLjE5NS0wLjE5NS0wLjUxMi0wLjE5NS0wLjcwNyAwcy0wLjE5NSAwLjUxMiAwIDAuNzA3bDQgNGMwLjA5OCAwLjA5OCAwLjIyNiAwLjE0NiAwLjM1NCAwLjE0NnMwLjI1Ni0wLjA0OSAwLjM1NC0wLjE0Nmw0LTRjMC4xOTUtMC4xOTUgMC4xOTUtMC41MTIgMC0wLjcwN3oiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+CjxwYXRoIGQ9Ik0xMi41IDE1Yy0wLjE4MyAwLTAuMzYtMC4xMDEtMC40NDgtMC4yNzctMC4xMjMtMC4yNDctMC4wMjMtMC41NDcgMC4yMjQtMC42NzFsMC41OTctMC4yOThjLTIuMjQxLTAuNjk1LTMuODczLTIuNzg4LTMuODczLTUuMjU0IDAtMy4wMzMgMi40NjctNS41IDUuNS01LjVzNS41IDIuNDY3IDUuNSA1LjVjMCAwLjI3Ni0wLjIyNCAwLjUtMC41IDAuNXMtMC41LTAuMjI0LTAuNS0wLjVjMC0yLjQ4MS0yLjAxOS00LjUtNC41LTQuNXMtNC41IDIuMDE5LTQuNSA0LjVjMCAyLjQ4MSAyLjAxOSA0LjUgNC41IDQuNSAwLjIzMiAwIDAuNDMzIDAuMTU5IDAuNDg3IDAuMzg1cy0wLjA1NiAwLjQ1OC0wLjI2MyAwLjU2MmwtMiAxYy0wLjA3MiAwLjAzNi0wLjE0OCAwLjA1My0wLjIyMyAwLjA1M3oiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+CjxwYXRoIGQ9Ik0xNC41IDEwYy0wLjA3NiAwLTAuMTUyLTAuMDE3LTAuMjIzLTAuMDUzbC0yLTFjLTAuMjQ3LTAuMTIzLTAuMzQ3LTAuNDI0LTAuMjI0LTAuNjcxczAuNDI0LTAuMzQ3IDAuNjcxLTAuMjI0bDEuNjc4IDAuODM5IDEuNzQ1LTEuNzQ1YzAuMTk1LTAuMTk1IDAuNTEyLTAuMTk1IDAuNzA3IDBzMC4xOTUgMC41MTIgMCAwLjcwN2wtMiAyYy0wLjA5NiAwLjA5Ni0wLjIyNCAwLjE0Ni0wLjM1NCAwLjE0NnoiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+Cjwvc3ZnPg==")`,
                            },
                        },
                    },
                },
            },
        },

        rows: DEFAULT.forms.collection.list.rows,
        row: DEFAULT.forms.collection.list.row,
        cell: DEFAULT.forms.collection.list.cell,
        icon: DEFAULT.forms.collection.list.icon,
        button: DEFAULT.forms.collection.list.button,
        selector: {
            width: 40,
            appearance: {
                background: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHJlY3QgeD0iMC41IiB5PSIwLjUiIHdpZHRoPSIxOSIgaGVpZ2h0PSIxOSIgcng9IjQiIHJ5PSI0IiBmaWxsPSIjZmZmZmZmIiBzdHJva2U9InJnYigyMTYsMjI5LDIzOCkiIHN0cm9rZS13aWR0aD0iMSIgLz4KPC9zdmc+Cg==") no-repeat 10px 10px`,
                backgroundSize: "20px 20px",
            },
            checked: {
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHJlY3QgeD0iMC41IiB5PSIwLjUiIHdpZHRoPSIxOSIgaGVpZ2h0PSIxOSIgcng9IjQiIHJ5PSI0IiBmaWxsPSIjZmZmZmZmIiAvPgo8cGF0aCBkPSJNOC41IDE0LjVjLTAuMTI4IDAtMC4yNTYtMC4wNDktMC4zNTQtMC4xNDZsLTMtM2MtMC4xOTUtMC4xOTUtMC4xOTUtMC41MTIgMC0wLjcwN3MwLjUxMi0wLjE5NSAwLjcwNyAwbDIuNjQ2IDIuNjQ2IDYuNjQ2LTYuNjQ2YzAuMTk1LTAuMTk1IDAuNTEyLTAuMTk1IDAuNzA3IDBzMC4xOTUgMC41MTIgMCAwLjcwN2wtNyA3Yy0wLjA5OCAwLjA5OC0wLjIyNiAwLjE0Ni0wLjM1NCAwLjE0NnoiIGZpbGw9InJnYigwLDE0NywyMzgpIj48L3BhdGg+Cjwvc3ZnPgo=")`,
            },
        },
        emptyState: {
            appearance: {
                fontFamily: "Roboto Light",
                fontSize: 18,
                color: "rgb(141,171,196)",
                textAlign: "center",
                padding: 32,
            },
        },
        scrollbars: (() => {
            const scrollbars = extendImmutable(DEFAULT.forms.cards.scrollbars);

            scrollbars.track.spacing = new Spacing(0, 1, 1, 1);

            return scrollbars;
        })(),
    },

    buttons: {
        export: {
            height: 40,
            appearance: {
                fontFamily: "Roboto Light",
                fontSize: 15,
                color: "rgb(0,147,238)",
                textAlign: "left",
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE3LjUgMjBoLTE2Yy0wLjgyNyAwLTEuNS0wLjY3My0xLjUtMS41di0zYzAtMC42MjcgMC4yMi0xLjU1NyAwLjUtMi4xMThsMi4xMDYtNC4yMTFjMC4zMjgtMC42NTcgMS4xNi0xLjE3MSAxLjg5NC0xLjE3MSAwLjI3NiAwIDAuNSAwLjIyNCAwLjUgMC41cy0wLjIyNCAwLjUtMC41IDAuNWMtMC4zNjIgMC0wLjgzOCAwLjI5NS0xIDAuNjE4bC0yLjEwNiA0LjIxMWMtMC4yMSAwLjQyLTAuMzk0IDEuMjAxLTAuMzk0IDEuNjcxdjNjMCAwLjI3NiAwLjIyNCAwLjUgMC41IDAuNWgxNmMwLjI3NiAwIDAuNS0wLjIyNCAwLjUtMC41di0zYzAtMC40Ny0wLjE4NC0xLjI1MS0wLjM5NC0xLjY3MWwtMi4xMDYtNC4yMTFjLTAuMTYyLTAuMzIzLTAuNjM4LTAuNjE4LTEtMC42MTgtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXMwLjIyNC0wLjUgMC41LTAuNWMwLjczNCAwIDEuNTY2IDAuNTE0IDEuODk0IDEuMTcxbDIuMTA2IDQuMjExYzAuMjggMC41NjEgMC41IDEuNDkxIDAuNSAyLjExOHYzYzAgMC44MjctMC42NzMgMS41LTEuNSAxLjV6IiBmaWxsPSIjMDA5M0VFIj48L3BhdGg+CjxwYXRoIGQ9Ik0xNi41IDE4aC0xYy0wLjI3NiAwLTAuNS0wLjIyNC0wLjUtMC41czAuMjI0LTAuNSAwLjUtMC41aDFjMC4yNzYgMCAwLjUgMC4yMjQgMC41IDAuNXMtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSIjMDA5M0VFIj48L3BhdGg+CjxwYXRoIGQ9Ik0xNi41IDE2aC0xNGMtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXMwLjIyNC0wLjUgMC41LTAuNWgxNGMwLjI3NiAwIDAuNSAwLjIyNCAwLjUgMC41cy0wLjIyNCAwLjUtMC41IDAuNXoiIGZpbGw9IiMwMDkzRUUiPjwvcGF0aD4KPHBhdGggZD0iTTEyLjg1NCA3LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMi4xNDYgMi4xNDZ2LTguMjkzYzAtMC4yNzYtMC4yMjQtMC41LTAuNS0wLjVzLTAuNSAwLjIyNC0wLjUgMC41djguMjkzbC0yLjE0Ni0yLjE0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMHMtMC4xOTUgMC41MTIgMCAwLjcwN2wzIDNjMC4wOTggMC4wOTggMC4yMjYgMC4xNDYgMC4zNTQgMC4xNDZzMC4yNTYtMC4wNDkgMC4zNTQtMC4xNDZsMy0zYzAuMTk1LTAuMTk1IDAuMTk1LTAuNTEyIDAtMC43MDd6IiBmaWxsPSIjMDA5M0VFIj48L3BhdGg+Cjwvc3ZnPgo=")`,
                backgroundSize: "16px 16px",
                backgroundPosition: "left 16px center",
                backgroundColor: "rgb(255,255,255)",
                border: `1px solid rgb(197,217,232)`,
                borderRadius: 4,
                marginLeft: 8,
                marginRight: 8,
                padding: "10px 20px 0px 40px",
                transition: "background .1s ease-in-out, opacity .1s ease-in-out",
            },
            hover: {
                backgroundColor: "rgb(225,236,245)",
            },
            tap: {
                backgroundColor: "rgb(197,217,232)",
            },
            opened: {
                backgroundColor: "rgb(197,217,232)",
            },
        },
        refresh: {
            height: 40,
            width: 40,
            appearance: {
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE5Ljg1NCA4LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMS4xNDkgMS4xNDljLTAuMDUxLTIuMDYwLTAuODc4LTMuOTktMi4zNDEtNS40NTItMS41MTEtMS41MTEtMy41Mi0yLjM0My01LjY1Ny0yLjM0M3MtNC4xNDYgMC44MzItNS42NTcgMi4zNDMtMi4zNDMgMy41Mi0yLjM0MyA1LjY1NyAwLjgzMiA0LjE0NiAyLjM0MyA1LjY1NyAzLjUyIDIuMzQzIDUuNjU3IDIuMzQzYzIuOTc0IDAgNS42ODYtMS42MzUgNy4wNzctNC4yNjYgMC4xMjktMC4yNDQgMC4wMzYtMC41NDctMC4yMDgtMC42NzZzLTAuNTQ3LTAuMDM2LTAuNjc2IDAuMjA4Yy0xLjIxNyAyLjMwMy0zLjU5IDMuNzM0LTYuMTkzIDMuNzM0LTMuODYgMC03LTMuMTQtNy03czMuMTQtNyA3LTdjMy43ODkgMCA2Ljg4NSAzLjAyNyA2Ljk5NyA2Ljc4OWwtMS4xNDMtMS4xNDNjLTAuMTk1LTAuMTk1LTAuNTEyLTAuMTk1LTAuNzA3IDBzLTAuMTk1IDAuNTEyIDAgMC43MDdsMiAyYzAuMDk4IDAuMDk4IDAuMjI2IDAuMTQ2IDAuMzU0IDAuMTQ2czAuMjU2LTAuMDQ5IDAuMzU0LTAuMTQ2bDItMmMwLjE5NS0wLjE5NSAwLjE5NS0wLjUxMiAwLTAuNzA3eiIgZmlsbD0icmdiKDE2OCwxOTgsMjIzKSI+PC9wYXRoPgo8L3N2Zz4K")`,
                backgroundSize: "16px 16px",
                backgroundPosition: "center center",
                backgroundColor: "rgb(255,255,255)",
                border: `1px solid rgb(197,217,232)`,
                borderRadius: 4,
                marginLeft: 12,
            },
            hover: {
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE5Ljg1NCA4LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMS4xNDkgMS4xNDljLTAuMDUxLTIuMDYwLTAuODc4LTMuOTktMi4zNDEtNS40NTItMS41MTEtMS41MTEtMy41Mi0yLjM0My01LjY1Ny0yLjM0M3MtNC4xNDYgMC44MzItNS42NTcgMi4zNDMtMi4zNDMgMy41Mi0yLjM0MyA1LjY1NyAwLjgzMiA0LjE0NiAyLjM0MyA1LjY1NyAzLjUyIDIuMzQzIDUuNjU3IDIuMzQzYzIuOTc0IDAgNS42ODYtMS42MzUgNy4wNzctNC4yNjYgMC4xMjktMC4yNDQgMC4wMzYtMC41NDctMC4yMDgtMC42NzZzLTAuNTQ3LTAuMDM2LTAuNjc2IDAuMjA4Yy0xLjIxNyAyLjMwMy0zLjU5IDMuNzM0LTYuMTkzIDMuNzM0LTMuODYgMC03LTMuMTQtNy03czMuMTQtNyA3LTdjMy43ODkgMCA2Ljg4NSAzLjAyNyA2Ljk5NyA2Ljc4OWwtMS4xNDMtMS4xNDNjLTAuMTk1LTAuMTk1LTAuNTEyLTAuMTk1LTAuNzA3IDBzLTAuMTk1IDAuNTEyIDAgMC43MDdsMiAyYzAuMDk4IDAuMDk4IDAuMjI2IDAuMTQ2IDAuMzU0IDAuMTQ2czAuMjU2LTAuMDQ5IDAuMzU0LTAuMTQ2bDItMmMwLjE5NS0wLjE5NSAwLjE5NS0wLjUxMiAwLTAuNzA3eiIgZmlsbD0icmdiKDAsMTQ3LDIzOCkiPjwvcGF0aD4KPC9zdmc+Cg==")`,
                backgroundColor: "rgb(225,236,245)",
            },
            tap: {
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE5Ljg1NCA4LjY0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMGwtMS4xNDkgMS4xNDljLTAuMDUxLTIuMDYwLTAuODc4LTMuOTktMi4zNDEtNS40NTItMS41MTEtMS41MTEtMy41Mi0yLjM0My01LjY1Ny0yLjM0M3MtNC4xNDYgMC44MzItNS42NTcgMi4zNDMtMi4zNDMgMy41Mi0yLjM0MyA1LjY1NyAwLjgzMiA0LjE0NiAyLjM0MyA1LjY1NyAzLjUyIDIuMzQzIDUuNjU3IDIuMzQzYzIuOTc0IDAgNS42ODYtMS42MzUgNy4wNzctNC4yNjYgMC4xMjktMC4yNDQgMC4wMzYtMC41NDctMC4yMDgtMC42NzZzLTAuNTQ3LTAuMDM2LTAuNjc2IDAuMjA4Yy0xLjIxNyAyLjMwMy0zLjU5IDMuNzM0LTYuMTkzIDMuNzM0LTMuODYgMC03LTMuMTQtNy03czMuMTQtNyA3LTdjMy43ODkgMCA2Ljg4NSAzLjAyNyA2Ljk5NyA2Ljc4OWwtMS4xNDMtMS4xNDNjLTAuMTk1LTAuMTk1LTAuNTEyLTAuMTk1LTAuNzA3IDBzLTAuMTk1IDAuNTEyIDAgMC43MDdsMiAyYzAuMDk4IDAuMDk4IDAuMjI2IDAuMTQ2IDAuMzU0IDAuMTQ2czAuMjU2LTAuMDQ5IDAuMzU0LTAuMTQ2bDItMmMwLjE5NS0wLjE5NSAwLjE5NS0wLjUxMiAwLTAuNzA3eiIgZmlsbD0icmdiKDAsMTQ3LDIzOCkiPjwvcGF0aD4KPC9zdmc+Cg==")`,
                backgroundColor: "rgb(197,217,232)",
            },
        },
        help: {
            height: 40,
            width: 40,
            appearance: {
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTkuNSAxN2MtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtM2MwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41IDMuMDMzIDAgNS41LTIuNDY3IDUuNS01LjVzLTIuNDY3LTUuNS01LjUtNS41LTUuNSAyLjQ2Ny01LjUgNS41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjVzLTAuNS0wLjIyNC0wLjUtMC41YzAtMy41ODQgMi45MTYtNi41IDYuNS02LjVzNi41IDIuOTE2IDYuNSA2LjVjMCAzLjQxNi0yLjY0OSA2LjIyNS02IDYuNDgxdjIuNTE5YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMTY4LDE5OCwyMjMpIj48L3BhdGg+CjxwYXRoIGQ9Ik05LjUgMjBjLTAuMjc2IDAtMC41LTAuMjI0LTAuNS0wLjV2LTFjMC0wLjI3NiAwLjIyNC0wLjUgMC41LTAuNXMwLjUgMC4yMjQgMC41IDAuNXYxYzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMTY4LDE5OCwyMjMpIj48L3BhdGg+Cjwvc3ZnPgo=")`,
                backgroundSize: "16px 16px",
                backgroundPosition: "center center",
                backgroundColor: "rgb(255,255,255)",
                border: `1px solid rgb(197,217,232)`,
                borderRadius: 4,
                marginLeft: 12,
                marginRight: 4,
            },
            hover: {
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTkuNSAxN2MtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtM2MwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41IDMuMDMzIDAgNS41LTIuNDY3IDUuNS01LjVzLTIuNDY3LTUuNS01LjUtNS41LTUuNSAyLjQ2Ny01LjUgNS41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjVzLTAuNS0wLjIyNC0wLjUtMC41YzAtMy41ODQgMi45MTYtNi41IDYuNS02LjVzNi41IDIuOTE2IDYuNSA2LjVjMCAzLjQxNi0yLjY0OSA2LjIyNS02IDYuNDgxdjIuNTE5YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMCwxNDcsMjM4KSI+PC9wYXRoPgo8cGF0aCBkPSJNOS41IDIwYy0wLjI3NiAwLTAuNS0wLjIyNC0wLjUtMC41di0xYzAtMC4yNzYgMC4yMjQtMC41IDAuNS0wLjVzMC41IDAuMjI0IDAuNSAwLjV2MWMwIDAuMjc2LTAuMjI0IDAuNS0wLjUgMC41eiIgZmlsbD0icmdiKDAsMTQ3LDIzOCkiPjwvcGF0aD4KPC9zdmc+Cg==")`,
                backgroundColor: "rgb(225,236,245)",
            },
            tap: {
                backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTkuNSAxN2MtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtM2MwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41IDMuMDMzIDAgNS41LTIuNDY3IDUuNS01LjVzLTIuNDY3LTUuNS01LjUtNS41LTUuNSAyLjQ2Ny01LjUgNS41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjVzLTAuNS0wLjIyNC0wLjUtMC41YzAtMy41ODQgMi45MTYtNi41IDYuNS02LjVzNi41IDIuOTE2IDYuNSA2LjVjMCAzLjQxNi0yLjY0OSA2LjIyNS02IDYuNDgxdjIuNTE5YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMCwxNDcsMjM4KSI+PC9wYXRoPgo8cGF0aCBkPSJNOS41IDIwYy0wLjI3NiAwLTAuNS0wLjIyNC0wLjUtMC41di0xYzAtMC4yNzYgMC4yMjQtMC41IDAuNS0wLjVzMC41IDAuMjI0IDAuNSAwLjV2MWMwIDAuMjc2LTAuMjI0IDAuNS0wLjUgMC41eiIgZmlsbD0icmdiKDAsMTQ3LDIzOCkiPjwvcGF0aD4KPC9zdmc+Cg==")`,
                backgroundColor: "rgb(197,217,232)",
            },
        },
        delete: {
            withLabel: {
                height: 40,
                appearance: {
                    fontFamily: "Roboto Light",
                    fontSize: 15,
                    color: "rgb(255,255,255)",
                    textAlign: "left",
                    backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE1LjUgMmgtMy41di0wLjVjMC0wLjgyNy0wLjY3My0xLjUtMS41LTEuNWgtMmMtMC44MjcgMC0xLjUgMC42NzMtMS41IDEuNXYwLjVoLTMuNWMtMC44MjcgMC0xLjUgMC42NzMtMS41IDEuNXYxYzAgMC42NTIgMC40MTggMS4yMDggMSAxLjQxNHYxMi41ODZjMCAwLjgyNyAwLjY3MyAxLjUgMS41IDEuNWgxMGMwLjgyNyAwIDEuNS0wLjY3MyAxLjUtMS41di0xMi41ODZjMC41ODItMC4yMDYgMS0wLjc2MiAxLTEuNDE0di0xYzAtMC44MjctMC42NzMtMS41LTEuNS0xLjV6TTggMS41YzAtMC4yNzYgMC4yMjQtMC41IDAuNS0wLjVoMmMwLjI3NiAwIDAuNSAwLjIyNCAwLjUgMC41djAuNWgtM3YtMC41ek0xNC41IDE5aC0xMGMtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtMTIuNWgxMXYxMi41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6TTE2IDQuNWMwIDAuMjc2LTAuMjI0IDAuNS0wLjUgMC41aC0xMmMtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtMWMwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41aDEyYzAuMjc2IDAgMC41IDAuMjI0IDAuNSAwLjV2MXoiIGZpbGw9IiNmZmZmZmYiPjwvcGF0aD4KPHBhdGggZD0iTTEyLjUgN2MtMC4yNzYgMC0wLjUgMC4yMjQtMC41IDAuNXYxMGMwIDAuMjc2IDAuMjI0IDAuNSAwLjUgMC41czAuNS0wLjIyNCAwLjUtMC41di0xMGMwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41eiIgZmlsbD0iI2ZmZmZmZiI+PC9wYXRoPgo8cGF0aCBkPSJNOS41IDdjLTAuMjc2IDAtMC41IDAuMjI0LTAuNSAwLjV2MTBjMCAwLjI3NiAwLjIyNCAwLjUgMC41IDAuNXMwLjUtMC4yMjQgMC41LTAuNXYtMTBjMC0wLjI3Ni0wLjIyNC0wLjUtMC41LTAuNXoiIGZpbGw9IiNmZmZmZmYiPjwvcGF0aD4KPHBhdGggZD0iTTYuNSA3Yy0wLjI3NiAwLTAuNSAwLjIyNC0wLjUgMC41djEwYzAgMC4yNzYgMC4yMjQgMC41IDAuNSAwLjVzMC41LTAuMjI0IDAuNS0wLjV2LTEwYzAtMC4yNzYtMC4yMjQtMC41LTAuNS0wLjV6IiBmaWxsPSIjZmZmZmZmIj48L3BhdGg+Cjwvc3ZnPgo=")`,
                    backgroundSize: "16px 16px",
                    backgroundPosition: "left 16px center",
                    backgroundColor: "rgb(255,21,31)",
                    borderRadius: 4,
                    marginLeft: 4,
                    marginRight: 4,
                    padding: "11px 20px 0px 40px",
                    transition: "background .1s ease-in-out, opacity .1s ease-in-out",
                },
                hover: {
                    backgroundColor: "rgb(215,7,16)",
                },
                tap: {
                    backgroundColor: "rgb(194,4,12)",
                },
            },
            withoutLabel: {
                height: 40,
                width: 48,
                appearance: {
                    backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE1LjUgMmgtMy41di0wLjVjMC0wLjgyNy0wLjY3My0xLjUtMS41LTEuNWgtMmMtMC44MjcgMC0xLjUgMC42NzMtMS41IDEuNXYwLjVoLTMuNWMtMC44MjcgMC0xLjUgMC42NzMtMS41IDEuNXYxYzAgMC42NTIgMC40MTggMS4yMDggMSAxLjQxNHYxMi41ODZjMCAwLjgyNyAwLjY3MyAxLjUgMS41IDEuNWgxMGMwLjgyNyAwIDEuNS0wLjY3MyAxLjUtMS41di0xMi41ODZjMC41ODItMC4yMDYgMS0wLjc2MiAxLTEuNDE0di0xYzAtMC44MjctMC42NzMtMS41LTEuNS0xLjV6TTggMS41YzAtMC4yNzYgMC4yMjQtMC41IDAuNS0wLjVoMmMwLjI3NiAwIDAuNSAwLjIyNCAwLjUgMC41djAuNWgtM3YtMC41ek0xNC41IDE5aC0xMGMtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtMTIuNWgxMXYxMi41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6TTE2IDQuNWMwIDAuMjc2LTAuMjI0IDAuNS0wLjUgMC41aC0xMmMtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtMWMwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41aDEyYzAuMjc2IDAgMC41IDAuMjI0IDAuNSAwLjV2MXoiIGZpbGw9IiNmZmZmZmYiPjwvcGF0aD4KPHBhdGggZD0iTTEyLjUgN2MtMC4yNzYgMC0wLjUgMC4yMjQtMC41IDAuNXYxMGMwIDAuMjc2IDAuMjI0IDAuNSAwLjUgMC41czAuNS0wLjIyNCAwLjUtMC41di0xMGMwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41eiIgZmlsbD0iI2ZmZmZmZiI+PC9wYXRoPgo8cGF0aCBkPSJNOS41IDdjLTAuMjc2IDAtMC41IDAuMjI0LTAuNSAwLjV2MTBjMCAwLjI3NiAwLjIyNCAwLjUgMC41IDAuNXMwLjUtMC4yMjQgMC41LTAuNXYtMTBjMC0wLjI3Ni0wLjIyNC0wLjUtMC41LTAuNXoiIGZpbGw9IiNmZmZmZmYiPjwvcGF0aD4KPHBhdGggZD0iTTYuNSA3Yy0wLjI3NiAwLTAuNSAwLjIyNC0wLjUgMC41djEwYzAgMC4yNzYgMC4yMjQgMC41IDAuNSAwLjVzMC41LTAuMjI0IDAuNS0wLjV2LTEwYzAtMC4yNzYtMC4yMjQtMC41LTAuNS0wLjV6IiBmaWxsPSIjZmZmZmZmIj48L3BhdGg+Cjwvc3ZnPgo=")`,
                    backgroundSize: "16px 16px",
                    backgroundPosition: "center center",
                    backgroundColor: "rgb(255,21,31)",
                    borderRadius: 4,
                    marginLeft: 4,
                    marginRight: 4,
                    transition: "background .1s ease-in-out, opacity .1s ease-in-out",
                },
                hover: {
                    backgroundColor: "rgb(215,7,16)",
                },
                tap: {
                    backgroundColor: "rgb(194,4,12)",
                },
            },
        },
    },
};

const PREVIEW: IPreviewStyle = {
    appearance: {
        borderLeft: "1px solid rgb(197,217,232)",
        backgroundColor: "rgb(255,255,255)",
    },
    header: {
        height: 64,
        appearance: {
            position: "absolute",
            left: 0,
            right: 0,
            height: 64,
            top: 0,
            backgroundColor: "rgb(250,251,252)",
            borderBottom: "1px solid rgb(216,229,238)",
            padding: "12px 240px 0px 12px",
        },
        runner: {
            appearance: {
                display: "inline-block",
                position: "relative",
                height: 40,
                fontFamily: "Roboto Light",
                fontSize: 15,
                color: "rgb(0,147,238)",
                textAlign: "left",
                backgroundImage: `url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCI+PHBhdGggZD0iTTAgNmEuNS41IDAgMDEuODUzLS4zNTRsOC42NDYgOC42NDYgOC42NDYtOC42NDZhLjUuNSAwIDAxLjcwNy43MDdsLTkgOWEuNS41IDAgMDEtLjcwNyAwbC05LTlhLjQ5OC40OTggMCAwMS0uMTQ2LS4zNTR6IiBmaWxsPSIjMDA5M0VFIi8+PC9zdmc+")`,
                backgroundSize: "16px 16px",
                backgroundPosition: "right 16px center",
                backgroundColor: "rgb(255,255,255)",
                backgroundRepeat: "no-repeat",
                border: "1px solid rgb(197,217,232)",
                borderRadius: 4,
                padding: "10px 40px 0px 36px",
                transition: "background .1s ease-in-out, opacity .1s ease-in-out",
                /** Icon */
                "> span:first-child": {
                    position: "absolute",
                    fontSize: 20,
                    left: 8,
                    top: 8,
                },
                /** Label */
                "> span:last-child": {
                    whiteSpace: "nowrap",
                },
            },
            hover: {
                backgroundColor: "rgb(225,236,245)",
            },
            tap: {
                backgroundColor: "rgb(197,217,232)",
            },
            opened: {
                backgroundColor: "rgb(197,217,232)",
            },
        },
        styles: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 64 + 54 + 54,
                width: 54,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
            },
            disabled: {
                pointerEvents: "none",
                opacity: 0.3,
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
        restart: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 64 + 54,
                width: 54,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
            },
            disabled: {
                pointerEvents: "none",
                opacity: 0.3,
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
        help: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 64,
                width: 54,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
                borderRight: "1px solid rgb(216,229,238)",
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
        close: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 12,
                width: 40,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
    },
    buttons: {
        height: 48,
        appearance: {
            position: "absolute",
            left: 0,
            right: 0,
            height: 48,
            top: 64,
        },
        button: {
            appearance: {
                backgroundColor: "rgb(255,255,255)",
                borderBottom: "1px solid rgb(216,229,238)",
                position: "absolute",
                top: 0,
                height: 48,
                width: "50%",
                fontFamily: "Roboto Regular",
                fontSize: 13,
                color: "rgb(141,171,196)",
                textAlign: "center",
                paddingTop: 16,
                transition: "color .1s ease-in-out, border .2s ease-in-out",
                "&:first-child": {
                    left: 0,
                },
                "&:last-child": {
                    right: 0,
                    borderLeft: "1px solid rgb(216,229,238)",
                },
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
            selected: {
                color: "rgb(0,147,238)",
                borderBottom: "2px solid rgb(0,147,238)",
            },
        },
    },
    runner: {
        appearance: {
            position: "absolute",
            left: 0,
            right: 0,
            top: 64 + 48,
            bottom: 0,
            opacity: 0,
            pointerEvents: "none",
            transition: "opacity .2s ease-in-out",

            "> iframe": {
                pointerEvents: "none",
            },
        },
        active: {
            pointerEvents: "auto",
            opacity: 1,

            "> iframe": {
                pointerEvents: "auto",
            },
        },
    },
};

const REQUEST_PREMIUM: IRequestPremiumStyle = {
    appearance: {
        borderLeft: "1px solid rgb(197,217,232)",
        backgroundColor: "rgb(255,255,255)",
    },
    header: {
        height: 64,
        appearance: {
            position: "absolute",
            left: 0,
            right: 0,
            height: 64,
            top: 0,
            backgroundColor: "rgb(250,251,252)",
            borderBottom: "1px solid rgb(216,229,238)",
            fontFamily: "Roboto Light",
            fontSize: 18,
            color: "rgb(44,64,89)",
            padding: "21px 64px 0px 32px",
        },
        close: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 12,
                width: 40,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
    },
    form: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 64,
        bottom: 0,

        "> iframe": {
            width: "100%",
            height: "100%",
            border: "none",
        },
    },
};

// We take the default skin of the builder and extend it to a studio skin.
export const SKIN = extendImmutable<Partial<IStudioStyle>>(DEFAULT, {
    header: HEADER,
    workspace: WORKSPACE,
    preview: PREVIEW,
    dialog: DIALOG,
    results: RESULTS,
    requestPremium: REQUEST_PREMIUM,
    signIn: {
        terms: {
            fontFamily: "Roboto Regular",
            fontSize: 13,
            color: "rgb(141,171,196)",
            lineHeight: "20px",

            b: {
                fontFamily: "Roboto Medium",
                fontWeight: "normal",
            },

            a: {
                color: "rgb(0,147,238)",
                textDecoration: "none",
                cursor: "pointer",

                "&:hover": {
                    textDecoration: "underline",
                },
            },
        },
    },
}) as IStudioStyle;
