import { DOM, IBuilderStyle } from "tripetto";
import { IWorkspaceStyle } from "../workspace/style";
import { IPreviewStyle } from "../preview/style";
import { IHeaderStyle } from "../header/style";
import { IDialogStyle } from "../dialog/style";
import { IResultsStyle } from "../results/style";
import { IRequestPremiumStyle } from "../request-premium/style";

export interface IStudioStyle extends IBuilderStyle {
    header: IHeaderStyle;
    workspace: IWorkspaceStyle;
    preview: IPreviewStyle;
    dialog: IDialogStyle;
    results: IResultsStyle;
    requestPremium: IRequestPremiumStyle;
    signIn: {
        terms: DOM.IStyles;
    };
}
