import { DOM, Layers, cancelUITimeout, scheduleUITimeout } from "tripetto";
import { IStudioStyle } from "@app/components/studio/style";

export class Loader {
    private static layer: Layers.Layer | undefined;
    private static count = 0;
    private static handle: number;
    public static style: IStudioStyle;

    static show(): void {
        this.count++;

        if (this.count === 1) {
            cancelUITimeout(this.handle);

            this.handle = scheduleUITimeout(() => {
                if (!this.layer) {
                    this.layer =
                        this.layer ||
                        Layers.Layer.app.createModal((layer: Layers.Layer) => {
                            layer.context.create("div", (loader: DOM.Element) => {
                                loader.style({
                                    position: "absolute",
                                    left: 0,
                                    right: 0,
                                    top: 0,
                                    bottom: 0,
                                    height: this.style.loader.size,
                                    margin: "auto auto",
                                    textAlign: "center",
                                });

                                for (let nDot = 1; nDot <= 3; nDot++) {
                                    loader.create("div", (pDot: DOM.Element) => {
                                        pDot.style({
                                            width: this.style.loader.size,
                                            height: this.style.loader.size,
                                            backgroundColor: this.style.loader.color,
                                            border: "1px solid #fff",
                                            borderRadius: "100%",
                                            display: "inline-block",
                                            margin: "0 2px",
                                            animation: `${DOM.Stylesheet.app.animation({
                                                "0%, 80%, 100%": {
                                                    transform: "scale(0)",
                                                },
                                                "40%": {
                                                    transform: "scale(1)",
                                                },
                                            })} 1.4s ease-in-out ${nDot === 1 ? "-0.32s" : nDot === 2 ? "-0.16s" : "0s"} infinite both`,
                                        });
                                    });
                                }
                            });
                        }, Layers.Layer.configuration.global().overlay(false).animation(Layers.LayerAnimations.Zoom));
                }
            }, 300);
        }
    }

    static hide(): void {
        this.count--;

        if (this.count === 0) {
            this.handle = cancelUITimeout(this.handle);

            if (this.layer) {
                this.layer.close();

                this.layer = undefined;
            }
        }
    }
}
