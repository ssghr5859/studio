import { DOM, Layers, Touch, linearicon, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { Loader } from "@app/components/loader";
import { IStudioStyle } from "../studio/style";

export class RequestPremiumComponent extends Layers.LayerComponent {
    private readonly style: IStudioStyle;
    private readonly id: string;
    private readonly name: string;
    private readonly emailAddress: string;

    static open(studio: StudioComponent, id: string, name: string): RequestPremiumComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => panel.component(new RequestPremiumComponent(studio, id, name)),
            Layers.Layer.configuration.width(500).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(studio: StudioComponent, id: string, name: string) {
        super(
            Layers.Layer.configuration.style({
                layer: {
                    appearance: studio.style.requestPremium.appearance,
                },
                applyToChildren: false,
            })
        );

        this.style = studio.style;
        this.id = id;
        this.name = name || pgettext("app", "Unnamed form");
        this.emailAddress = studio.userAccount?.emailAddress || "";
    }

    onRender(): void {
        this.layer.context.create(
            "div",
            (header: DOM.Element) => {
                header.text = pgettext("app", "Buy upgrade");

                header.create(
                    "div",
                    (close: DOM.Element) => {
                        linearicon(0xe92a, close);

                        Touch.Tap.single(
                            close,
                            () => this.layer.parent.layer!.close(),
                            () => close.addSelectorSafe("tap"),
                            () => close.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(close, (pHover: Touch.IHoverEvent) => close.selectorSafe("hover", pHover.isHovered));
                    },
                    [
                        this.style.requestPremium.header.close.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: this.style.requestPremium.header.close.hover,
                            [DOM.Stylesheet.selector("tap")]: this.style.requestPremium.header.close.tap,
                        },
                    ]
                );
            },
            this.style.requestPremium.header.appearance
        );

        this.layer.wait();
        Loader.show();

        this.layer.context.create(
            "div",
            (form: DOM.Element) => {
                form.create("iframe", (frame: DOM.Element) => {
                    const fnLoaded = () => {
                        this.layer.done();

                        Loader.hide();
                    };

                    frame.on("load", () => {
                        setTimeout(fnLoaded, 500);
                    });

                    frame.attribute(
                        "src",
                        `https://tripetto.app/run/OSIY5UDCI1/?email=${encodeURIComponent(this.emailAddress)}&id=${encodeURIComponent(
                            this.id
                        )}&name=${encodeURIComponent(this.name)}`
                    );

                    setTimeout(fnLoaded, 4000);
                });
            },
            this.style.requestPremium.form
        );
    }
}
