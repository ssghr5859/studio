import { DOM } from "tripetto";

export interface IRequestPremiumStyle {
    appearance: DOM.IStyles;
    header: {
        height: number;
        appearance: DOM.IStyles;

        close: {
            appearance: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
        };
    };
    form: DOM.IStyles;
}
