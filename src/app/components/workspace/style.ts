/** Dependencies */
import { Components, DOM } from "tripetto";

export interface IWorkspaceStyle {
    left: number;
    top: number;
    right: number;
    bottom: number;
    rulers: boolean;
    appearance?: DOM.IStyles;
    ensuing?: DOM.IStyles;
    scrollbars?: Components.IScrollbarsStyle;
    buttons: {
        add: {
            appearance: DOM.IStyles;
            hover?: DOM.IStyles;
            tap?: DOM.IStyles;
        };
    };
    collections: {
        spacing: number;
        padding: number;
        appearance: DOM.IStyles;
        moving: DOM.IStyles;
        ensuing: DOM.IStyles;
        header: {
            height: number;
            appearance?: DOM.IStyles;
            label: {
                appearance: DOM.IStyles;
                unnamed?: DOM.IStyles;
            };
            buttons: {
                add: {
                    appearance: DOM.IStyles;
                    hover?: DOM.IStyles;
                    tap?: DOM.IStyles;
                    opened?: DOM.IStyles;
                };
                context: {
                    appearance: DOM.IStyles;
                    hover?: DOM.IStyles;
                    tap?: DOM.IStyles;
                    opened?: DOM.IStyles;
                };
            };
        };
        message: {
            appearance: DOM.IStyles;
            visible?: DOM.IStyles;
        };
    };

    tiles: {
        width: number;
        height: number;
        spacing: number;
        appearance: DOM.IStyles;
        moving: DOM.IStyles;
        following: DOM.IStyles;
        ensuing: DOM.IStyles;
        type: DOM.IStyles;
        icon: DOM.IStyles;
        counter: {
            appearance: DOM.IStyles;
            visible?: DOM.IStyles;
            hover?: DOM.IStyles;
            tap?: DOM.IStyles;
        };
        label: {
            appearance: DOM.IStyles;
            unnamed?: DOM.IStyles;
        };
        buttons: {
            context: {
                appearance: DOM.IStyles;
                hover?: DOM.IStyles;
                tap?: DOM.IStyles;
                opened?: DOM.IStyles;
            };
        };
    };
}
