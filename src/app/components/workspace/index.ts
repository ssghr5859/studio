import { StudioComponent } from "@app/components/studio";
import { Components, Debounce, LayerRenderer, Layers, Num, assert, map, pgettext, reduce } from "tripetto";
import { WorkspaceEditor } from "./workspace/editor";
import { CollectionLayout } from "./collection/layout";
import { TileLayout } from "./tile/layout";
import { IStudioStyle } from "@app/components/studio/style";
import { WorkspaceLayout } from "./workspace/layout";
import { IWorkspace } from "@server/entities/workspaces";
import { mutate, query } from "@app/helpers/api";
import * as ReadRootDataQuery from "@app/queries/workspaces/read-root.graphql";
import * as ReadDataQuery from "@app/queries/workspaces/read-data.graphql";
import * as UpdateQuery from "@app/queries/workspaces/update.graphql";
import { appendLocalStoreToEmptyWorkspace } from "@app/helpers/local-store";

export class WorkspaceComponent extends LayerRenderer<
    Components.Workspace,
    Components.Workspace | Components.Collection | Components.Tile,
    IStudioStyle
> {
    readonly studio: StudioComponent;
    readonly isRoot: boolean;
    whenReady?: () => void;
    whenClosed?: () => void;

    static async open(
        studio: StudioComponent,
        id: string | "root",
        onError?: (authenticated: boolean) => void
    ): Promise<WorkspaceComponent | undefined> {
        const result = await query<IWorkspace>({
            query: id === "root" ? ReadRootDataQuery : ReadDataQuery,
            variables: { id },
            onError: onError || StudioComponent.showApiError,
        });

        if (result) {
            return studio.openPanel((panel: Layers.Layer) => {
                let ref!: WorkspaceComponent;
                const workspacePanels = assert(panel.createChain(Layers.Layer.configuration.layout("hbca")));

                workspacePanels.createPanel(
                    (layer: Layers.Layer) => {
                        const workspace = Components.Workspace.create((ref = new WorkspaceComponent(layer, studio, !id || id === "root")));
                        const mutateWorkspace = new Debounce((name: string, workspaceData: Components.IWorkspace) => {
                            const tileCount = reduce(
                                map(
                                    workspaceData.collections,
                                    (collection: Components.ICollection) => (collection.tiles && collection.tiles.length) || 0
                                ),
                                (count: number, tiles: number) => count + tiles,
                                0
                            );

                            mutate({
                                query: UpdateQuery,
                                variables: {
                                    data: {
                                        id,
                                        tileCount,
                                        name,
                                        data: workspaceData,
                                    },
                                },
                                onError: StudioComponent.showApiError,
                            });
                        }, 1000);

                        workspace.onChanged = () => mutateWorkspace.invoke(workspace.name, workspace.save());

                        if (result.data) {
                            workspace.load(result.data);
                        }

                        if (id === "root") {
                            appendLocalStoreToEmptyWorkspace(workspace);
                        }
                    },
                    Layers.Layer.configuration
                        .scrollbars({
                            style: studio.style.workspace.scrollbars,
                            direction: "horizontal",
                            bounceHorizontal: "always",
                        })
                        .autoCloseChildPanels("tap")
                );

                return ref;
            });
        }

        return undefined;
    }

    static async openRoot(studio: StudioComponent): Promise<WorkspaceComponent | undefined> {
        return await this.open(studio, "root");
    }

    constructor(layer: Layers.Layer, studio: StudioComponent, isRoot: boolean) {
        super(
            [
                {
                    layout: WorkspaceLayout,
                    type: Components.Workspace,
                },
                {
                    layout: CollectionLayout,
                    type: Components.Collection,
                },
                {
                    layout: TileLayout,
                    type: Components.Tile,
                },
            ],
            layer,
            studio.style
        );

        this.studio = studio;
        this.isRoot = isRoot;

        layer.onReady = () => this.whenReady && this.whenReady();
        layer.onClose = () => this.whenClosed && this.whenClosed();
    }

    get panel(): Layers.Layer {
        return assert(assert(this.layer.chain).parent.layer);
    }

    get workspace(): Components.Workspace | undefined {
        return this.grid as Components.Workspace | undefined;
    }

    private recalculate(): void {
        const workspace = this.workspace;

        if (workspace) {
            let height = this.layer.height / workspace.zoom;

            workspace.rectangles.minHeight("self", height);
            workspace.rectangles.maxHeight("self", height);

            height -=
                workspace.rectangles.spacingVertical("children") +
                this.style.workspace.collections.header.height +
                this.style.workspace.collections.padding * 2 -
                this.style.workspace.tiles.spacing;

            workspace.threshold = Num.floor(height / (this.style.workspace.tiles.height + this.style.workspace.tiles.spacing));

            workspace.applyResize();
        }
    }

    onInit(): void {
        this.layer.hook("OnResize", "synchronous", () => this.recalculate());
    }

    onZoomEnd(zoom: number): void {
        this.recalculate();

        super.onZoomEnd(zoom);
    }

    render(): void {
        this.recalculate();

        if (this.workspace) {
            this.workspace.render(true);
        }
    }

    attach(context: {}, callback: (props: { name: string; hasName: boolean }) => void): void {
        const workspace = assert(this.workspace);
        const fnName = () => {
            callback({
                name: workspace.name || pgettext("studio", "Unnamed workspace"),
                hasName: workspace.name ? true : false,
            });
        };

        fnName();

        workspace.hook("OnRename", "synchronous", () => fnName(), context);
    }

    detach(context: {}): void {
        assert(this.workspace).unhookContext(context);
    }

    edit(): void {
        WorkspaceEditor.openPanel(this.layer, assert(this.workspace));
    }

    close(): void {
        this.panel.close();
    }
}
