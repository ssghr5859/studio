import { Components, DOM, Layers, MoveableLayout, Rectangles, TGridRectangles, Touch, linearicon } from "tripetto";
import { WorkspaceComponent } from "..";
import { IStudioStyle } from "@app/components/studio/style";

export class WorkspaceLayout extends MoveableLayout<Components.Workspace, Components.Workspace, IStudioStyle> {
    constructor(renderer: WorkspaceComponent, workspace: Components.Workspace, layer: Layers.Layer) {
        super(renderer, workspace, layer, false);
    }

    get configuration(): TGridRectangles {
        return new Rectangles([
            {
                name: "self",
            },
            {
                name: "children",
                spacingLeft: this.style.workspace.left,
                spacingTop: this.style.workspace.top,
                spacingRight: this.style.workspace.right,
                spacingBottom: this.style.workspace.bottom,
                align: ["self"],
            },
        ]);
    }

    get workspace(): Components.Workspace {
        return this.parent;
    }

    get isRoot(): boolean {
        return this.renderer instanceof WorkspaceComponent && this.renderer.isRoot;
    }

    draw(x: number, y: number, w: number, h: number, zoom: number, zIndex: number, redraw: boolean): void {
        if (!this.element) {
            return;
        }

        if (!redraw) {
            if (this.style.workspace.rulers) {
                this.enableRulers("rgba(255,0,0,0.5)");
            }

            this.element.style(
                [
                    this.style.workspace.appearance,
                    {
                        [DOM.Stylesheet.selector("ensuing")]: this.style.workspace.ensuing,
                    },
                ],
                this.stylesheet
            );

            this.element.create(
                "div",
                (add: DOM.Element) => {
                    linearicon(0xe936, add);

                    Touch.Tap.single(
                        add,
                        () => this.workspace.collections.append(),
                        () => add.addSelectorSafe("tap"),
                        () => add.removeSelectorSafe("tap")
                    );

                    Touch.Hover.pointer(add, (pHover: Touch.IHoverEvent) => add.selectorSafe("hover", pHover.isHovered));
                },
                [
                    this.style.workspace.buttons.add.appearance,
                    {
                        [DOM.Stylesheet.selector("hover")]: this.style.workspace.buttons.add.hover,
                        [DOM.Stylesheet.selector("tap")]: this.style.workspace.buttons.add.tap,
                    },
                ],
                this.stylesheet
            );
        }
    }
}
