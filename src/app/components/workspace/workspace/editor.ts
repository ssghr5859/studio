import { Components, Forms, Layers, MoveableLayout, assert, pgettext } from "tripetto";
import { IStudioStyle } from "@app/components/studio/style";

export class WorkspaceEditor extends Components.Controller<Components.Workspace> {
    static open(layer: Layers.Layer, workspace: Components.Workspace): WorkspaceEditor {
        return new WorkspaceEditor(layer, workspace);
    }

    static openPanel(parent: Layers.Layer, workspace: Components.Workspace): Layers.Layer {
        const pStyle =
            workspace.layout && (workspace.layout as MoveableLayout<Components.Workspace, Components.Workspace, IStudioStyle>).style;

        return assert(
            parent.createPanel(
                (layer: Layers.Layer) => {
                    this.open(layer, workspace);
                },
                Layers.Layer.configuration
                    .width(pStyle ? pStyle.forms.width.small : 0)
                    .animation(Layers.LayerAnimations.Zoom)
                    .autoCloseChildPanels("stroke")
            )
        );
    }

    private constructor(layer: Layers.Layer, workspace: Components.Workspace) {
        super(
            layer,
            workspace,
            pgettext("studio", "Workspace properties"),
            "compact",
            workspace.layout && (workspace.layout as MoveableLayout<Components.Workspace, Components.Workspace, IStudioStyle>).style
        );
    }

    private get workspace(): Components.Workspace {
        return this.ref;
    }

    onCards(cards: Components.Cards): void {
        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Name"),
                controls: [
                    new Forms.Text("singleline", Forms.Text.bind(this.workspace, "name", ""))
                        .escape(() => {
                            this.close();

                            return true;
                        })
                        .enter(() => {
                            this.close();

                            return true;
                        })
                        .placeholder(pgettext("studio", "Unnamed workspace"))
                        .autoFocus()
                        .autoSelect(),
                ],
            })
        );
    }
}
