import { Components, Debounce, Forms, Layers, REGEX_IS_URL, compare, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { BuilderComponent } from "@app/components/builder";
import { REGEX_IS_EMAIL } from "../../helpers/regex";
import { TEST_SLACK } from "@server/endpoints";
import { HELP_NOTIFICATION, HELP_SLACK } from "@app/urls";
import { IHookSettings } from "@server/entities/definitions/interface";
import { IUser } from "@server/entities/users";
import { mutate } from "@app/helpers/api";
import superagent from "superagent";
import * as UpdateHookSettingsQuery from "@app/queries/definitions/update-hooks.graphql";

export class NotificationsComponent extends Components.Controller<{
    readonly studio: StudioComponent;
    readonly definitionId?: string;
    readonly hooks: IHookSettings;
    readonly user?: IUser;
}> {
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(studio: StudioComponent, hooks: IHookSettings, definitionId?: string): NotificationsComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new NotificationsComponent(panel, studio, hooks, definitionId),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent, hooks: IHookSettings, definitionId?: string) {
        super(
            layer,
            {
                studio,
                definitionId,
                hooks,
                user: studio.userAccount,
            },
            pgettext("studio", "Notifications"),
            "compact",
            studio.style,
            "right",
            "on-when-validated",
            pgettext("studio", "Close")
        );

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private testSlack(url: string, includeFields: boolean): Promise<boolean> {
        return new Promise((fnResolve: (succeeded: boolean) => void) => {
            superagent
                .post(TEST_SLACK)
                .send({
                    url,
                    includeFields,
                    name: this.ref.studio.activeComponent instanceof BuilderComponent ? this.ref.studio.activeComponent.name : undefined,
                    mockup:
                        this.ref.studio.activeComponent instanceof BuilderComponent ? this.ref.studio.activeComponent.mockup : undefined,
                })
                .then((res) => {
                    fnResolve(res.status === 200);
                })
                .catch(() => {
                    fnResolve(false);
                });
        });
    }

    onCards(cards: Components.Cards): void {
        const isBuilderOpen = this.ref.studio.activeComponent instanceof BuilderComponent;
        const email =
            this.ref.hooks.email || (this.ref.hooks.email = { enabled: false, recipient: this.ref.user && this.ref.user.emailAddress });
        const slack = this.ref.hooks.slack || (this.ref.hooks.slack = { enabled: false });

        let currentHooks = JSON.parse(JSON.stringify({ ...this.ref.hooks, email, slack }));
        const updateHooks = new Debounce(() => {
            if (this.ref.definitionId) {
                const hooks = { ...this.ref.hooks, email, slack };

                if (!compare(hooks, currentHooks, true)) {
                    currentHooks = JSON.parse(JSON.stringify(hooks));

                    mutate({
                        query: UpdateHookSettingsQuery,
                        variables: {
                            id: this.ref.definitionId,
                            hooks,
                        },
                    });
                }
            }
        }, 100);

        if (!this.ref.definitionId) {
            cards.add(
                new Forms.Form({
                    controls: [
                        new Forms.Notification(
                            pgettext(
                                "studio",
                                "An account is required to use notifications. Sign in or create an account with just your email address to do so."
                            ),
                            "info"
                        ),
                    ],
                })
            );
        }

        const emailGroup = new Forms.Group([
            new Forms.Static(pgettext("studio", "Send a notification to:")),
            new Forms.Email((email && email.recipient) || "")
                .autoValidate((recipient: Forms.Text) =>
                    recipient.value === "" || recipient.isDisabled || !recipient.isObservable
                        ? "unknown"
                        : REGEX_IS_EMAIL.test(recipient.value)
                        ? "pass"
                        : "fail"
                )
                .on((recipient: Forms.Email) => {
                    if (recipient && !recipient.isInvalid) {
                        email.recipient = recipient.value;

                        updateHooks.invoke();
                    }
                }),
            new Forms.Checkbox(pgettext("studio", "Include response data in the message"), email.includeFields).on(
                (checkbox: Forms.Checkbox) => {
                    email.includeFields = checkbox.isChecked;
                    updateHooks.invoke();
                }
            ),
        ]).visible(email.enabled);

        const emailEnabled = new Forms.Checkbox(
            pgettext("studio", "Send an email when someone completes your form ([learn more](%1))", HELP_NOTIFICATION),
            email.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;
                email.enabled = checked;
                updateHooks.invoke();

                emailGroup.visible(checked);
            });

        cards.add(
            new Forms.Form({
                title: "📧 " + pgettext("studio", "Email notification"),
                controls: [emailEnabled, emailGroup],
            })
        ).isDisabled = this.ref.definitionId ? false : true;

        const slackUrl = new Forms.Text("singleline", slack.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url: Forms.Text) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((url: Forms.Text) => {
                slack.url = url.value;
                updateHooks.invoke();

                slackTestButton.buttonType = "normal";
                slackTestButton.disabled(!slackUrl.value || !REGEX_IS_URL.test(slackUrl.value));
                slackTestButton.label("🩺 " + pgettext("studio", "Test"));
            });
        const slackIncludeFields = new Forms.Checkbox(pgettext("studio", "Include response data in the message"), slack.includeFields).on(
            (checkbox: Forms.Checkbox) => {
                slack.includeFields = checkbox.isChecked;
                updateHooks.invoke();
            }
        );
        const slackTestButton = new Forms.Button("🩺 " + pgettext("studio", "Test"))
            .visible(isBuilderOpen)
            .disable()
            .on((testButton) => {
                slackUrl.disable();
                testButton.disable();
                testButton.buttonType = "normal";
                testButton.label("⏳ " + pgettext("studio", "Testing..."));

                this.testSlack(slackUrl.value, slackIncludeFields.isChecked).then((succeeded) => {
                    slackUrl.enable();
                    testButton.enable();
                    testButton.buttonType = succeeded ? "accept" : "warning";
                    testButton.label(succeeded ? "✔ " + pgettext("studio", "All good!") : "❌ " + pgettext("studio", "Something's wrong"));
                });
            });
        const slackGroup = new Forms.Group([
            new Forms.Static(
                pgettext(
                    "studio",
                    "Please create an incoming [Slack webhook](https://api.slack.com/incoming-webhooks) and enter the URL of the webhook here:"
                )
            ).markdown(),
            slackUrl,
            slackIncludeFields,
            slackTestButton,
        ]).visible(slack.enabled);

        const slackEnabled = new Forms.Checkbox(
            pgettext("studio", "Send a Slack message when someone completes your form ([learn more](%1))", HELP_SLACK),
            slack.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                slack.enabled = checked;
                updateHooks.invoke();

                slackGroup.visible(checked);
            });

        cards.add(
            new Forms.Form({
                title: "📣 " + pgettext("studio", "Slack notification"),
                controls: [slackEnabled, slackGroup],
            })
        ).isDisabled = this.ref.definitionId ? false : true;
    }
}
