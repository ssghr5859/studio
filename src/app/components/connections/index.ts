import { Components, Debounce, Forms, Layers, REGEX_IS_URL, compare, findFirst, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { BuilderComponent } from "@app/components/builder";
import { RequestPremiumComponent } from "@app/components/request-premium";
import { TEST_WEBHOOK } from "@server/endpoints";
import {
    HELP_WEBHOOK,
    HELP_WEBHOOK_CUSTOM,
    HELP_WEBHOOK_CUSTOM_RAW,
    HELP_WEBHOOK_MAKE,
    HELP_WEBHOOK_PABBLY,
    HELP_WEBHOOK_ZAPIER,
} from "@app/urls";
import { IHookSettings } from "@server/entities/definitions/interface";
import { mutate } from "@app/helpers/api";
import superagent from "superagent";
import * as UpdateHookSettingsQuery from "@app/queries/definitions/update-hooks.graphql";

export class ConnectionsComponent extends Components.Controller<{
    readonly studio: StudioComponent;
    readonly definitionId?: string;
    readonly definitionName?: string;
    readonly hooks: IHookSettings;
    readonly premium: boolean;
}> {
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(
        studio: StudioComponent,
        hooks: IHookSettings,
        premium: boolean,
        definitionId?: string,
        definitionName?: string
    ): ConnectionsComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new ConnectionsComponent(panel, studio, hooks, premium, definitionId, definitionName),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(
        layer: Layers.Layer,
        studio: StudioComponent,
        hooks: IHookSettings,
        premium: boolean,
        definitionId?: string,
        definitionName?: string
    ) {
        super(
            layer,
            {
                studio,
                definitionId,
                definitionName,
                hooks,
                premium,
            },
            pgettext("studio", "Connections"),
            "compact",
            studio.style,
            "right",
            "on-when-validated",
            pgettext("studio", "Close"),
            [new Components.ToolbarLink(studio.style.results.buttons.help, HELP_WEBHOOK)]
        );

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private testWebhook(url: string, nvp: boolean): Promise<boolean> {
        return new Promise((fnResolve: (succeeded: boolean) => void) => {
            superagent
                .post(TEST_WEBHOOK)
                .send({
                    url,
                    nvp,
                    mockup:
                        this.ref.studio.activeComponent instanceof BuilderComponent ? this.ref.studio.activeComponent.mockup : undefined,
                })
                .then((res) => {
                    fnResolve(res.status === 200);
                })
                .catch(() => {
                    fnResolve(false);
                });
        });
    }

    onCards(cards: Components.Cards): void {
        const isBuilderOpen = this.ref.studio.activeComponent instanceof BuilderComponent;
        const webhook = this.ref.hooks.webhook || (this.ref.hooks.webhook = { enabled: false, nvp: true });
        const make = this.ref.hooks.make || this.ref.hooks.integromat || (this.ref.hooks.make = { enabled: false });
        const zapier = this.ref.hooks.zapier || (this.ref.hooks.zapier = { enabled: false });
        const pabbly = this.ref.hooks.pabbly || (this.ref.hooks.pabbly = { enabled: false });

        let currentHooks = JSON.parse(
            JSON.stringify({
                ...this.ref.hooks,
                webhook,
                make,
                zapier,
                pabbly,
            })
        );
        const updateHooks = new Debounce(() => {
            if (this.ref.definitionId) {
                const hooks = { ...this.ref.hooks, webhook, make, zapier, pabbly };

                delete hooks.integromat;

                if (!compare(hooks, currentHooks, true)) {
                    currentHooks = JSON.parse(JSON.stringify(hooks));

                    mutate({
                        query: UpdateHookSettingsQuery,
                        variables: {
                            id: this.ref.definitionId,
                            hooks,
                        },
                    });
                }
            }
        }, 100);

        if (!this.ref.definitionId) {
            cards.add(
                new Forms.Form({
                    controls: [
                        new Forms.Notification(
                            pgettext(
                                "studio",
                                "An account is required to use connections. Sign in or create an account with just your email address to do so."
                            ),
                            "info"
                        ),
                    ],
                })
            );
        } else if (!this.ref.premium) {
            cards.add(
                new Forms.Form({
                    title: "🔒 " + pgettext("studio", "Upgrade required"),
                    controls: [
                        new Forms.Static(
                            "To use all connection options you need to unlock these features. Please click the button below for more information."
                        ),
                        new Forms.Button("🛒 " + pgettext("studio", "Upgrade now"), "normal")
                            .on(() => {
                                if (this.ref.studio.activeComponent instanceof BuilderComponent) {
                                    this.ref.studio.activeComponent.requestPremium();
                                } else {
                                    RequestPremiumComponent.open(
                                        this.ref.studio,
                                        this.ref.definitionId || "",
                                        this.ref.definitionName || ""
                                    );
                                }
                            })
                            .width(250),
                    ],
                })
            );
        }

        const fnService = (
            title: string,
            domains: string[],
            help: string,
            description: string,
            service: {
                enabled: boolean;
                url?: string;
                allowDownloads?: boolean;
            },
            serviceName?: string
        ) => {
            const validateUrl = (url: string) =>
                findFirst(domains, (domain) => {
                    if (domain.indexOf("*") !== -1) {
                        const a = domain.substring(0, domain.indexOf("*"));
                        const b = domain.substring(domain.indexOf("*") + 1);

                        return url.indexOf(a) === 0 && url.indexOf(b) > a.length;
                    } else {
                        return url.indexOf(domain) === 0;
                    }
                })
                    ? true
                    : false;
            const serviceUrl = new Forms.Text("singleline", service.url)
                .inputMode("url")
                .placeholder("https://")
                .autoValidate((url) =>
                    url.value === "" || url.isDisabled || !url.isObservable
                        ? "unknown"
                        : REGEX_IS_URL.test(url.value) && validateUrl(url.value)
                        ? "pass"
                        : "fail"
                )
                .on((url) => {
                    service.url = url.value;
                    updateHooks.invoke();

                    serviceTestButton.buttonType = "normal";
                    serviceTestButton.disabled(!serviceUrl.value || !REGEX_IS_URL.test(serviceUrl.value) || !validateUrl(serviceUrl.value));
                    serviceTestButton.label("🩺 " + pgettext("studio", "Test"));
                });
            const serviceTestButton = new Forms.Button("🩺 " + pgettext("studio", "Test"))
                .visible(isBuilderOpen)
                .disable()
                .on((testButton) => {
                    serviceUrl.disable();
                    testButton.disable();
                    testButton.buttonType = "normal";
                    testButton.label("⏳ " + pgettext("studio", "Testing..."));

                    this.testWebhook(serviceUrl.value, true).then((succeeded) => {
                        serviceUrl.enable();
                        testButton.enable();
                        testButton.buttonType = succeeded ? "accept" : "warning";
                        testButton.label(
                            succeeded ? "✔ " + pgettext("studio", "All good!") : "❌ " + pgettext("studio", "Something's wrong")
                        );
                    });
                });

            const serviceGroup = new Forms.Group([new Forms.Static(description).markdown(), serviceUrl, serviceTestButton]).visible(
                service.enabled
            );

            const serviceEnabled = new Forms.Checkbox(
                pgettext("studio", "Submit completed forms to %1 ([learn more](%2))", serviceName || title, help),
                service.enabled
            )
                .markdown()
                .on((checkbox: Forms.Checkbox) => {
                    const checked = checkbox.isChecked;

                    service.enabled = checked;
                    updateHooks.invoke();

                    serviceGroup.visible(checked);
                });

            cards.add(
                new Forms.Form({
                    title: "🌐 " + title,
                    controls: [serviceEnabled, serviceGroup],
                })
            ).isDisabled = !this.ref.definitionId || !this.ref.premium;
        };

        fnService(
            pgettext("studio", "Make (formerly Integromat)"),
            ["https://hook.*.make.com/", "https://hook.integromat.com/"],
            HELP_WEBHOOK_MAKE,
            pgettext(
                "studio",
                "Configure [Make](%1) and paste their webhook URL here:",
                "https://www.make.com/en/integrations/tripetto?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program"
            ),
            make,
            "Make"
        );
        fnService(
            "Zapier",
            ["https://hooks.zapier.com/"],
            HELP_WEBHOOK_ZAPIER,
            pgettext("studio", "Configure [Zapier](%1) and paste their webhook URL here:", "https://zapier.com/apps/webhook/integrations"),
            zapier
        );
        fnService(
            "Pabbly Connect",
            ["https://connect.pabbly.com/"],
            HELP_WEBHOOK_PABBLY,
            pgettext(
                "studio",
                "Configure [Pabbly Connect](%1) and paste their webhook URL here:",
                "https://www.pabbly.com/connect/integrations/Tripetto/"
            ),
            pabbly
        );

        const webhookUrl = new Forms.Text("singleline", webhook.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url: Forms.Text) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((url: Forms.Text) => {
                webhook.url = url.value;
                updateHooks.invoke();

                webhookTestButton.buttonType = "normal";
                webhookTestButton.disabled(!webhookUrl.value || !REGEX_IS_URL.test(webhookUrl.value));
                webhookTestButton.label("🩺 " + pgettext("studio", "Test"));
            });
        const webhookRaw = new Forms.Checkbox(
            pgettext("studio", "Send raw response data to webhook ([learn more](%1))", HELP_WEBHOOK_CUSTOM_RAW),
            !webhook.nvp
        )
            .markdown()
            .description(
                pgettext(
                    "studio",
                    "Please only use this option if you're an expert on webhooks. When enabled this option sends way more data to your webhook and in such a format your automation tool cannot process this by default. By leaving this option disabled, you can just use your response data easily."
                )
            )
            .on((checkbox: Forms.Checkbox) => {
                webhook.nvp = !checkbox.isChecked;
                updateHooks.invoke();
            });
        const webhookTestButton = new Forms.Button("🩺 " + pgettext("studio", "Test"))
            .visible(isBuilderOpen)
            .disable()
            .on((testButton) => {
                webhookUrl.disable();
                testButton.disable();
                testButton.buttonType = "normal";
                testButton.label("⏳ " + pgettext("studio", "Testing..."));

                this.testWebhook(webhookUrl.value, !webhookRaw.isChecked).then((succeeded) => {
                    webhookUrl.enable();
                    testButton.enable();
                    testButton.buttonType = succeeded ? "accept" : "warning";
                    testButton.label(succeeded ? "✔ " + pgettext("studio", "All good!") : "❌ " + pgettext("studio", "Something's wrong"));
                });
            });

        const webhookGroup = new Forms.Group([
            new Forms.Static(pgettext("studio", "Post the data to the following URL:")),
            webhookUrl,
            webhookRaw,
            webhookTestButton,
        ]).visible(webhook.enabled);

        const webhookEnabled = new Forms.Checkbox(
            pgettext("studio", "Submit completed forms to a custom webhook ([learn more](%1))", HELP_WEBHOOK_CUSTOM),
            webhook.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                webhook.enabled = checked;
                updateHooks.invoke();

                webhookGroup.visible(checked);
            });

        cards.add(
            new Forms.Form({
                title: "🔗 " + pgettext("studio", "Custom webhook"),
                controls: [webhookEnabled, webhookGroup],
            })
        ).isDisabled = this.ref.definitionId ? false : true;
    }
}
