import { Components, DOM, linearicon } from "tripetto";
import { IHeaderStyle } from "./style";

export class Button<T> extends Components.ToolbarButton<T> {
    private mode: "automate" | "normal" | "small" | "share" | "results" | "customize";
    private icon: number;

    constructor(props: {
        style: IHeaderStyle;
        mode?: "automate" | "normal" | "small" | "share" | "results" | "customize";
        icon: number;
        label?: string;
        onTap: (button: Button<T>) => void;
    }) {
        super(props.label ? props.style.button.withLabel : props.style.button.withoutLabel, props.label, undefined, props.onTap);

        this.mode = props.mode || "normal";
        this.icon = props.icon;
    }

    onDraw(toolbar: Components.Toolbar<T>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.addSelector(this.mode);
        element.create("i", (icon: DOM.Element) => linearicon(this.icon, icon));
    }
}

export class ButtonWithMenu<T> extends Components.ToolbarMenu<T> {
    private mode: "automate" | "normal" | "small" | "share" | "results" | "customize";
    private icon: number;

    constructor(props: {
        style: IHeaderStyle;
        mode?: "automate" | "normal" | "small" | "share" | "results" | "customize";
        icon: number;
        options: () => Components.MenuOption[];
        label?: string;
    }) {
        super(props.label ? props.style.button.withLabel : props.style.button.withoutLabel, props.label, props.options);

        this.mode = props.mode || "normal";
        this.icon = props.icon;
    }

    onDraw(toolbar: Components.Toolbar<T>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.addSelector(this.mode);
        element.create("i", (icon: DOM.Element) => linearicon(this.icon, icon));
    }
}
