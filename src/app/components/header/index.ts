import { StudioComponent } from "@app/components/studio";
import { Components, Layers, assert, each, pgettext } from "tripetto";
import { Title } from "./title";
import { User } from "./user";
import { Menu, contactMenu as supportMenu, developersMenu, supportMenu as learnMenu } from "./menu";
import { Toggle } from "./toggle";
import { Separator } from "./separator";
import { Spacer } from "./spacer";
import { Button, ButtonWithMenu } from "./button";
import { BuilderComponent } from "@app/components/builder";
import { WorkspaceComponent } from "@app/components/workspace";
import { ShareComponent } from "@app/components/share";
import { DialogComponent } from "@app/components/dialog";
import { DeviceSize } from "@server/providers/metric";
import { windowSize } from "@app/helpers/device";
import { createDefinition } from "@app/helpers/definition";
import { addTile, getOrCreateFirstCollection } from "@app/helpers/workspace";
import { Loader } from "@app/components/loader";
import { HELP_RUNNERS } from "@app/urls";
import ASSET_TRIPETTO from "../../assets/logos/tripetto.svg";

export class HeaderComponent {
    private readonly studio: StudioComponent;
    private readonly menu: [Menu<DeviceSize>, Separator<DeviceSize>];
    private readonly support: [Menu<DeviceSize>, Separator<DeviceSize>];
    private readonly back: [Button<DeviceSize>, Separator<DeviceSize>];
    private readonly actions: [Button<DeviceSize>, Button<DeviceSize>];
    private readonly add: Button<DeviceSize>;
    private readonly title: Title;
    private readonly builder: [
        ButtonWithMenu<DeviceSize>,
        Separator<DeviceSize>,
        Button<DeviceSize>,
        Separator<DeviceSize>,
        ButtonWithMenu<DeviceSize>,
        Separator<DeviceSize>,
        Button<DeviceSize>,
        Separator<DeviceSize>,
        Spacer<DeviceSize>,
        Toggle<DeviceSize>,
        Toggle<DeviceSize>,
        Toggle<DeviceSize>,
        Spacer<DeviceSize>,
        Separator<DeviceSize>
    ];
    private readonly preview: {
        desktop: Toggle<DeviceSize>;
        tablet: Toggle<DeviceSize>;
        phone: Toggle<DeviceSize>;
    };
    readonly user: User;

    constructor(studio: StudioComponent) {
        const style = studio.style.header;
        const badge = (label = "Locked") => (this.needUpgrade ? label : "");

        this.studio = studio;
        this.menu = [new Menu<DeviceSize>(studio, 0xe92b, () => this.getMenu()), new Separator<DeviceSize>(style)];

        this.support = [
            new Menu<DeviceSize>(studio, 0xe7ed, () => this.getSupport()).excludeInViews("xs", "s"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s"),
        ];

        this.back = [
            new Button<DeviceSize>({ style, icon: 0xe93b, onTap: () => this.goBack() }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
        ];

        this.actions = [
            new Button<DeviceSize>({
                style,
                mode: "small",
                icon: 0xe8e1,
                label: pgettext("studio", "Restore"),
                onTap: () => this.doRestore(),
            }).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "small",
                icon: 0xe611,
                label: pgettext("studio", "Wipe"),
                onTap: () => this.doWipe(),
            }).excludeInViews("xs", "s", "m"),
        ];

        this.add = new Button<DeviceSize>({
            style,
            mode: "small",
            icon: 0xe8f8,
            label: pgettext("studio", "Use this template"),
            onTap: () => this.doUseTemplate(),
        }).excludeInViews("xs", "s", "m");

        this.title = new Title(style, "", () => this.doEdit());

        this.preview = {
            desktop: new Toggle<DeviceSize>({
                style,
                icon: 0xe7af,
                onToggle: () => this.toggleDevice("desktop"),
            }).excludeInViews("xs", "s", "m"),
            tablet: new Toggle<DeviceSize>({
                style,
                icon: 0xe7ab,
                onToggle: () => this.toggleDevice("tablet"),
            }).excludeInViews("xs", "s"),
            phone: new Toggle<DeviceSize>({
                style,
                icon: 0xe7a5,
                onToggle: () => this.toggleDevice("phone"),
            }),
        };

        this.builder = [
            new ButtonWithMenu<DeviceSize>({
                style,
                mode: "customize",
                icon: 0xe62b,
                label: pgettext("studio", "Customize"),
                options: () => [
                    ...this.getRunnersMenu(),
                    new Components.MenuSeparator(),
                    new Components.MenuLabel(pgettext("studio", "Form appearance")),
                    new Components.MenuItemWithIcon(0xe756, pgettext("studio", "Properties"), () => this.doEdit()),
                    new Components.MenuItemWithIcon(0xe61f, pgettext("studio", "Styles"), () => this.doStyles()),
                    new Components.MenuItemWithIcon(0xe886, pgettext("studio", "Translations"), () => this.doL10n()),
                    ...this.getRequestPremiumMenu(pgettext("studio", "Remove Tripetto branding")),
                ],
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "share",
                icon: 0xe8c1,
                label: pgettext("studio", "Share"),
                onTap: () => this.doShareOrEmbed(),
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new ButtonWithMenu<DeviceSize>({
                style,
                mode: "automate",
                icon: 0xe920,
                label: pgettext("studio", "Automate"),
                options: () => [
                    new Components.MenuItemWithIcon(0xe8aa, pgettext("studio", "Notifications"), () => this.doNotifications()),
                    new Components.MenuItemWithIcon(0xe920, pgettext("studio", "Connections"), () => this.doConnections()).badge(badge()),
                    new Components.MenuItemWithIcon(0xe77f, pgettext("studio", "Tracking"), () => this.doTrackers()).badge(badge()),
                    ...this.getRequestPremiumMenu(),
                ],
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "results",
                icon: 0xe6ac,
                label: pgettext("studio", "Results"),
                onTap: () => this.doResults(),
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Spacer(style),
            this.preview.desktop,
            this.preview.tablet,
            this.preview.phone,
            new Spacer(style),
            new Separator<DeviceSize>(style),
        ];

        this.user = new User(studio);

        const toolbarComponent = studio.layer.component(
            new Components.ToolbarComponent<DeviceSize>(
                {
                    style: style.style,
                    view: windowSize(),
                    left: [
                        ...this.menu,
                        ...this.back,
                        this.title,
                        ...this.actions,
                        this.add,
                        new Button<DeviceSize>({ style, mode: "small", icon: 0xe672, onTap: () => this.doEdit() }).excludeInViews("xs"),
                    ],
                    right: [new Separator(style), ...this.builder, ...this.support, this.user],
                },
                Layers.Layer.configuration.height(style.height).alignTop()
            )
        );

        toolbarComponent.layer.hook("OnResize", "synchronous", () => {
            toolbarComponent.toolbar.view = windowSize();
        });
    }

    private get needUpgrade(): boolean {
        const builder = this.studio.activeComponent instanceof BuilderComponent && this.studio.activeComponent;

        if (!builder || builder.tier === "premium" || !this.studio.userAccount || !builder.hasId || builder.isTemplate) {
            return false;
        }

        return true;
    }

    private getRunnersMenu(): Components.MenuOption[] {
        const builder = this.studio.activeComponent instanceof BuilderComponent && this.studio.activeComponent;

        if (!builder) {
            return [];
        }

        return [
            new Components.MenuLabel(pgettext("studio", "Form face")),
            new Components.MenuItemWithIcon(
                builder.runner === "autoscroll" ? 0xe999 : 0xe98d,
                pgettext("studio", "Autoscroll"),
                () => (builder.runner = "autoscroll")
            ),
            new Components.MenuItemWithIcon(
                builder.runner === "chat" ? 0xe999 : 0xe98d,
                pgettext("studio", "Chat"),
                () => (builder.runner = "chat")
            ),
            new Components.MenuItemWithIcon(
                builder.runner === "classic" ? 0xe999 : 0xe98d,
                pgettext("studio", "Classic"),
                () => (builder.runner = "classic")
            ),
            new Components.MenuSeparator(),
            new Components.MenuLinkWithIcon(0xe933, pgettext("studio", "Help with form faces"), HELP_RUNNERS),
        ];
    }

    private getRequestPremiumMenu(title: string = pgettext("studio", "Unlock all features")): Components.MenuOption[] {
        if (!this.needUpgrade) {
            return [];
        }

        return [
            new Components.MenuSeparator(),
            new Components.MenuLabel(pgettext("studio", "Upgrade")),
            new Components.MenuItemWithIcon(0xe7ff, title, () => this.requestPremium()),
        ];
    }

    private getMenu(): Components.MenuOption[] {
        const options: Components.MenuOption[] = [];
        const badge = (label = "Locked") => (this.needUpgrade ? label : "");

        if (this.user.isAuthenticated) {
            options.push(
                new Components.MenuLabel(pgettext("studio", "Navigation")),
                new Components.MenuItemWithIcon(
                    0xe93b,
                    pgettext("studio", "Back"),
                    () => this.goBack(),
                    !this.back[0].isVisible || this.back[0].isDisabled
                ),
                new Components.MenuItemWithIcon(
                    0xe602,
                    pgettext("studio", "Home"),
                    () => this.goHome(),
                    this.studio.activeComponent instanceof WorkspaceComponent && this.studio.activeComponent.isRoot
                )
            );
        } else {
            options.push(
                new Components.MenuLabel(pgettext("studio", "Account")),
                new Components.MenuItemWithIcon(0xe71e, pgettext("studio", "Sign in / register"), () => this.user.signIn())
            );
        }
        if (this.studio.activeComponent instanceof BuilderComponent) {
            options.push(
                new Components.MenuSeparator(),
                new Components.MenuLabel(pgettext("studio", "Design")),
                new Components.MenuSubmenuWithIcon(0xe6f3, pgettext("studio", "Form face"), this.getRunnersMenu()),
                new Components.MenuItemWithIcon(0xe672, pgettext("studio", "Properties"), () => this.doEdit()),
                new Components.MenuItemWithIcon(0xe61f, pgettext("studio", "Styles"), () => this.doStyles()),
                new Components.MenuItemWithIcon(0xe886, pgettext("studio", "Translations"), () => this.doL10n()),
                this.add.isVisible
                    ? new Components.MenuItemWithIcon(0xe8f8, pgettext("studio", "Use this template"), () => this.doUseTemplate())
                    : new Components.MenuItemWithIcon(0xe6db, pgettext("studio", "Share as template"), () => this.doShareTemplate()),
                new Components.MenuSeparator(),
                new Components.MenuLabel(pgettext("studio", "Run")),
                new Components.MenuItemWithIcon(
                    0xe8c1,
                    pgettext("studio", "Share or embed"),
                    () => this.doShareOrEmbed(),
                    this.builder[2].isDisabled
                ),
                new Components.MenuSeparator(),
                new Components.MenuLabel(pgettext("studio", "Automate")),
                new Components.MenuItemWithIcon(
                    0xe8aa,
                    pgettext("studio", "Notifications"),
                    () => this.doNotifications(),
                    this.builder[4].isDisabled
                ),
                new Components.MenuItemWithIcon(
                    0xe920,
                    pgettext("studio", "Connections"),
                    () => this.doConnections(),
                    this.builder[4].isDisabled
                ).badge(badge()),
                new Components.MenuItemWithIcon(
                    0xe77f,
                    pgettext("studio", "Tracking"),
                    () => this.doTrackers(),
                    this.builder[4].isDisabled
                ).badge(badge()),
                new Components.MenuSeparator(),
                new Components.MenuLabel(pgettext("studio", "Results")),
                new Components.MenuItemWithIcon(
                    0xe6ac,
                    pgettext("studio", "View results"),
                    () => this.doResults(),
                    this.builder[6].isDisabled
                ),
                ...this.getRequestPremiumMenu()
            );

            if (this.actions[0].isVisible || this.actions[1].isVisible) {
                options.push(new Components.MenuSeparator(), new Components.MenuLabel(pgettext("studio", "Actions")));

                if (this.actions[0].isVisible) {
                    options.push(new Components.MenuItemWithIcon(0xe8e1, pgettext("studio", "Restore"), () => this.doRestore()));
                }

                if (this.actions[1].isVisible) {
                    options.push(new Components.MenuItemWithIcon(0xe611, pgettext("studio", "Wipe"), () => this.doWipe()));
                }
            }
        } else if (this.studio.activeComponent instanceof WorkspaceComponent) {
            options.push(
                new Components.MenuSeparator(),
                new Components.MenuLabel(pgettext("studio", "Workspace")),
                new Components.MenuItemWithIcon(0xe672, pgettext("studio", "Properties"), () => this.doEdit())
            );
        }

        if (this.support[0].isExcluded) {
            options.push(
                new Components.MenuSeparator(),
                new Components.MenuLabel(pgettext("studio", "More...")),
                new Components.MenuSubmenuWithIcon(0xe6da, pgettext("studio", "Learn"), learnMenu(this.studio)),
                new Components.MenuSubmenuWithIcon(0xe7ed, pgettext("studio", "Support"), supportMenu()),
                new Components.MenuSubmenuWithImage(`${ASSET_TRIPETTO}`, pgettext("studio", "For developers"), developersMenu())
            );
        }

        return options;
    }

    private getSupport(): Components.MenuOption[] {
        return [
            new Components.MenuSeparator(),
            new Components.MenuLabel(pgettext("studio", "Learn")),
            ...learnMenu(this.studio),
            new Components.MenuSeparator(),
            new Components.MenuLabel(pgettext("studio", "Support")),
            ...supportMenu(),
            new Components.MenuSeparator(),
            new Components.MenuSubmenuWithImage(`${ASSET_TRIPETTO}`, pgettext("studio", "For developers"), developersMenu()),
        ];
    }

    private goHome(): void {
        if (this.user.isAuthenticated && !this.studio.rootWorkspace) {
            this.studio.openWorkspaceRoot();
        } else {
            this.studio.showRootWorkspace();
        }
    }

    private goBack(): void {
        if (this.user.isAuthenticated && !this.studio.rootWorkspace) {
            this.studio.openWorkspaceRoot();
        } else if (this.studio.activeComponent) {
            this.studio.activeComponent.close();
        }
    }

    private doEdit(): void {
        if (this.studio.activeComponent) {
            this.studio.activeComponent.edit();
        }
    }

    private doShareTemplate(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.shareTemplate(this.studio.activeComponent.id);
        }
    }

    private doShareOrEmbed(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            const shareButton = this.builder[2];

            shareButton.disable();

            this.studio.activeComponent
                .share()
                .then((shareComponent: ShareComponent) => (shareComponent.whenClosed = () => shareButton.enable()));
        }
    }

    private doStyles(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.activeComponent.stylesEditor();
        }
    }

    private doL10n(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.activeComponent.l10nEditor();
        }
    }

    private requestPremium(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.activeComponent.requestPremium();
        }
    }

    private doNotifications(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.activeComponent.notifications();
        }
    }

    private doConnections(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.activeComponent.connections();
        }
    }

    private doTrackers(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.activeComponent.tracking();
        }
    }

    private doResults(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            const resultsButton = this.builder[6];

            resultsButton.disable();

            this.studio.activeComponent.results().whenClosed = () => resultsButton.enable();
        }
    }

    private doRestore(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            const builder = this.studio.activeComponent;

            DialogComponent.confirm(
                pgettext("studio", "Restore form"),
                pgettext(
                    "studio",
                    "Do you want to restore the form to its initial state? You will lose all the changes you've made to it."
                ),
                pgettext("studio", "Yes, restore please!"),
                pgettext("studio", "No, do nothing!"),
                true,
                () => builder.restore()
            );
        }
    }

    private doWipe(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            const builder = this.studio.activeComponent;

            DialogComponent.confirm(
                pgettext("studio", "Wipe form"),
                pgettext(
                    "studio",
                    "Do you want to clear the current form and wipe all items in the builder? You will lose all the work you've done so far."
                ),
                pgettext("studio", "Yes, wipe it!"),
                pgettext("studio", "No, keep it!"),
                true,
                () => builder.wipe()
            );
        }
    }

    private doUseTemplate(): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            const builderComponent = this.studio.activeComponent;
            const definition = builderComponent.ref.definition!;
            const runner = builderComponent.runner;
            const styles = builderComponent.styles;
            const l10n = builderComponent.l10n;

            Loader.show();
            builderComponent.close();

            this.studio.openWorkspaceRoot().then((workspaceComponent?: WorkspaceComponent) => {
                if (!workspaceComponent || !workspaceComponent.workspace) {
                    StudioComponent.showApiError(true);
                    return;
                }

                const workspace = workspaceComponent.workspace;
                const collection = assert(getOrCreateFirstCollection(workspace));

                createDefinition(runner, definition, styles, l10n, StudioComponent.showApiError).then((id: string) => {
                    addTile(collection, id);

                    Loader.hide();
                });
            });
        }
    }

    private toggleDevice(device: "phone" | "tablet" | "desktop"): void {
        if (this.studio.activeComponent instanceof BuilderComponent) {
            this.studio.activeComponent.toggleDevice(device);
        }
    }

    update(): void {
        let mode: "loading" | "workspace" | "build" = "loading";

        each([...this.back, ...this.actions, this.add, ...this.builder], (toolbarItem: Components.ToolbarItem<DeviceSize>) =>
            toolbarItem.hide()
        );

        if (this.studio.activeComponent instanceof BuilderComponent) {
            const builder = this.studio.activeComponent;

            mode = "build";

            each(this.builder, (toolbarItem: Components.ToolbarItem<DeviceSize>) => toolbarItem.show());

            (builder.onPreviewModeChange = () => {
                this.preview.desktop.isSelected = builder.device === "desktop";
                this.preview.tablet.isSelected = builder.device === "tablet";
                this.preview.phone.isSelected = builder.device === "phone";
            })();

            this.builder[2].disabled(builder.isTemplate);
            this.builder[4].disabled(builder.isTemplate);
            this.builder[6].disabled(builder.isTemplate);

            if (this.studio.userAccount) {
                each(this.back, (toolbarItem: Components.ToolbarItem<DeviceSize>) => toolbarItem.show());

                if (builder.isTemplate) {
                    this.add.show();
                }
            } else {
                each(this.actions, (toolbarItem: Components.ToolbarItem<DeviceSize>) => toolbarItem.show());
                this.actions[0].visible(!builder.isTemplate);
            }
        } else if (this.studio.activeComponent instanceof WorkspaceComponent) {
            mode = "workspace";

            if (!this.studio.activeComponent.isRoot) {
                each(this.back, (toolbarItem: Components.ToolbarItem<DeviceSize>) => toolbarItem.show());
            }
        }

        if (this.studio.activeComponent) {
            this.studio.activeComponent.attach(this, (props: { name: string; hasName: boolean }) => {
                this.title.label = props.name;
                this.title.setMode(mode);
            });
        }
    }
}
