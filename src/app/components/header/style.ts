/** Dependencies */
import { Components } from "tripetto";

export interface IHeaderStyle {
    height: number;
    style: Components.IToolbarStyle;
    separator: Components.IToolbarItemStyle;
    divider: Components.IToolbarItemStyle;
    spacer: Components.IToolbarItemStyle;
    button: {
        withLabel: Components.IToolbarMenuStyle;
        withoutLabel: Components.IToolbarMenuStyle;
    };
    title: Components.IToolbarItemStyle;
    toggle: Components.IToolbarItemStyle;
    user: Components.IToolbarMenuStyle;
}
