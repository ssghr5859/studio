import { Components } from "tripetto";
import { IHeaderStyle } from "./style";

export class Spacer<T> extends Components.ToolbarItem<T> {
    constructor(style: IHeaderStyle) {
        super(style.spacer);
    }
}
