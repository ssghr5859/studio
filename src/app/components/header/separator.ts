import { Components } from "tripetto";
import { IHeaderStyle } from "./style";

export class Separator<T> extends Components.ToolbarItem<T> {
    constructor(style: IHeaderStyle) {
        super(style.separator);
    }
}
