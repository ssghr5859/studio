import { Components, DOM, linearicon } from "tripetto";
import { IHeaderStyle } from "./style";

export class Toggle<T> extends Components.ToolbarButton<T> {
    private icon: number;

    constructor(props: { style: IHeaderStyle; icon: number; onToggle: () => void }) {
        super(props.style.toggle, undefined, undefined, () => props.onToggle());

        this.icon = props.icon;
    }

    onDraw(toolbar: Components.Toolbar<T>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.create("i", (icon: DOM.Element) => linearicon(this.icon, icon));
    }
}
