import { Components } from "tripetto";

export interface IResultsStyle {
    readonly list: Components.IListStyle;

    readonly buttons: {
        readonly export: Components.IToolbarMenuStyle;
        readonly refresh: Components.IToolbarItemStyle;
        readonly help: Components.IToolbarItemStyle;
        readonly delete: {
            withLabel: Components.IToolbarItemStyle;
            withoutLabel: Components.IToolbarItemStyle;
        };
    };
}
