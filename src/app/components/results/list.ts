import { Components, DEFAULT, Layers, pgettext } from "tripetto";
import { Result } from "./result";

export class ResultsList extends Components.List<Result> {
    constructor(
        parent: Layers.Layer,
        style: Components.IListStyle,
        onChange: (resultCount: number) => void,
        definitionId: string | undefined
    ) {
        super({
            parent,
            style,
            columns: [
                {
                    type: "selector",
                },
                {
                    property: "dateSubmitted",
                    type: "datetime",
                    name: pgettext("studio", "Date submitted"),
                    width: "40%",
                    alignment: "left",
                    sortable: true,
                    resizable: true,
                },
                {
                    property: "id",
                    type: "string",
                    name: pgettext("studio", "Identification"),
                    width: "60%",
                    alignment: "left",
                    sortable: true,
                    resizable: true,
                },
                {
                    property: "menu",
                    type: "button",
                    alignment: "center",
                    width: 54,
                    size: 20,
                    default: DEFAULT.forms.collection.assets.menu,
                    menu: (result: Result) => [
                        new Components.MenuLabel(pgettext("studio", "Result actions")),
                        new Components.MenuItemWithIcon(0xe6a5, pgettext("studio", "View"), () => result.open()),
                        new Components.MenuItemWithIcon(0xe681, pgettext("studio", "Delete"), () => result.confirmDelete()),
                    ],
                },
            ],
            sorting: "dateSubmitted",
            sortingDirection: "descending",
            emptyStateMessage: definitionId
                ? pgettext("studio", "There are no results collected yet...")
                : pgettext("studio", "This is where the results are collected. But you need to login if you want to use this feature."),
        });

        this.onAdd = () => onChange(this.count);
        this.onDelete = () => onChange(this.count);
    }
}
