import { StudioComponent } from "@app/components/studio";
import { Components, Forms, L10n, Layers, Str, castToNumber, each, pgettext } from "tripetto";
import { Result } from "./result";
import { IResponse } from "@server/entities/responses";
import { Export } from "tripetto-runner-foundation";
import { query } from "../../helpers/api";
import ReadSingleResponseQuery from "../../queries/responses/read-single.graphql";
import { Loader } from "../loader";
import { ResultsComponent } from ".";
import { URL } from "@app/globals";
import { ATTACHMENT } from "@server/endpoints";

export class ResultDetailComponent extends Components.Controller<{
    readonly result: Result;
    readonly exportables?: Export.IExportables;
}> {
    whenReady?: () => void;
    whenClosed?: () => void;

    static async open(results: ResultsComponent, result: Result): Promise<ResultDetailComponent | undefined> {
        Loader.show();

        const response = await query<IResponse>({
            query: ReadSingleResponseQuery,
            variables: { definitionId: result.definitionId, responseId: result.id },
            onError: StudioComponent.showApiError,
        });

        Loader.hide();

        if (response) {
            return results.createPanel(
                (panel: Layers.Layer) =>
                    new ResultDetailComponent(panel, results.studio, result, response.data ? JSON.parse(response.data) : undefined),
                Layers.Layer.configuration.width(600).animation(Layers.LayerAnimations.Zoom)
            );
        }

        return undefined;
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent, result: Result, exportables?: Export.IExportables) {
        super(
            layer,
            {
                result,
                exportables,
            },
            Str.capitalize(L10n.locale.dateTimeFull(result.dateSubmitted)),
            "compact",
            studio.style,
            "right",
            "always-on",
            pgettext("studio", "Close"),
            [
                new Components.ToolbarButton(studio.style.results.buttons.delete.withoutLabel, undefined, undefined, () =>
                    result.confirmDelete()
                ),
            ]
        );

        layer.hook("OnReady", "framed", () => {
            if (!result.list?.isMultipleSelect) {
                result.select();
            }

            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (!result.list?.isMultipleSelect) {
                result.deselect();
            }

            if (this.whenClosed) {
                this.whenClosed();
            }
        });

        result.hook("OnDelete", "synchronous", () => this.close(), this);
    }

    onCards(cards: Components.Cards): void {
        let fields = 0;

        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Identification number"),
                controls: [new Forms.Text("singleline", this.ref.result.id, 1).readonly()],
            })
        );

        each(this.ref.exportables?.fields, (field) => {
            if (field.string) {
                const controls: Forms.Control[] = [];

                switch (field.type) {
                    case "tripetto-block-file-upload":
                    case "file-upload":
                        controls.push(
                            new Forms.Text("singleline", field.string, 1).readonly(),
                            new Forms.Button(pgettext("studio", "Download"))
                                .url(URL + ATTACHMENT + "/" + (field.reference || ""))
                                .width(200)
                        );
                        break;
                    case "tripetto-block-date":
                        controls.push(
                            new Forms.Text(
                                "singleline",
                                field.string.indexOf(" ") !== -1
                                    ? L10n.locale.dateTimeFull(castToNumber(field.value), true)
                                    : L10n.locale.dateFull(castToNumber(field.value), true),
                                1
                            ).readonly()
                        );
                        break;
                    default:
                        controls.push(new Forms.Text("multiline", field.string, 1).readonly());
                        break;
                }

                cards.add(
                    new Forms.Form({
                        title: field.name,
                        controls,
                    })
                );

                fields++;
            }
        });

        if (fields === 0) {
            cards.add(
                new Forms.Form({
                    controls: [
                        new Forms.Notification(
                            pgettext(
                                "studio",
                                "This result does not contain any data. The submitted form did not contain any exportable fields."
                            ),
                            "warning"
                        ),
                    ],
                })
            );
        }
    }

    onDestroy(): void {
        this.ref.result.unhookContext(this);

        super.onDestroy();
    }
}
