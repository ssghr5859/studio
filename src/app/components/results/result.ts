import { StudioComponent } from "@app/components/studio";
import { Components, pgettext } from "tripetto";
import { IMutationResult } from "@server/entities/mutationresult";
import { mutate } from "../../helpers/api";
import DeleteResponseQuery from "../../queries/responses/delete.graphql";
import { ResultDetailComponent } from "./detail";
import { ResultsComponent } from "./index";
import { DialogComponent } from "@app/components/dialog";

export class Result extends Components.ListRow {
    private readonly results: ResultsComponent;
    private detail: ResultDetailComponent | undefined;

    @Components.identifier
    @Components.column
    public readonly id: string;

    @Components.column
    public readonly dateSubmitted: number;

    public readonly fingerprint: string;
    public readonly stencil: string;
    public readonly definitionId: string | undefined;

    constructor(results: ResultsComponent, id: string, dateSubmitted: number, fingerprint: string, stencil: string, definitionId?: string) {
        super();

        this.results = results;
        this.id = id;
        this.dateSubmitted = dateSubmitted;
        this.fingerprint = fingerprint;
        this.stencil = stencil;
        this.definitionId = definitionId;

        this.onTap(() => {
            this.open(true);
        });
    }

    async open(toggle: boolean = false): Promise<void> {
        if (this.list && this.list.isMultipleSelect) {
            this.isSelected = !this.isSelected;

            return;
        }

        if (!toggle || !this.isSelected) {
            this.detail = await ResultDetailComponent.open(this.results, this);
        } else if (toggle) {
            this.deselect();

            if (this.detail) {
                this.detail.close();

                this.detail = undefined;
            }
        }
    }

    confirmDelete(): void {
        DialogComponent.confirm(
            pgettext("studio", "Delete result"),
            pgettext("studio", "Are you sure you want to permanently delete this result?"),
            pgettext("studio", "Delete result"),
            pgettext("studio", "Cancel"),
            true,
            () => this.deleteFromServer()
        );
    }

    deleteFromServer(): void {
        mutate({
            query: DeleteResponseQuery,
            variables: { definitionId: this.definitionId, responseId: this.id },
            onError: StudioComponent.showApiError,
        }).then((mutationResult: IMutationResult) => {
            if (mutationResult.isSuccess) {
                this.delete();
            }
        });
    }
}
