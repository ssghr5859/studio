import { Components, Forms, Layers, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { DialogComponent } from "../dialog";

export class AccountComponent extends Components.Controller<StudioComponent> {
    whenClosed?: () => void;

    static open(studio: StudioComponent): AccountComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new AccountComponent(panel, studio),
            Layers.Layer.configuration.width(400).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent) {
        super(layer, studio, pgettext("studio", "Account"), "compact", studio.style, "right", "always-on", pgettext("studio", "Close"));

        layer.onClose = () => this.whenClosed && this.whenClosed();
    }

    onCards(cards: Components.Cards): void {
        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Email address"),
                controls: [new Forms.Email(this.ref.userAccount && this.ref.userAccount.emailAddress).readonly()],
            })
        );

        cards.add(
            new Forms.Form({
                controls: [
                    new Forms.Button(pgettext("studio", "Sign out"), "normal")
                        .width("auto")
                        .on(() =>
                            DialogComponent.confirm(
                                pgettext("studio", "Signing you out"),
                                pgettext("studio", "We like having you around and welcome you back anytime. Are you really leaving us?"),
                                pgettext("studio", "I'm outta here"),
                                pgettext("studio", "Cancel"),
                                false,
                                () => this.ref.signOut()
                            )
                        ),
                ],
            })
        );

        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Danger zone!"),
                controls: [
                    new Forms.Button(pgettext("studio", "Delete account"), "cancel")
                        .width("auto")
                        .on(() =>
                            DialogComponent.confirm(
                                pgettext("studio", "Deleting your account"),
                                pgettext(
                                    "studio",
                                    "Deletion is irreversable. Are you sure you want to permanently delete your account and everything under it?"
                                ),
                                pgettext("studio", "Delete account"),
                                pgettext("studio", "Cancel"),
                                true,
                                () => this.ref.deleteAccount()
                            )
                        ),
                ],
            })
        );
    }
}
