import { Components, Forms, Layers, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { URL } from "@app/globals";
import { TEMPLATE } from "@server/endpoints";
import { addMetric } from "@app/helpers/metric";
import { MetricEventType } from "@server/providers/metric";

export class ShareTemplateComponent extends Components.Controller<{
    readonly definitionId?: string;
    readonly token?: string;
}> {
    private readonly studio: StudioComponent;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(studio: StudioComponent, definitionId?: string, token?: string): ShareTemplateComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new ShareTemplateComponent(panel, studio, definitionId, token),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent, definitionId?: string, token?: string) {
        super(
            layer,
            {
                definitionId,
                token,
            },
            pgettext("studio", "Share design as template"),
            "compact",
            studio.style,
            "right",
            "always-on",
            pgettext("studio", "Close")
        );

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });

        this.studio = studio;
    }

    private addSharingMetric(event: MetricEventType): void {
        addMetric(event, "definition_template", this.ref.definitionId);
    }

    private copyToClipboard(source: Forms.Text, eventType: MetricEventType): Forms.Button {
        const label = pgettext("studio", "Copy to clipboard");

        return new Forms.Button(label).width(200).onClick((button: Forms.Button) => {
            button.disable();
            button.type("accept");
            button.label(pgettext("studio", "Copied!"));

            source.copyToClipboard();

            this.addSharingMetric(eventType);

            setTimeout(() => {
                button.enable();
                button.type("normal");
                button.label(label);
            }, 1000);
        });
    }

    onCards(cards: Components.Cards): void {
        const templateUrl = URL + TEMPLATE + "/" + this.ref.token;
        const urlText = new Forms.Text("singleline", templateUrl).readonly();

        cards.add(
            new Forms.Form({
                controls: this.ref.token
                    ? [
                          new Forms.Static(pgettext("studio", "Use the link below to share your template.")),
                          urlText,
                          this.copyToClipboard(urlText, "share_copy_link"),
                      ]
                    : [
                          new Forms.Static(
                              pgettext(
                                  "studio",
                                  "An account is required to host shared forms at Tripetto. Sign in or create an account with just your email address to do so."
                              )
                          ),
                          new Forms.Spacer("small"),
                          new Forms.Button(pgettext("studio", "Sign in or register")).on(() => this.studio.signIn()),
                      ],
            })
        );
    }
}
