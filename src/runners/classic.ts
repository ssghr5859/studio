import * as Runner from "tripetto-runner-classic";
import * as Sentry from "@sentry/browser";
import Services from "@services";

export { trackingTrustedType } from "./tracking";

export function run(
    token: string,
    filestoreUrl: string,
    trackers: ((
        type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
        definition: {
            readonly fingerprint: string;
            readonly name: string;
        },
        block?: {
            readonly id: string;
            readonly name: string;
        }
    ) => void)[],
    sentryDSN?: string
): void {
    if (sentryDSN) {
        Sentry.init({
            dsn: sentryDSN,
            environment:
                filestoreUrl.indexOf("localhost") !== -1
                    ? "development"
                    : filestoreUrl.indexOf("staging") !== -1
                    ? "staging"
                    : "production",
        });
    }

    const { definition, snapshot, styles, l10n, locale, translations, attachments, onSubmit, onPause, onReload } =
        Services.internal<Runner.IClassicSnapshot>({ token, filestoreUrl });

    Runner.run({
        display: "page",
        definition,
        snapshot,
        styles,
        l10n,
        locale,
        translations,
        attachments,
        onSubmit,
        onPause,
        onReload,
        onAction: (trackers.length > 0 && ((a, b, c) => trackers.forEach((tracker) => tracker(a, b, c)))) || undefined,
    });
}
