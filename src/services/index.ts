import Promise from "promise-polyfill";
import { Export, IDefinition, ISnapshot, Instance, L10n, TL10n, TStyles } from "tripetto-runner-foundation";
import { FILESTORE_URL, URL } from "./settings";
import { getDefinition, getL10n, getStyles } from "./definitions";
import { getSnapshot, saveSnapshot } from "./snapshots";
import { getLocale } from "./locales";
import { getTranslation } from "./translations";
import { submitResponse } from "./responses";
import { deleteFile, downloadFile, uploadFile } from "./files";

interface IAttachments {
    readonly get: (file: string) => Promise<Blob>;
    readonly put: (file: File, onProgress: (percentage: number) => void) => Promise<string>;
    readonly delete: (file: string) => Promise<void>;
}

// tslint:disable-next-line:no-default-export
export default class Services<Snapshot = unknown> {
    /** Static services instance (for backwards compatibility). */
    private static services?: Services;

    /** Specifies the token to send along with a server request.  */
    private token: string;

    /** Specifies the url of the server.  */
    private url: string;

    /** Specifices the url of the filestore. */
    private filestoreUrl: string;

    private static assert(): Services {
        if (!this.services) {
            throw new Error("Services are not initialized.");
        }

        return this.services;
    }

    /** Initialize a services instance.
     * @param props.token Specifies the token.
     * @param props.url Specifies the url of the server (optional).
     * @param props.filestoreUrl Specifies the url of the filestore service (optional).
     */
    static init<Snapshot = unknown>(props: { token: string; url?: string; filestoreUrl?: string }): Services<Snapshot> {
        return (Services.services = new Services<Snapshot>(props));
    }

    /** Creates an internal services instance. */
    static internal<Snapshot = unknown>(props?: { token?: string; filestoreUrl?: string }): Services<Snapshot> {
        return new Services<Snapshot>({ ...props, url: "" });
    }

    static get definition(): Promise<IDefinition | undefined> {
        return Services.assert().definition;
    }

    /** Fetches the style from the server. */
    static get style(): Promise<TStyles | undefined> {
        return Services.assert().styles;
    }

    static get onAttachment(): IAttachments {
        return Services.assert().attachments;
    }

    static onFinish(instance: Instance, language?: string, locale?: string): void {
        Services.assert().onSubmit(instance, language || "", locale || "");
    }

    /** Creates a new services instance.
     * @param props.token Specifies the token.
     * @param props.url Specifies the url of the server (optional).
     * @param props.filestoreUrl Specifies the url of the filestore service (optional).
     */
    constructor(props: { token?: string; url?: string; filestoreUrl?: string }) {
        this.token = props.token || "";
        this.url = typeof props.url === "string" ? props.url : URL;
        this.filestoreUrl = props.filestoreUrl || FILESTORE_URL;
    }

    /** Verifies whether the token is set. */
    private requireToken(): string {
        if (!this.token) {
            throw new Error("Token is required, but not available.");
        }

        return this.token;
    }

    /** Handles retrieving an attachment from the filestore.
     * @param file The file to be retrieved.
     */
    private onAttachmentGet(file: string): Promise<Blob> {
        return downloadFile(this.filestoreUrl, this.requireToken(), file);
    }

    /** Handles uploading an attachment to the filestore.
     * @param file The file to be uploaded.
     * @param onProgress Function for handling the progress of the file upload.
     */
    private onAttachmentPut(file: File, onProgress: (percentage: number) => void): Promise<string> {
        return uploadFile(this.filestoreUrl, this.requireToken(), file, onProgress);
    }

    /** Handles deleting an attachment from the filestore.
     * @param file The file to be deleted.
     */
    private onAttachmentDelete(file: string): Promise<void> {
        return deleteFile(this.filestoreUrl, this.requireToken(), file);
    }

    /** Fetches the definition from the server. */
    get definition(): Promise<IDefinition | undefined> {
        return getDefinition(this.url, this.requireToken());
    }

    /** Fetches the snapshot from the server. */
    get snapshot(): Promise<ISnapshot<Snapshot> | undefined> {
        return getSnapshot(this.url, this.requireToken());
    }

    /** Fetches the styles from the server. */
    get styles(): Promise<TStyles | undefined> {
        return getStyles(this.url, this.requireToken());
    }

    /** Fetches the l10n data from the server. */
    get l10n(): Promise<TL10n | undefined> {
        return getL10n(this.url, this.requireToken());
    }

    /** Retrieves the attachments handler. */
    get attachments(): IAttachments {
        return {
            get: (file: string) => this.onAttachmentGet(file),
            put: (file: File, onProgress: (percentage: number) => void) => this.onAttachmentPut(file, onProgress),
            delete: (file: string) => this.onAttachmentDelete(file),
        };
    }

    /** Retrieves locale data from the server. */
    get locale(): (locale: "auto" | string) => Promise<L10n.ILocale | undefined> {
        return (locale: "auto" | string) => getLocale(this.url, locale);
    }

    /** Retrieves translations from the server. */
    get translations(): (language: "auto" | string, context: string) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined> {
        return (language: "auto" | string, context: string) => getTranslation(this.url, language, context);
    }

    /** Saves submitted responses to the server. */
    get onSubmit(): (instance: Instance, language: string, locale: string, runner?: string) => Promise<string | undefined> {
        return (instance: Instance, language: string, locale: string, runner?: string) =>
            submitResponse(
                this.url,
                this.requireToken(),
                Export.exportables(instance),
                Export.actionables(instance),
                language,
                locale,
                runner || ""
            );
    }

    /** Saves the snapshot to the server. */
    get onPause(): {
        recipe: "email";
        onPause: (emailAddress: string, snapshot: ISnapshot<Snapshot>, language: string, locale: string, runner: string) => Promise<void>;
    } {
        return {
            recipe: "email",
            onPause: (emailAddress: string, snapshot: ISnapshot<Snapshot>, language: string, locale: string, runner: string) =>
                saveSnapshot(this.url, this.requireToken(), snapshot, emailAddress, language, locale, runner),
        };
    }

    /** Reloads the definition from the server. */
    get onReload(): () => Promise<IDefinition> {
        return () =>
            new Promise((resolve: (definition: IDefinition) => void, reject: () => void) => {
                getDefinition(this.url, this.requireToken())
                    .then((definition) => {
                        if (definition) {
                            resolve(definition);
                        } else {
                            reject();
                        }
                    })
                    .catch(() => reject());
            });
    }
}
