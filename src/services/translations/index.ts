import Promise from "promise-polyfill";
import * as Superagent from "superagent";
import { L10n } from "tripetto-runner-foundation";
import { TRANSLATION } from "@server/endpoints";

const translationCache: {
    [language: string]: L10n.TTranslation | L10n.TTranslation[] | undefined;
} = {};

/**
 * Saves a responses to the server.
 * @param url Specifies the url.
 * @param language Specifies the language of the translation.
 */
export const getTranslation = (url: string, language?: string | "auto", context?: string) =>
    new Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>(
        (resolve: (translation: L10n.TTranslation | L10n.TTranslation[] | undefined) => void) => {
            let querystring = "";

            if (language && language !== "auto") {
                querystring += (querystring === "" ? "?" : "&") + `l=${encodeURIComponent(language)}`;
            }

            if (context) {
                querystring += (querystring === "" ? "?" : "&") + `c=${encodeURIComponent(context)}`;
            }

            const uri = url + TRANSLATION + querystring;
            const cache = translationCache[uri];

            if (!cache) {
                Superagent.get(url + TRANSLATION + querystring)
                    .send()
                    .then((response) => resolve((translationCache[uri] = response.body)))
                    .catch(() => resolve(undefined));
            } else {
                resolve(cache);
            }
        }
    );
