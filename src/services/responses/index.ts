import Promise from "promise-polyfill";
import * as Superagent from "superagent";
import { Export, castToNumber, castToString, checksum, findFirst, powDuration, powHashRate, powSolve } from "tripetto-runner-foundation";
import { HEADER_RUNNER_TOKEN } from "@server/headers";
import { RESPONSE_ANNOUNCE, RESPONSE_SUBMIT } from "@server/endpoints";

/**
 * Submits a response to the server.
 * @param url Specifies the url.
 * @param token The token that represents the definition.
 * @param exportables The response data.
 * @param language The language identifier.
 * @param locale The locale identifier.
 */
export const submitResponse = (
    url: string,
    token: string,
    exportables: Export.IExportables,
    actionables: Export.IActionables | undefined,
    language: string,
    locale: string,
    runner: string
) =>
    new Promise<string | undefined>((resolve: (id: string | undefined) => void, reject: (reason?: string) => void) => {
        /**
         * If we have no fields or only fields without data, we don't need to
         * post that data to the server. It's simply a form without data. We
         * don't handle that.
         */
        if (!actionables && !findFirst(exportables.fields, (field) => (field.time ? true : false))) {
            return resolve(undefined);
        }

        const payloadChecksum = checksum({ exportables, actionables }, true);

        if (payloadChecksum) {
            Superagent.post(url + RESPONSE_ANNOUNCE)
                .set(HEADER_RUNNER_TOKEN, token)
                .send({
                    fingerprint: exportables.fingerprint,
                    checksum: payloadChecksum,
                    runner,
                })
                .then((announcement) => {
                    if (announcement.status === 200 && announcement.body && announcement.body.id && announcement.body.difficulty) {
                        try {
                            const id = castToString(announcement.body.id);
                            const difficulty = castToNumber(announcement.body.difficulty);
                            const timestamp = castToNumber(announcement.body.timestamp);
                            const nonce = powSolve({ exportables, actionables }, difficulty, id, 16, 1000 * 60 * 5, timestamp);

                            Superagent.post(url + RESPONSE_SUBMIT)
                                .set(HEADER_RUNNER_TOKEN, token)
                                .send({
                                    id,
                                    nonce,
                                    powDuration: powDuration(),
                                    powHashRate: powHashRate(),
                                    language,
                                    locale,
                                    exportables,
                                    actionables,
                                })
                                .then((result) => {
                                    if (result.status === 200 && result.text) {
                                        resolve(result.text);
                                    } else {
                                        reject(result.status === 403 ? "rejected" : (result.error && result.error.text) || undefined);
                                    }
                                })
                                .catch((result) => reject((result && result.status === 403 && "rejected") || undefined));
                        } catch {
                            reject();
                        }
                    } else {
                        reject(announcement.status === 409 ? "outdated" : (announcement.error && announcement.error.text) || undefined);
                    }
                })
                .catch((result) => reject((result && result.status === 409 && "outdated") || undefined));
        } else {
            reject();
        }
    });
