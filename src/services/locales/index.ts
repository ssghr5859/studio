import Promise from "promise-polyfill";
import * as Superagent from "superagent";
import { L10n } from "tripetto-runner-foundation";
import { LOCALE } from "@server/endpoints";

const localeCache: {
    [locale: string]: L10n.ILocale | undefined;
} = {};

/**
 * Saves a responses to the server.
 * @param url Specifies the url.
 * @param language Specifies the locale.
 */
export const getLocale = (url: string, locale?: "auto" | string) =>
    new Promise<L10n.ILocale | undefined>((resolve: (locale: L10n.ILocale | undefined) => void) => {
        const uri = url + LOCALE + (locale && locale !== "auto" ? `?l=${encodeURIComponent(locale)}` : "");
        const cache = localeCache[uri];

        if (!cache) {
            Superagent.get(uri)
                .send()
                .then((response: Superagent.Response) => resolve((localeCache[uri] = response.body)))
                .catch(() => resolve(undefined));
        } else {
            resolve(cache);
        }
    });
