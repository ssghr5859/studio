# <a href="https://tripetto.com/"><img src="https://unpkg.com/tripetto/assets/banner.svg" alt="Tripetto"></a>

Tripetto is a full-fledged form kit. Rapidly build and run smart flowing forms and surveys. Drop the kit in your codebase and use all of it or just the parts you need. The visual [**builder**](https://www.npmjs.com/package/tripetto) is for form building, and the [**runners**](https://www.npmjs.com/package/tripetto-runner-foundation) are for running those forms in different UI variants. It is entirely extendible and customizable. Anyone can build their own building [**blocks**](https://docs.tripetto.com/guide/blocks) (e.g., question types) or runner UI's.

# Services package
[![Version](https://img.shields.io/npm/v/tripetto-services.svg)](https://www.npmjs.com/package/tripetto-services)
[![License](https://img.shields.io/npm/l/tripetto-services.svg)](https://opensource.org/licenses/MIT)
[![Downloads](https://img.shields.io/npm/dt/tripetto-services.svg)](https://www.npmjs.com/package/tripetto-services)
[![Pipeline status](https://gitlab.com/tripetto/studio/badges/production/pipeline.svg)](https://gitlab.com/tripetto/studio/commits/production)
[![Join the community on Spectrum](https://withspectrum.github.io/badge/badge.svg)](https://spectrum.chat/tripetto)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

The services package handles communications between the runner and the [Tripetto Studio](https://tripetto.app) if you choose to host the form definition or collected data – or both – at Tripetto. It contains helpers for the implementation on your end of the form definition and collected data store at the Tripetto platform.

# Get started
To use the library in your project you should install the Tripetto Services package as a dependency using the following command:

```
$ npm install tripetto-services --save
```

# Example
The following example uses the [tripetto-runner-autoscroll](https://www.npmjs.com/package/tripetto-runner-autoscroll) package for the runner and uses the services package to store form results in the Tripetto Studio.

```
import { run } from "tripetto-runner-autoscroll";
import Services from "tripetto-services";

const { definition, onSubmit } = Services.init({
    token: "a-valid-tripetto-token"
});

run({
    element: document.body,
    definition,
    onSubmit
});
```

# Documentation
The complete Tripetto documentation can be found at [docs.tripetto.com](https://docs.tripetto.com).

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/studio/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
