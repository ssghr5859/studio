import Promise from "promise-polyfill";
import * as Superagent from "superagent";
import { ISnapshot } from "tripetto-runner-foundation";
import { HEADER_RUNNER_TOKEN } from "@server/headers";
import { SNAPSHOT } from "@server/endpoints";
import { noCacheUrl } from "../no-cache";

/**
 * Gets a snapshot from the server.
 * @param token The token that represents the snapshot.
 */
export const getSnapshot = <T>(url: string, token: string) =>
    new Promise<ISnapshot<T> | undefined>((resolve: (snapshot: ISnapshot<T> | undefined) => void) => {
        Superagent.get(noCacheUrl(url + SNAPSHOT, token))
            .set(HEADER_RUNNER_TOKEN, token)
            .set("Cache-Control", "no-store")
            .then((res) => resolve((res.status === 200 && res.body) || undefined))
            .catch(() => resolve(undefined));
    });

/**
 * Saves a snapshot to the server.
 * @param token The token that represents the definition.
 * @param snapshot The snapshot data.
 * @param emailAddress The email address of the person that wants to save the snapshot
 */
export const saveSnapshot = <T>(
    url: string,
    token: string,
    snapshot: ISnapshot<T>,
    emailAddress: string,
    language: string,
    locale: string,
    runner: string
) =>
    new Promise<void>((resolve: () => void, reject: (reason?: string) => void) => {
        Superagent.post(url + SNAPSHOT)
            .set(HEADER_RUNNER_TOKEN, token)
            .send({
                emailAddress,
                snapshot,
                language,
                locale,
                runner,
            })
            .then((res) => {
                if (res.status === 200) {
                    resolve();
                } else {
                    reject((res.error && res.error.text) || undefined);
                }
            })
            .catch(() => reject());
    });
