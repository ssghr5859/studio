import Promise from "promise-polyfill";
import * as Superagent from "superagent";
import { IDefinition, TL10n, TStyles } from "tripetto-runner-foundation";
import { noCacheUrl } from "../no-cache";
import { HEADER_RUNNER_TOKEN } from "@server/headers";
import { DEFINITION, L10N, STYLES } from "@server/endpoints";

/**
 * Gets a definition from the server.
 * @param url Specifies the base url.
 * @param token The token that represents the definition.
 */
export const getDefinition = (url: string, token: string) =>
    new Promise<IDefinition | undefined>((resolve: (definition: IDefinition | undefined) => void) => {
        Superagent.get(noCacheUrl(url + DEFINITION, token))
            .set(HEADER_RUNNER_TOKEN, token)
            .set("Cache-Control", "no-store")
            .then((response: Superagent.Response) => resolve(response.body?.definition))
            .catch(() => resolve(undefined));
    });

/**
 * Gets a style from the server.
 * @param url Specifies the base url.
 * @param token The token that represents the definition the styles belongs to.
 */
export const getStyles = (url: string, token: string) =>
    new Promise<TStyles | undefined>((resolve: (styles: TStyles | undefined) => void) => {
        Superagent.get(noCacheUrl(url + STYLES, token))
            .set(HEADER_RUNNER_TOKEN, token)
            .set("Cache-Control", "no-store")
            .then((response: Superagent.Response) => resolve(response.body?.styles))
            .catch(() => resolve(undefined));
    });

/**
 * Gets a l10n data from the server.
 * @param url Specifies the base url.
 * @param token The token that represents the definition the l10n data belongs to.
 */
export const getL10n = (url: string, token: string) =>
    new Promise<TL10n | undefined>((resolve: (l10n: TL10n | undefined) => void) => {
        Superagent.get(noCacheUrl(url + L10N, token))
            .set(HEADER_RUNNER_TOKEN, token)
            .set("Cache-Control", "no-store")
            .then((response: Superagent.Response) => resolve(response.body?.l10n))
            .catch(() => resolve(undefined));
    });
