/** Dependencies */
import { sync as osLocale } from "os-locale";
import * as FS from "fs";
import * as Path from "path";

/** Verifies if a translation exists. */
function verify(locale: string, context?: string): string {
    if (!locale || !/^[A-Za-z]{2,4}([_-]([A-Za-z]{4}|[0-9]{3}))?([_-]([A-Za-z]{2}|[0-9]{3}))?$/.test(locale)) {
        return "";
    }

    const sLocales = Path.join(__dirname, "../../translations" + (context ? "/" + context : ""));
    let sPath = Path.join(sLocales, `${locale}.json`);

    if (FS.existsSync(sPath)) {
        return sPath;
    }

    sPath = Path.join(sLocales, `${locale.substr(0, locale.indexOf("_"))}.json`);

    if (FS.existsSync(sPath)) {
        return sPath;
    }

    return "";
}

/** Retrieves the path to the translation. */
export function translationPath(locale: string = "", fallback: boolean, context?: string): string {
    if (context && !/^[a-z-]+$/.test(context)) {
        return "";
    }

    if (locale === "en") {
        return "";
    }

    if (locale !== "") {
        locale = locale.replace("-", "_");

        if (locale.indexOf(";") !== -1) {
            locale = locale.substr(0, locale.indexOf(";"));
        }

        if (locale.indexOf(",") !== -1) {
            locale = locale.substr(0, locale.indexOf(","));
        }

        const sPath = verify(locale, context);

        if (sPath !== "") {
            return sPath;
        }
    }

    if (!fallback) {
        return "";
    }

    if (locale.indexOf("en_") === 0) {
        return "";
    }

    try {
        locale = osLocale();
    } catch {
        return "";
    }

    return verify(locale, context);
}
