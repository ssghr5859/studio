import { createHash as _createHash } from "crypto";

export function createHash(data: string): string {
    const hash = _createHash("sha256");
    hash.update(data);
    return hash.digest("base64");
}
