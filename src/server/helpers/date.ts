import { Export, castToNumber } from "tripetto-runner-foundation";
import { getLocale } from "../helpers/locales";

export function isDate(field: Export.IExportableField): boolean {
    return field.type === "tripetto-block-date";
}

export function formatDate(field: Export.IExportableField, type: "short" | "full", locale?: string): string {
    if (field.string) {
        switch (type) {
            case "short":
                return field.string.indexOf(" ") !== -1
                    ? getLocale(locale).dateTimeShort(castToNumber(field.value), true, locale)
                    : getLocale(locale).dateShort(castToNumber(field.value), true, locale);
            case "full":
                return field.string.indexOf(" ") !== -1
                    ? getLocale(locale).dateTimeFull(castToNumber(field.value), true, locale)
                    : getLocale(locale).dateFull(castToNumber(field.value), true, locale);
        }
    }

    return "";
}
