import { TAny, TL10n, TStyles } from "tripetto-runner-foundation";

function convertFont(font: string): string | undefined {
    switch (font) {
        case "Arial, Helvetica, Sans-Serif":
            return "Arial";
        case "Arial Black, Gadget, Sans-Serif":
            return "Arial Black";
        case "Comic Sans MS, Textile, Cursive, Sans-Serif":
            return "Comic Sans MS";
        case "Courier New, Courier, Monospace":
            return "Courier New";
        case "Georgia, Times New Roman, Times, Serif":
            return "Georgia";
        case "Impact, Charcoal, Sans-Serif":
            return "Impact";
        case "Lucida Console, Monaco, Monospace":
            return "Lucida Console";
        case "Lucida Sans Unicode, Lucida Grande, Sans-Serif":
            return "Lucida Sans Unicode";
        case "Palatino Linotype, Book Antiqua, Palatino, Serif":
            return "Palatino";
        case "Tahoma, Geneva, Sans-Serif":
            return "Tahoma";
        case "Times New Roman, Times, Serif":
            return "Times New Roman";
        case "Trebuchet MS, Helvetica, Sans-Serif":
            return "Trebuchet MS";
        case "Verdana, Geneva, Sans-Serif":
            return "Verdana";
    }

    return undefined;
}

function convertBootstrapStyles(
    style:
        | "primary"
        | "secondary"
        | "success"
        | "info"
        | "warning"
        | "danger"
        | "light"
        | "dark"
        | "outline-primary"
        | "outline-secondary"
        | "outline-success"
        | "outline-info"
        | "outline-warning"
        | "outline-danger"
        | "outline-light"
        | "outline-dark"
        | string,
    isBorder: boolean = false
): string | undefined {
    switch (style) {
        case "primary":
        case "outline-primary":
            return "#007bff";
        case "secondary":
        case "outline-secondary":
            return "#6c757d";
        case "success":
        case "outline-success":
            return "#28a745";
        case "info":
        case "outline-info":
            return "#17a2b8";
        case "warning":
        case "outline-warning":
            return "#ffc107";
        case "danger":
        case "outline-danger":
            return "#dc3545";
        case "light":
        case "outline-light":
            return isBorder ? "#ffffff" : "#f8f9fa";
        case "dark":
        case "outline-dark":
            return "#343a40";
    }

    return undefined;
}

export function migrateStyles(styles: TAny = {}): TStyles {
    if (!styles.contract) {
        const migratedStyles: TAny = {
            contract: {
                name: "tripetto-collector-rolling",
                version: "0.0.0",
            },
        };

        migratedStyles.color = styles.textColor;

        if (styles.textFont) {
            migratedStyles.font = {
                family: convertFont(styles.textFont),
            };
        }

        if (styles.backgroundColor || styles.backgroundImage) {
            migratedStyles.background = {
                color: styles.backgroundColor,
                url: styles.backgroundImage?.url,
                positioning: styles.backgroundImage?.size,
            };
        }

        if (typeof styles.showNavigation === "boolean") {
            migratedStyles.showNavigation = styles.showNavigation ? "auto" : "never";
        }

        if (typeof styles.showProgressbar === "boolean") {
            migratedStyles.showProgressbar = styles.showProgressbar;
        }

        if (typeof styles.showEnumerators === "boolean") {
            migratedStyles.showEnumerators = styles.showEnumerators;
        }

        if (typeof styles.showScrollbar === "boolean") {
            migratedStyles.showScrollbar = styles.showScrollbar;
        }

        if (typeof styles.autoFocus === "boolean") {
            migratedStyles.autoFocus = styles.autoFocus;
        }

        if (typeof styles.centerActiveBlock === "boolean") {
            migratedStyles.verticalAlignment = styles.centerActiveBlock ? "middle" : "top";
        }

        if (typeof styles.form === "object") {
            migratedStyles.inputs = {
                backgroundColor: "#fff",
                borderColor: convertBootstrapStyles(styles.form.inputStyle || "", true),
                borderSize: 1,
                textColor: "#000",
                errorColor: "#f00",
                agreeColor: convertBootstrapStyles(styles.form.positiveStyle || ""),
                declineColor: convertBootstrapStyles(styles.form.negativeStyle || ""),
                selectionColor: convertBootstrapStyles(styles.form.selectedStyle || ""),
            };
        }

        if (typeof styles.buttons === "object") {
            migratedStyles.buttons = {
                baseColor: convertBootstrapStyles(styles.buttons.okStyle || ""),
                mode: (styles.buttons.okStyle || "").indexOf("outline-") === -1 ? "fill" : "outline",
                finishColor: convertBootstrapStyles(styles.buttons.completeStyle || ""),
            };
        }

        if (typeof styles.footer === "object") {
            migratedStyles.navigation = {
                backgroundColor: styles.footer.backgroundColor,
                textColor: styles.footer.textColor,
                progressbarColor: convertBootstrapStyles(styles.footer.progressbarStyle),
            };
        }

        return migratedStyles;
    }

    return styles as TStyles;
}

export function migrateL10n(l10n: TL10n, styles: TAny = {}): TL10n {
    if (
        (!l10n || !l10n.contract) &&
        typeof styles.buttons === "object" &&
        (styles.buttons.okLabel || styles.buttons.backLabel || styles.buttons.completeLabel)
    ) {
        const migratedL10n: TAny = {
            language: "en",
            translations: {
                "": {
                    language: "en",
                },
            },
        };

        if (styles.buttons.okLabel) {
            // tslint:disable-next-line: no-null-keyword
            migratedL10n.translations["runner#1|🆗 Buttons\u0004Next"] = [null, styles.buttons.okLabel];
        }

        if (styles.buttons.backLabel) {
            // tslint:disable-next-line: no-null-keyword
            migratedL10n.translations["runner#1|🆗 Buttons\u0004Back"] = [null, styles.buttons.backLabel];
        }

        if (styles.buttons.completeLabel) {
            // tslint:disable-next-line: no-null-keyword
            migratedL10n.translations["runner#1|🆗 Buttons\u0004Submit"] = [null, styles.buttons.completeLabel];
        }

        return migratedL10n;
    }

    return l10n as TL10n;
}
