import { Str } from "tripetto-runner-foundation";
import { ITrackers } from "../entities/definitions/interface";

/**
 * Generates the tracker code.
 * @param trackers Reference to the trackers object.
 * @param allowCustomCode Specifies if custom code is allowed.
 * @param trustedType Specifies if trusted types should be used.
 * @returns Returns the tracker codes or undefined if no trackers are active.
 */
export function getTrackerCode(trackers: ITrackers, allowCustomCode: boolean, trustedType: boolean): string | undefined {
    const formProps = ", form: form.name, fingerprint: form.fingerprint";
    const blockProps = ", block: block.name, key: block.id";
    let code = "";

    if (trackers.ga?.enabled) {
        const useGlobal = allowCustomCode && trackers.ga.useGlobal === true;

        if (useGlobal || trackers.ga.id) {
            const useGTM = useGlobal || trackers.ga.id.toUpperCase().indexOf("GTM-") !== -1;

            code += `${code === "" ? "" : ","}(function() {`;

            if (!useGlobal) {
                if (useGTM) {
                    code +=
                        "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=" +
                        (trustedType ? "Tripetto.trackingTrustedType(" : "") +
                        "'https://www.googletagmanager.com/gtm.js?id='+i+dl" +
                        (trustedType ? ")" : "") +
                        ";var n=d.querySelector('[nonce]');n&&j.setAttribute('nonce',n.nonce||n.getAttribute('nonce'));f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','" +
                        encodeURIComponent(trackers.ga.id) +
                        "');";
                } else {
                    code +=
                        "(function(d,s,u,e,h){e=d.createElement(s);h=d.getElementsByTagName(s)[0];e.async=1;e.src=" +
                        (trustedType ? "Tripetto.trackingTrustedType(u)" : "u") +
                        ";h.parentNode.insertBefore(e,h)})(document,'script','https://www.googletagmanager.com/gtag/js?id=" +
                        encodeURIComponent(trackers.ga.id) +
                        "&l=tripettoGTAG');";
                    code += "window.tripettoGTAG = window.tripettoGTAG || [];";
                    code += "function gtag(){tripettoGTAG.push(arguments);}";
                    code += "gtag('js', new Date());";
                    code += "gtag('config', '" + encodeURIComponent(trackers.ga.id) + "');";
                }
            }

            if (useGTM) {
                code += "window.dataLayer = window.dataLayer || [];";
            }

            code += "return function(event, form, block) {";
            code += "switch(event){";

            if (useGTM) {
                if (trackers.ga.trackStart) {
                    code +=
                        "case 'start': dataLayer.push({ event: 'tripetto_start', description: 'Form is started.'" +
                        formProps +
                        " }); break;";
                }

                if (trackers.ga.trackStage) {
                    code +=
                        "case 'stage': dataLayer.push({ event: 'tripetto_stage', description: 'Form block becomes available.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackUnstage) {
                    code +=
                        "case 'unstage': dataLayer.push({ event: 'tripetto_unstage', description: 'Form block becomes unavailable.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackFocus) {
                    code +=
                        "case 'focus': dataLayer.push({ event: 'tripetto_focus', description: 'Form input element gained focus.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackBlur) {
                    code +=
                        "case 'blur': dataLayer.push({ event: 'tripetto_blur', description: 'Form input element lost focus.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackPause) {
                    code +=
                        "case 'pause': dataLayer.push({ event: 'tripetto_pause', description: 'Form is paused.'" +
                        formProps +
                        " }); break;";
                }

                if (trackers.ga.trackComplete) {
                    code +=
                        "case 'complete': dataLayer.push({ event: 'tripetto_complete', description: 'Form is completed.'" +
                        formProps +
                        " }); break;";
                }
            } else {
                if (trackers.ga.trackStart) {
                    code += "case 'start': gtag('event', 'tripetto_start', { description: 'Form is started.'" + formProps + " }); break;";
                }

                if (trackers.ga.trackStage) {
                    code +=
                        "case 'stage': gtag('event', 'tripetto_stage', { description: 'Form block becomes available.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackUnstage) {
                    code +=
                        "case 'unstage': gtag('event', 'tripetto_unstage', { description: 'Form block becomes unavailable.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackFocus) {
                    code +=
                        "case 'focus': gtag('event', 'tripetto_focus', { description: 'Form input element gained focus.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackBlur) {
                    code +=
                        "case 'blur': gtag('event', 'tripetto_blur', { description: 'Form input element lost focus.'" +
                        formProps +
                        blockProps +
                        " }); break;";
                }

                if (trackers.ga.trackPause) {
                    code += "case 'pause': gtag('event', 'tripetto_pause', { description: 'Form is paused.'" + formProps + " }); break;";
                }

                if (trackers.ga.trackComplete) {
                    code +=
                        "case 'complete': gtag('event', 'tripetto_complete', { description: 'Form is completed.'" +
                        formProps +
                        " }); break;";
                }
            }

            code += "}";
            code += "}";
            code += "})()";
        }
    }

    if (trackers.fb?.enabled) {
        const useGlobal = allowCustomCode && trackers.fb.useGlobal === true;

        if (useGlobal || trackers.fb.id) {
            code += `${code === "" ? "" : ","}(function() {`;

            if (!useGlobal) {
                code +=
                    "!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=" +
                    (trustedType ? "Tripetto.trackingTrustedType(v)" : "v") +
                    ";s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');";
                code += "fbq('init', '" + encodeURIComponent(trackers.fb.id) + "');";
                code += "fbq('track', 'PageView');";
            }

            code += "return function(event, form, block) {";
            code += "switch(event){";

            if (trackers.fb.trackStart) {
                code += "case 'start': fbq('trackCustom', 'TripettoStart', { description: 'Form is started.'" + formProps + " }); break;";
            }

            if (trackers.fb.trackStage) {
                code +=
                    "case 'stage': fbq('trackCustom', 'TripettoStage', { description: 'Form block becomes available.'" +
                    formProps +
                    blockProps +
                    " }); break;";
            }

            if (trackers.fb.trackUnstage) {
                code +=
                    "case 'unstage': fbq('trackCustom', 'TripettoUnstage', { description: 'Form block becomes unavailable.'" +
                    formProps +
                    blockProps +
                    " }); break;";
            }

            if (trackers.fb.trackFocus) {
                code +=
                    "case 'focus': fbq('trackCustom', 'TripettoFocus', { description: 'Form input element gained focus.'" +
                    formProps +
                    blockProps +
                    " }); break;";
            }

            if (trackers.fb.trackBlur) {
                code +=
                    "case 'blur': fbq('trackCustom', 'TripettoBlur', { description: 'Form input element lost focus.'" +
                    formProps +
                    blockProps +
                    " }); break;";
            }

            if (trackers.fb.trackPause) {
                code += "case 'pause': fbq('trackCustom', 'TripettoPause', { description: 'Form is paused.'" + formProps + " }); break;";
            }

            if (trackers.fb.trackComplete) {
                code +=
                    "case 'complete': fbq('trackCustom', 'TripettoComplete', { description: 'Form is completed.'" +
                    formProps +
                    " }); break;";
            }

            code += "}";
            code += "}";
            code += "})()";
        }
    }

    if (allowCustomCode && trackers.custom?.enabled) {
        const customCode = Str.replaceMultiple(trackers.custom.code, ["\n", "\r", "\t", " "], "");

        if (customCode.indexOf("function(){") === 0 && customCode.charAt(customCode.length - 1) === "}") {
            code += `${code === "" ? "" : ","}(${trackers.custom.code})()`;
        }
    }

    return (code && `[${code}]`) || undefined;
}
