import { Export } from "tripetto-runner-foundation";
import { ATTACHMENT } from "../endpoints";
import { URL } from "../settings";

export function isAttachment(field: Export.IExportableField): boolean {
    return field.type === "tripetto-block-file-upload" || field.type === "file-upload";
}

export function attachmentUrl(field: Export.IExportableField): string {
    return (field.reference && `${URL}${ATTACHMENT}/${field.reference || ""}`) || "";
}
