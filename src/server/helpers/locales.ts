import { L10n } from "tripetto-runner-foundation";
import { sync as osLocale } from "os-locale";
import * as FS from "fs";
import * as Path from "path";

/** Verifies if a locale exists. */
function verify(locale: string): {
    readonly locale: string;
    readonly path: string;
} {
    if (!locale || !/^[A-Za-z]{2,4}([-]([A-Za-z]{4}|[0-9]{3}))?([-]([A-Za-z]{2}|[0-9]{3}))?$/.test(locale)) {
        return {
            locale: "",
            path: "",
        };
    }

    const locales = Path.join(__dirname, "../../locales");
    let path = Path.join(locales, `${locale}.json`);

    if (!FS.existsSync(path)) {
        locale = locale.substr(0, locale.indexOf("-"));
        path = Path.join(locales, `${locale}.json`);

        if (!FS.existsSync(path)) {
            return {
                locale: "",
                path: "",
            };
        }
    }

    return {
        locale,
        path,
    };
}

/** Retrieves the path to the locale profile. */
export function localePath(locale: string = ""): string {
    if (locale !== "") {
        locale = locale.replace("_", "-");

        if (locale.indexOf(";") !== -1) {
            locale = locale.substr(0, locale.indexOf(";"));
        }

        if (locale.indexOf(",") !== -1) {
            locale = locale.substr(0, locale.indexOf(","));
        }

        const result = verify(locale);

        if (result.path !== "") {
            return result.path;
        }
    }

    try {
        locale = osLocale().replace("_", "-") || "en";
    } catch {
        locale = "en";
    }

    return verify(locale).path;
}

export function localeIdentifier(locale: string = ""): string {
    if (locale !== "") {
        locale = locale.replace("_", "-");

        if (locale.indexOf(";") !== -1) {
            locale = locale.substr(0, locale.indexOf(";"));
        }

        if (locale.indexOf(",") !== -1) {
            locale = locale.substr(0, locale.indexOf(","));
        }

        const result = verify(locale);

        if (result.locale !== "") {
            return result.locale;
        }
    }

    try {
        locale = osLocale().replace("_", "-") || "en";
    } catch {
        locale = "en";
    }

    return verify(locale).locale;
}

export function getLocale(locale?: string): typeof L10n.Locales {
    if (locale && !L10n.Locales.isLoaded(locale)) {
        const path = localePath(locale);

        if (path) {
            try {
                const localeData = FS.readFileSync(path, "utf8");

                if (localeData) {
                    L10n.Locales.load(JSON.parse(localeData), false);
                }
                // tslint:disable-next-line: no-empty
            } catch {}
        }
    }

    return L10n.Locales;
}
