export const ENV =
    process.env.TRIPETTO_APP_ENV === "production" ? "production" : process.env.TRIPETTO_APP_ENV === "staging" ? "staging" : "development";

if (!process.env.TRIPETTO_APP_URL) {
    throw new Error("Tripetto Studio: The environment variable 'TRIPETTO_APP_URL' is not defined.");
}

export const URL = process.env.TRIPETTO_APP_URL;

if (!process.env.TRIPETTO_APP_PK) {
    throw new Error("Tripetto Studio: The environment variable 'TRIPETTO_APP_PK' is not defined.");
}

export const TRIPETTO_APP_PK = process.env.TRIPETTO_APP_PK;

if (!process.env.TRIPETTO_APP_SENDGRID) {
    throw new Error("Tripetto Studio: The environment variable 'TRIPETTO_APP_SENDGRID' is not defined.");
}

export const SENDGRID_API_KEY = process.env.TRIPETTO_APP_SENDGRID;

if (!process.env.GOOGLE_CLOUD_PROJECT) {
    throw new Error("Tripetto Studio: The environment variable 'GOOGLE_CLOUD_PROJECT' is not defined.");
}

export const GOOGLE_CLOUD_PROJECT = process.env.GOOGLE_CLOUD_PROJECT;

if (!process.env.TRIPETTO_FILESTORE_URL) {
    throw new Error("Tripetto Studio: The environment variable 'TRIPETTO_FILESTORE_URL' is not defined.");
}

export const TRIPETTO_FILESTORE_URL = process.env.TRIPETTO_FILESTORE_URL;

export const EMAIL_FROM_ADDRESS = "no-reply@tripetto.com";
export const EMAIL_FROM_NAME = "Tripetto";

export const SESSION_COOKIE_NAME = "session";

export const WRITE_FILE_RETRY_TRESHOLD = 3;

export const TOKEN_LOGIN_EXPIRES_IN = "15m";
export const TOKEN_AUTH_EXPIRES_IN = "31d";
export const TOKEN_FILE_EXPIRES_IN = "1m";
export const TOKEN_REFRESH_INTERVAL_IN_SECONDS = 3600;
export const TOKEN_COOKIE_NAME = "token";

export const MAXIMUM_REQUEST_SIZE = "25mb";

export const SHORT_ID_SIZE = 10;

export const SENTRY_DSN_SERVER = process.env.SENTRY_DSN_SERVER || "";
export const SENTRY_DSN_WEBAPP = process.env.SENTRY_DSN_WEBAPP || "";
export const SENTRY_DSN_RUNNER = process.env.SENTRY_DSN_RUNNER || "";
