import { GraphQLResolveInfo } from "graphql";
import { Arg, Authorized, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { inject, injectable } from "inversify";
import { logProviderSymbol, metricProviderSymbol, workspaceProviderSymbol } from "../../symbols";
import { ILogProvider, IMetricProvider, IWorkspaceProvider } from "../../providers";
import { IWorkspace, Workspace, WorkspaceInput } from ".";
import { IUserContext } from "../tokens/interface";
import { CreateResult, IMutationResult, MutationResult } from "../mutationresult";
import { isDataListedInQuery } from "../helpers/query";
import { logError } from "../../helpers/error";

@injectable()
@Resolver()
export class WorkspaceResolver {
    private readonly workspaceProvider: IWorkspaceProvider;
    private readonly logProvider: ILogProvider;
    private readonly metricProvider: IMetricProvider;

    constructor(
        @inject(workspaceProviderSymbol) workspaceProvider: IWorkspaceProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider,
        @inject(metricProviderSymbol) metricProvider: IMetricProvider
    ) {
        this.logProvider = logProvider;
        this.workspaceProvider = workspaceProvider;
        this.logProvider = logProvider;
        this.metricProvider = metricProvider;
    }

    @Authorized()
    @Query(() => Workspace!, { nullable: true, description: "Get specific workspace for authenticated user." })
    async workspace(
        @Arg("id", { description: "Id of workspace." }) id: string,
        @Ctx() context: IUserContext,
        @Info() info: GraphQLResolveInfo
    ): Promise<Workspace | undefined> {
        return this.workspaceProvider
            .read(context.token.user, id, isDataListedInQuery(info))
            .then((workspace: IWorkspace | undefined) => {
                if (!workspace) {
                    return undefined;
                }
                return new Workspace().load(workspace);
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Query(() => Workspace, { nullable: true, description: "Get root workspace for authenticated user." })
    async rootWorkspace(@Ctx() context: IUserContext, @Info() info: GraphQLResolveInfo): Promise<Workspace | undefined> {
        return this.workspaceProvider
            .readRoot(context.token.user, isDataListedInQuery(info))
            .then((workspace?: IWorkspace) => {
                if (!workspace) {
                    return undefined;
                }
                return new Workspace().load(workspace);
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => CreateResult, { nullable: true, description: "Create workspace for authenticated user." })
    async createWorkspace(@Ctx() context: IUserContext): Promise<CreateResult> {
        return this.workspaceProvider
            .create(context.token.user)
            .then((id?: string) => {
                this.metricProvider.add({ event: "create", subject: "workspace", subjectId: id, userId: context.token.user });

                const result = new CreateResult();
                result.id = id;
                return result;
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => Workspace, { description: "Update workspace for authenticated user." })
    async updateWorkspace(
        @Arg("data", { description: "Workspace to update." }) input: WorkspaceInput,
        @Ctx() context: IUserContext
    ): Promise<Workspace> {
        return this.workspaceProvider
            .update(context.token.user, input)
            .then(() => new Workspace().load(input))
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Delete workspace for authenticated user." })
    async deleteWorkspace(
        @Arg("id", { description: "Id of workspace." }) id: string,
        @Ctx() context: IUserContext
    ): Promise<IMutationResult> {
        return this.workspaceProvider
            .delete(context.token.user, id)
            .then(() => {
                this.metricProvider.add({
                    event: "delete",
                    subject: "workspace",
                    subjectId: id,
                    userId: context.token.user,
                });
                return { isSuccess: true };
            })
            .catch(logError(this.logProvider));
    }
}
