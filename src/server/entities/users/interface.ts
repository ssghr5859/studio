import { IPublic } from "../public";

/** Describes a user. */
export interface IUser extends IPublic {
    /** Language of the user. */
    language: string;

    /** Locale of the user. */
    locale: string;

    /** Emailaddress of user. */
    emailAddress: string;

    /** Share settings of user. */
    shareSettings?: IShareSettings;
}

/** Describes the share settings of a user. */
export interface IShareSettings {
    mode?: ShareModes;

    embed?: {
        type?: ShareEmbedTypes;
        hostDefinition?: boolean;
        hostResponse?: boolean;
        supportPauseAndResume?: boolean;
        persistent?: boolean;
        cdn?: string;
    };
}

/** Share modes. */
export type ShareModes = "url" | "embed";

/** Types to embed to sharing code. */
export type ShareEmbedTypes = "snippet" | "snippet-page" | "page" | "es6" | "form-kit";
