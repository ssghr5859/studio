import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { inject, injectable } from "inversify";
import { logProviderSymbol, userProviderSymbol } from "../../symbols";
import { ILogProvider, IUserProvider } from "../../providers";
import { IUserContext } from "../tokens/interface";
import { IShareSettings, IUser, User } from ".";
import { logError } from "../../helpers/error";
import { MutationResult } from "../mutationresult";
import { shareSettings } from "./scalar";

@injectable()
@Resolver()
export class UserResolver {
    private readonly userProvider: IUserProvider;
    private readonly logProvider: ILogProvider;

    constructor(@inject(userProviderSymbol) userProvider: IUserProvider, @inject(logProviderSymbol) logProvider: ILogProvider) {
        this.logProvider = logProvider;
        this.userProvider = userProvider;
    }

    @Authorized()
    @Query(() => User, { nullable: true, description: "Get user info for authenticated user." })
    async user(@Ctx() context: IUserContext): Promise<IUser | undefined> {
        return this.userProvider
            .readById(context.token.user)
            .then((value?: IUser) => value)
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update share settings for authenticated user." })
    async updateShareSettings(
        @Arg("settings", () => shareSettings, { description: "Share settings of user." }) settings: IShareSettings,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.userProvider
            .updateShareSettings(context.token.user, settings)
            .then(() => {
                const result = new MutationResult();
                result.isSuccess = true;
                return result;
            })
            .catch(logError(this.logProvider));
    }
}
