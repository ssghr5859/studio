import { Field, ObjectType } from "type-graphql";
import { IHookSettings, IServerDefinition, IServerDefinitionInput, ITrackers } from "./interface";
import { TRunners } from "./runners";
import { IDefinition as ITripettoDefinition } from "tripetto";
import { TL10n, TStyles } from "tripetto-runner-foundation";
import { definition, hooks, l10n, runners, styles, trackers } from "./scalar";
import { Loadable } from "../loadable";

@ObjectType({ description: "Describes a definition." })
export class Definition extends Loadable<IServerDefinitionInput> implements IServerDefinition {
    @Field({ description: "Id of definition." })
    id!: string;

    @Field(() => runners, { nullable: true, description: "Type of runner." })
    runner!: TRunners;

    @Field({ nullable: true, description: "Name of definition." })
    name!: string;

    @Field(() => definition, { nullable: true, description: "Data of definition." })
    data!: ITripettoDefinition;

    @Field(() => styles, { nullable: true, description: "Styles for the runner." })
    styles!: TStyles;

    @Field(() => l10n, { nullable: true, description: "L10n data for the runner." })
    l10n!: TL10n;

    @Field({ description: "Public key." })
    publicKey!: string;

    @Field({ description: "Read token." })
    readToken!: string;

    @Field({ description: "Alias for the read token." })
    readTokenAlias!: string;

    @Field({ description: "Alias for the template token." })
    templateTokenAlias!: string;

    @Field({ description: "Number of responses to the definition." })
    responseCount!: number;

    @Field({ description: "Number of times the definition was read." })
    readCount!: number;

    @Field({ nullable: true, description: "Timestamp when definition was created." })
    created!: number;

    @Field({ nullable: true, description: "Timestamp when definition was last modified." })
    modified!: number;

    @Field(() => hooks, { nullable: true, description: "Settings of hooks" })
    hooks!: IHookSettings;

    @Field(() => trackers, { nullable: true, description: "Settings of trackers" })
    trackers!: ITrackers;

    @Field({ nullable: true, description: "Tier of the definition." })
    tier!: "standard" | "premium";
}
