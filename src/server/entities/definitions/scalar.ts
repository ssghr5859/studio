import { GraphQLError, GraphQLScalarType, Kind, ValueNode } from "graphql";
import { IDefinition } from "tripetto";
import { IHookSettings, ITrackers } from "./interface";
import { TRunners } from "../definitions/runners";
import { TL10n, TStyles } from "tripetto-runner-foundation";

export const definition = new GraphQLScalarType({
    name: "definition",
    description: "Content of a Tripetto form definition",
    parseValue: (value: IDefinition): IDefinition => {
        return value;
    },
    serialize: (value?: IDefinition): IDefinition | undefined => {
        return value;
    },
    parseLiteral: (ast: ValueNode): IDefinition => {
        // Here we can validate the input
        if (ast.kind === Kind.STRING) {
            return JSON.parse(ast.value);
        }
        throw new GraphQLError(`Can only validate as string, but got a: ${ast.kind}`);
    },
});

export const runners = new GraphQLScalarType({
    name: "runners",
    description: "Type of runner",
    parseValue: (value: TRunners): TRunners => {
        return value;
    },
    serialize: (value?: TRunners): TRunners | undefined => {
        return value;
    },
    parseLiteral: (ast: ValueNode): TRunners => {
        // Here we can validate the input
        if (ast.kind === Kind.STRING) {
            return JSON.parse(ast.value);
        }
        throw new GraphQLError(`Can only validate as string, but got a: ${ast.kind}`);
    },
});

export const styles = new GraphQLScalarType({
    name: "styles",
    description: "Runner styles",
    parseValue: (value: TStyles): TStyles => {
        return value;
    },
    serialize: (value?: TStyles): TStyles | undefined => {
        return value;
    },
    parseLiteral: (ast: ValueNode): TStyles => {
        // Here we can validate the input
        if (ast.kind === Kind.STRING) {
            return JSON.parse(ast.value);
        }
        throw new GraphQLError(`Can only validate as string, but got a: ${ast.kind}`);
    },
});

export const l10n = new GraphQLScalarType({
    name: "l10n",
    description: "Runner localization data",
    parseValue: (value: TL10n): TL10n => {
        return value;
    },
    serialize: (value?: TL10n): TL10n | undefined => {
        return value;
    },
    parseLiteral: (ast: ValueNode): TL10n => {
        // Here we can validate the input
        if (ast.kind === Kind.STRING) {
            return JSON.parse(ast.value);
        }
        throw new GraphQLError(`Can only validate as string, but got a: ${ast.kind}`);
    },
});

export const hooks = new GraphQLScalarType({
    name: "hooks",
    description: "Hooks settings",
    parseValue: (value: IHookSettings): IHookSettings => {
        return value;
    },
    serialize: (value?: IHookSettings): IHookSettings | undefined => {
        return value;
    },
    parseLiteral: (ast: ValueNode): IHookSettings => {
        // Here we can validate the input
        if (ast.kind === Kind.STRING) {
            return JSON.parse(ast.value);
        }
        throw new GraphQLError(`Can only validate as string, but got a: ${ast.kind}`);
    },
});

export const trackers = new GraphQLScalarType({
    name: "trackers",
    description: "Tracker settings",
    parseValue: (value: ITrackers): ITrackers => {
        return value;
    },
    serialize: (value?: ITrackers): ITrackers | undefined => {
        return value;
    },
    parseLiteral: (ast: ValueNode): ITrackers => {
        // Here we can validate the input
        if (ast.kind === Kind.STRING) {
            return JSON.parse(ast.value);
        }
        throw new GraphQLError(`Can only validate as string, but got a: ${ast.kind}`);
    },
});
