import { inject, injectable } from "inversify";
import { GraphQLResolveInfo } from "graphql";
import { Arg, Authorized, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { isDataListedInQuery } from "../helpers/query";
import { IResponse, Response } from ".";
import { IAuthenticationToken, IUserContext } from "../tokens/interface";
import { definitionProviderSymbol, logProviderSymbol, responseProviderSymbol } from "../../symbols";
import { IDefinitionProvider, ILogProvider, IResponseProvider } from "../../providers";
import { MutationResult } from "../mutationresult";
import { logError } from "../../helpers/error";
import { IDefinition } from "../definitions";
import { Export, set } from "tripetto-runner-foundation";

@injectable()
@Resolver()
export class ResponseResolver {
    private readonly definitionProvider: IDefinitionProvider;
    private readonly responseProvider: IResponseProvider;
    private readonly logProvider: ILogProvider;

    constructor(
        @inject(responseProviderSymbol) responseProvider: IResponseProvider,
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider
    ) {
        this.logProvider = logProvider;
        this.responseProvider = responseProvider;
        this.definitionProvider = definitionProvider;
    }

    /* Legacy responses don't have a fingerprint saved separately yet. This function updates the fingerprint if necessary. */
    private async updateFingerprint(userId: string, definitionId: string, response: IResponse): Promise<void> {
        if (!response.fingerprint) {
            return this.responseProvider.read(userId, definitionId, response.id, true).then((value?: IResponse) => {
                if (value && value.data) {
                    const exportables = JSON.parse(value.data) as Export.IExportables;

                    if (!response.fingerprint) {
                        set(response, "fingerprint", exportables.fingerprint);
                    }

                    return this.responseProvider.updateFingerprint(userId, definitionId, response.id, response.fingerprint);
                }
                return Promise.resolve();
            });
        } else {
            return Promise.resolve();
        }
    }

    @Authorized()
    @Query(() => [Response], { nullable: true, description: "Get all responses to a definition for authenticated user." })
    async responses(
        @Arg("definitionId", { description: "Id of definition." }) definitionId: string,
        @Ctx() context: { token: IAuthenticationToken },
        @Info() info: GraphQLResolveInfo
    ): Promise<Response[] | undefined> {
        return this.responseProvider
            .readAllByDefinition(context.token.user, definitionId, isDataListedInQuery(info))
            .then(async (responses?: IResponse[]) => {
                if (!responses) {
                    return undefined;
                }

                await Promise.all(responses.map((value: IResponse) => this.updateFingerprint(context.token.user, definitionId, value)));

                return responses.map((value: IResponse) => new Response().load(value));
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Query(() => Response, { description: "Get response to a definition for authenticated user." })
    async response(
        @Arg("definitionId", { description: "Id of definition." }) definitionId: string,
        @Arg("responseId", { description: "Id of response." }) responseId: string,
        @Ctx() context: { token: IAuthenticationToken },
        @Info() info: GraphQLResolveInfo
    ): Promise<Response | undefined> {
        return this.responseProvider
            .read(context.token.user, definitionId, responseId, isDataListedInQuery(info))
            .then((response?: IResponse) => {
                if (!response) {
                    return undefined;
                }
                return new Response().load(response);
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Delete response to a definition for authenticated user." })
    async deleteResponse(
        @Arg("definitionId", { description: "Id of definition." }) definitionId: string,
        @Arg("responseId", { description: "Id of response." }) responseId: string,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .readById(context.token.user, definitionId, false)
            .then((definition?: IDefinition) => {
                if (definition) {
                    return this.responseProvider
                        .delete(context.token.user, definitionId, definition.publicKey, responseId)
                        .then(() => ({ isSuccess: true }));
                } else {
                    return { isSuccess: false };
                }
            })
            .catch(logError(this.logProvider));
    }
}
