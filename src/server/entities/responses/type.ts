import { Field, ObjectType } from "type-graphql";
import { IResponse } from ".";
import { Loadable } from "../loadable";

@ObjectType({ description: "Describes a response." })
export class Response extends Loadable<IResponse> implements IResponse {
    @Field({ description: "Id of response." })
    id!: string;

    @Field({ description: "Timestamp response was created." })
    created!: number;

    @Field({ description: "Timestamp response was last modified." })
    modified!: number;

    @Field({ description: "Data of response." })
    data!: string;

    @Field({ description: "Fingerprint of the definition this response belongs to." })
    fingerprint!: string;

    @Field({ description: "Stencil of the response data." })
    stencil!: string;
}
