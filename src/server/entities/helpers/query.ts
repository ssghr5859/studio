import { FieldNode, GraphQLResolveInfo, SelectionNode } from "graphql";

/** Checks if the query contains the field `data`. */
export function isDataListedInQuery(info: GraphQLResolveInfo): boolean {
    return getFieldsFromQuery(info).includes("data");
}

function getFieldsFromQuery(info: GraphQLResolveInfo): string[] {
    const fields: string[] = [];

    info.fieldNodes.forEach((fieldNode: FieldNode) => {
        if (fieldNode && fieldNode.selectionSet) {
            fieldNode.selectionSet.selections.forEach((value: SelectionNode) => {
                if (value.kind === "Field") {
                    fields.push(value.name.value);
                }
            });
        }
    });
    return fields;
}
