import { Field, ObjectType } from "type-graphql";
import { ICreateResult, IMutationResult } from "./interface";

@ObjectType({ description: "Describes the result of a mutation." })
export class MutationResult implements IMutationResult {
    @Field()
    isSuccess!: boolean;

    @Field({ nullable: true })
    message?: string;
}

@ObjectType({ description: "Describes the result of a create." })
export class CreateResult implements ICreateResult {
    @Field({ nullable: true, description: "Id of the newly created object." })
    id?: string;
}
