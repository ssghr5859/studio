export interface IMutationResult {
    isSuccess: boolean;
    message?: string;
}

export interface ICreateResult {
    id?: string;
}
