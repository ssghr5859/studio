export abstract class Loadable<T> {
    load(data: T): this {
        const src = data as {
            // tslint:disable-next-line:no-any
            [key: string]: any;
        };

        const dest = this as {
            // tslint:disable-next-line:no-any
            [key: string]: any;
        };

        Object.keys(data).forEach((key: string) => {
            dest[key] = src[key];
        });

        return this;
    }
}
