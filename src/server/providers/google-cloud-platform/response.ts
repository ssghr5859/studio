import { DocumentData, DocumentReference, DocumentSnapshot, FieldValue, QueryDocumentSnapshot, Timestamp } from "@google-cloud/firestore";
import { inject, injectable } from "inversify";
import {
    Export,
    Num,
    SHA2,
    calculateFingerprintAndStencil,
    checksum as calculateChecksum,
    powVerify,
    set,
} from "tripetto-runner-foundation";
import { FirestoreProvider } from "./firestore";
import { attachmentProviderSymbol, fileProviderSymbol, logProviderSymbol } from "../../symbols";
import { IResponse, IResponseAnnouncement, IResponseSubmission } from "../../entities/responses/interface";
import { IAttachmentProvider, IFileProvider, ILogProvider, IResponseProvider } from "..";
import { isAttachment } from "../../helpers/attachment";

@injectable()
export class ResponseProvider extends FirestoreProvider implements IResponseProvider {
    private readonly fileProvider: IFileProvider;
    private readonly logProvider: ILogProvider;
    private readonly attachmentProvider: IAttachmentProvider;

    constructor(
        @inject(fileProviderSymbol) fileProvider: IFileProvider,
        @inject(attachmentProviderSymbol) attachmentProvider: IAttachmentProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider
    ) {
        super();

        this.fileProvider = fileProvider;
        this.attachmentProvider = attachmentProvider;
        this.logProvider = logProvider;
    }

    private confirmAttachments(attachments: string[], userPublicKey: string, definitionPublicKey: string): void {
        attachments.forEach((attachment: string) => {
            this.attachmentProvider
                .confirm(attachment, userPublicKey, definitionPublicKey)
                .catch((err: Error) => this.logProvider.error(err));
        });
    }

    private load(doc: QueryDocumentSnapshot<DocumentData> | DocumentSnapshot): IResponse {
        const data = doc.data();

        return {
            id: doc.id,
            created: doc.createTime?.toMillis() || 0,
            fingerprint: data?.fingerprint || "",
            stencil: data?.stencil || "",
            attachments: data?.attachments || undefined,
        };
    }

    private async appendDataToResponse(response: IResponse, userId: string, definitionId: string, responseId: string): Promise<IResponse> {
        return new Promise((fnResolve: (response: IResponse) => void) => {
            this.fileProvider
                .readResponse(userId, definitionId, responseId)
                .then((data?: string | Buffer) => {
                    set(response, "data", (data && data.toString()) || "");

                    fnResolve(response);
                })
                .catch(() => {
                    set(response, "data", "");

                    fnResolve(response);
                });
        });
    }

    private async verifyFingerprint(userId: string, definition: DocumentReference<DocumentData>, fingerprint: string): Promise<boolean> {
        const definitionDoc = await definition.get();

        if (definitionDoc.exists) {
            const definitionData = definitionDoc.data();

            if (definitionData) {
                if (definitionData.fingerprint === fingerprint && definitionData.stencil && definitionData.actionables) {
                    return true;
                }

                if (!definitionData.fingerprint || !definitionData.stencil || !definitionData.actionables) {
                    const file = await this.fileProvider.readDefinition(userId, definition.id);

                    if (file) {
                        const result = calculateFingerprintAndStencil(JSON.parse(file.toString()));

                        this.firestore.doc(this.getDefinitionPath(userId, definition.id)).update({
                            fingerprint: result.fingerprint,
                            stencil: result.stencil("exportables"),
                            actionables: result.stencil("actionables"),
                        });

                        const lastUpdate = (definitionData.modified as Timestamp).toMillis();

                        if (lastUpdate > 0) {
                            const responsesCollection = definition.collection(this.responseCollectionName);

                            (await responsesCollection.get()).docs.forEach(async (doc) => {
                                if (doc.createTime.toMillis() > lastUpdate) {
                                    const responseData = doc.data();

                                    if (!responseData.fingerprint || !responseData.stencil || !responseData.actionables) {
                                        if (responseData.fingerprint) {
                                            (
                                                await responsesCollection.where("fingerprint", "==", responseData.fingerprint).get()
                                            ).docs.forEach((sibling) => {
                                                responsesCollection.doc(sibling.id).update({
                                                    fingerprint: result.fingerprint,
                                                    stencil: result.stencil("exportables"),
                                                });
                                            });
                                        } else {
                                            responsesCollection.doc(doc.id).update({
                                                fingerprint: result.fingerprint,
                                                stencil: result.stencil("exportables"),
                                            });
                                        }
                                    }
                                }
                            });
                        }

                        return result.fingerprint === fingerprint;
                    }
                }
            }
        }

        return false;
    }

    async announce(
        userPublicKey: string,
        definitionPublicKey: string,
        fingerprint: string,
        checksum: string,
        runner: string,
        ip: string
    ): Promise<IResponseAnnouncement | undefined> {
        const user = await this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), userPublicKey);

        if (fingerprint && user) {
            const definition = await this.getDocumentByPublicKey(user.collection(this.definitionCollectionName), definitionPublicKey);

            if (definition && (await this.verifyFingerprint(user.id, definition, fingerprint))) {
                const identity = SHA2.SHA2_256(fingerprint + ip);
                const responses = definition.collection(this.responseCollectionName);
                const recentResponses = await responses.where("identity", "==", identity).get();
                const announcements = definition.collection(this.announcementCollectionName);
                const recentAnnouncements = await announcements.where("identity", "==", identity).get();
                const timeFrame = Date.now() - 1000 * 60 * 10;
                let difficulty = 12;

                recentResponses.docs.forEach((doc) => {
                    if (doc.createTime.toMillis() > timeFrame) {
                        difficulty++;
                    }
                });

                recentAnnouncements.docs.forEach((doc) => {
                    if (doc.createTime.toMillis() > timeFrame) {
                        difficulty = Num.max(difficulty + 2, doc.data().difficulty + 2);
                    }
                });

                const announcement = await announcements.add({
                    created: FieldValue.serverTimestamp(),
                    fingerprint,
                    checksum,
                    difficulty,
                    runner,
                    identity,
                });

                console.log(`ANNOUNCEMENT<${definitionPublicKey}>: announcement=${announcement.id} difficulty=${difficulty}`);

                return {
                    id: announcement.id,
                    difficulty,
                    timestamp: Date.now(),
                };
            }
        }

        return undefined;
    }

    async submit(
        userPublicKey: string,
        definitionPublicKey: string,
        announcementId: string,
        nonce: string,
        powHashRate: number,
        powDuration: number,
        language: string,
        locale: string,
        exportables: Export.IExportables,
        actionables: Export.IActionables | undefined
    ): Promise<IResponseSubmission | undefined> {
        const user = await this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), userPublicKey);

        if (user) {
            const definition = await this.getDocumentByPublicKey(user.collection(this.definitionCollectionName), definitionPublicKey);
            const userDoc = await user.get();
            const userData = userDoc.data();

            if (definition && userData && announcementId && nonce) {
                const definitionDoc = await definition.get();
                const definitionData = definitionDoc.data();

                const announcements = definition.collection(this.announcementCollectionName);
                const announcement = await announcements.doc(announcementId).get();

                const announcementData = announcement.data();
                const checksum = calculateChecksum(
                    {
                        exportables,
                        actionables,
                    },
                    false
                );

                console.log(`SUBMIT<${definitionPublicKey}>: announcement=${announcementId} checksum=${checksum}`);

                if (
                    definitionData &&
                    announcementData &&
                    announcementData.checksum === checksum &&
                    announcementData.fingerprint === definitionData.fingerprint &&
                    definitionData.fingerprint === exportables.fingerprint &&
                    definitionData.stencil === exportables.stencil &&
                    (!actionables ||
                        (actionables.fingerprint === announcementData.fingerprint && actionables.stencil === definitionData.actionables)) &&
                    powVerify(
                        nonce,
                        announcementData.difficulty,
                        1000 * 60 * 15,
                        {
                            exportables,
                            actionables,
                        },
                        announcementId
                    )
                ) {
                    const responses = definition.collection(this.responseCollectionName);
                    const attachments = exportables.fields
                        .filter((field) => isAttachment(field) && field.reference)
                        .map((field) => field.reference!);

                    const response = await responses.add({
                        created: FieldValue.serverTimestamp(),
                        fingerprint: definitionData.fingerprint,
                        stencil: definitionData.stencil,
                        runner: announcementData.runner,
                        checksum,
                        language,
                        locale,
                        identity: announcementData.identity,
                        powDifficulty: announcementData.difficulty,
                        powHashRate,
                        powDuration,
                        ...((attachments.length > 0 && {
                            attachments,
                            attachmentCount: attachments.length,
                        }) ||
                            undefined),
                    });

                    await announcements.doc(announcement.id).delete();

                    try {
                        await this.fileProvider.writeResponse(user.id, definition.id, response.id, JSON.stringify(exportables));

                        this.confirmAttachments(attachments, userPublicKey, definitionPublicKey);

                        console.log(`STORED<${definitionPublicKey}>: announcement=${announcementId} id=${response.id}`);

                        return {
                            id: response.id,
                            language: userData.language || "",
                            locale: userData.locale || "",
                            name: definitionData.name || "",
                            readTokenAlias: definitionData.readTokenAlias || undefined,
                            hooks: definitionData.hooks || undefined,
                            tier: definitionData.tier || undefined,
                        };
                    } catch {
                        responses.doc(response.id).delete();

                        return undefined;
                    }
                } else {
                    console.log(
                        `PoW Error / FINGERPRINT(${
                            announcementData && definitionData && announcementData.fingerprint === definitionData.fingerprint ? "V" : "X"
                        }) / STENCIL(${definitionData && definitionData.stencil === exportables.stencil ? "V" : "X"}) / CHECKSUM(${
                            announcementData && announcementData.checksum === checksum ? "V" : "X"
                        }) / PROOF(${
                            announcementData &&
                            powVerify(
                                nonce,
                                announcementData.difficulty,
                                1000 * 60 * 15,
                                {
                                    exportables,
                                    actionables,
                                },
                                announcementId
                            )
                                ? "V"
                                : "X"
                        }) / Expected checksum: ${
                            (announcementData && announcementData.checksum) || ""
                        } / Received checksum: ${checksum} / Nonce: ${nonce} / Definition: ${definitionPublicKey}`
                    );
                }
            }
        }

        return undefined;
    }

    async read(userId: string, definitionId: string, responseId: string, includeData: boolean): Promise<IResponse | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getResponsePath(userId, definitionId, responseId))
                .get()
                .then((responseSnapshot: DocumentSnapshot) => {
                    if (!responseSnapshot.exists) {
                        return Promise.resolve(undefined);
                    }

                    return includeData
                        ? this.appendDataToResponse(this.load(responseSnapshot), userId, definitionId, responseId)
                        : this.load(responseSnapshot);
                });
        });
    }

    async readAllByDefinition(userId: string, definitionId: string, includeData: boolean): Promise<IResponse[] | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getResponsesPath(userId, definitionId))
                .get()
                .then((responses) =>
                    Promise.all(
                        responses.docs.map(async (responseDoc) =>
                            includeData
                                ? this.appendDataToResponse(this.load(responseDoc), userId, definitionId, responseDoc.id)
                                : this.load(responseDoc)
                        )
                    )
                );
        });
    }

    async readAllByFingerprint(userId: string, definitionId: string, fingerprint: string): Promise<IResponse[] | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getResponsesPath(userId, definitionId))
                .where("fingerprint", "==", fingerprint)
                .get()
                .then((responses) => responses.docs.map((response) => this.load(response)));
        });
    }

    async readAllByStencil(userId: string, definitionId: string, stencil: string): Promise<IResponse[] | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getResponsesPath(userId, definitionId))
                .where("stencil", "==", stencil)
                .get()
                .then((responses) => responses.docs.map((response) => this.load(response)));
        });
    }

    async readCountByDefinition(userId: string, definitionId: string): Promise<number | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getResponsesPath(userId, definitionId))
                .listDocuments()
                .then((docs: DocumentReference[]) => docs.length);
        });
    }

    async list(userId: string, definitionId: string): Promise<string[] | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getResponsesPath(userId, definitionId))
                .listDocuments()
                .then((docs: DocumentReference[]) => docs.map((value: DocumentReference) => value.id));
        });
    }

    async listAttachments(userId: string, definitionId: string): Promise<string[] | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getResponsesPath(userId, definitionId))
                .where("attachmentCount", ">", 0)
                .get()
                .then((querySnapshot) => {
                    let attachments: string[] = [];

                    querySnapshot.docs.forEach((doc) => {
                        attachments = attachments.concat(doc.get("attachments"));
                    });

                    return attachments;
                });
        });
    }

    async delete(userId: string, definitionId: string, definitionPublicKey: string, responseId: string): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async (userDocument: DocumentReference) => {
            this.read(userId, definitionId, responseId, false).then(async (response?: IResponse) => {
                if (response) {
                    return userDocument.get().then((value) => {
                        const publicKey = value.get("publicKey");

                        return Promise.all([
                            response.attachments
                                ? response.attachments.map((attachment: string) =>
                                      this.attachmentProvider.delete(attachment, publicKey, definitionPublicKey)
                                  )
                                : [],
                            this.fileProvider.deleteResponse(userId, definitionId, responseId),
                            this.firestore.doc(this.getResponsePath(userId, definitionId, responseId)).delete(),
                        ]).then(() => Promise.resolve());
                    });
                }
            });
        });
    }

    async updateFingerprint(userId: string, definitionId: string, responseId: string, fingerprint: string): Promise<void> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getResponsePath(userId, definitionId, responseId))
                .update({ fingerprint })
                .then(() => Promise.resolve());
        });
    }
}
