import { injectable } from "inversify";
import { DocumentReference, FieldValue, QuerySnapshot } from "@google-cloud/firestore";
import { IAccessTokenProvider } from "..";
import { FirestoreProvider } from "./firestore";

@injectable()
export class AccessTokenProvider extends FirestoreProvider implements IAccessTokenProvider {
    constructor() {
        super();
    }

    async create(token: string): Promise<string | undefined> {
        return this.firestore
            .collection(this.accessTokenCollectionName)
            .add({
                token,
                created: FieldValue.serverTimestamp(),
            })
            .then((doc: DocumentReference) => doc.id);
    }

    async read(token: string): Promise<string | undefined> {
        return this.firestore
            .collection(this.accessTokenCollectionName)
            .where("token", "==", token)
            .get()
            .then((tokenQuerySnapshot: QuerySnapshot) => (tokenQuerySnapshot.empty ? undefined : tokenQuerySnapshot.docs[0].id));
    }

    async delete(id: string): Promise<void> {
        return this.firestore
            .doc(this.getLoginTokenPath(id))
            .delete()
            .then(() => Promise.resolve());
    }
}
