import { DocumentReference, DocumentSnapshot, FieldValue, QuerySnapshot, WriteResult } from "@google-cloud/firestore";
import { IShareSettings, IUser } from "../../entities/users/interface";
import { IUserProvider } from "../user";
import { FirestoreProvider } from "./firestore";
import { inject, injectable } from "inversify";
import { definitionProviderSymbol, fileProviderSymbol } from "../../symbols";
import { IDefinitionProvider } from "../definition";
import { IFileProvider } from "../file";

@injectable()
export class UserProvider extends FirestoreProvider implements IUserProvider {
    private readonly definitionProvider: IDefinitionProvider;
    private readonly fileProvider: IFileProvider;

    constructor(
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(fileProviderSymbol) fileProvider: IFileProvider
    ) {
        super();

        this.definitionProvider = definitionProvider;
        this.fileProvider = fileProvider;
    }

    private async updateLoginData(user: DocumentReference, snapshot: DocumentSnapshot): Promise<WriteResult> {
        const data = snapshot.data();
        const serverTimestamp = FieldValue.serverTimestamp();

        const updateData: {
            firstLogin?: FieldValue;
            lastLogin: FieldValue;
            loginCount: number;
        } = {
            lastLogin: serverTimestamp,
            loginCount: 0,
        };
        if (data) {
            updateData.loginCount = data.loginCount || updateData.loginCount;
            if (!data.firstLogin) {
                updateData.firstLogin = serverTimestamp;
            }
        }

        updateData.loginCount = updateData.loginCount + 1;
        return user.update(updateData);
    }

    async create(emailAddress: string, language: string, locale: string): Promise<IUser> {
        return this.firestore
            .collection(this.userCollectionName)
            .add({
                emailAddress: emailAddress.toLowerCase(),
                language,
                locale,
                created: FieldValue.serverTimestamp(),
                loginCount: 0,
            })
            .then(async (doc: DocumentReference) => {
                return this.setPublicKey(doc).then(
                    (publicKey: string): IUser => ({
                        language,
                        locale,
                        emailAddress,
                        publicKey,
                    })
                );
            });
    }

    async readByEmail(emailAddress: string, language: string, locale: string): Promise<IUser | undefined> {
        return this.firestore
            .collection(this.userCollectionName)
            .where("emailAddress", "==", emailAddress.toLowerCase())
            .get()
            .then((existingUsers: QuerySnapshot): IUser | undefined => {
                if (existingUsers.empty) {
                    return undefined;
                }

                const userDoc = existingUsers.docs[0];
                const userData = userDoc.data();

                /**
                 * This adds the language and locale for existing users that
                 * don't have these fields in the database.
                 */
                if (!userData.language || !userData.locale) {
                    userDoc.ref.update({
                        language: userData.language || language,
                        locale: userData.locale || locale,
                    });
                }

                return {
                    emailAddress: userData.emailAddress,
                    publicKey: userData.publicKey,
                    language: userData.language || language,
                    locale: userData.locale || locale,
                    shareSettings: userData.shareSettings,
                };
            });
    }

    async readById(id: string): Promise<IUser | undefined> {
        const user = this.firestore.doc(this.getUserPath(id));
        return user.get().then((snapshot: DocumentSnapshot): IUser | undefined => {
            const userData = snapshot.data();

            if (!snapshot.exists || !userData) {
                return undefined;
            }

            return {
                emailAddress: userData.emailAddress,
                publicKey: userData.publicKey,
                language: userData.language || "",
                locale: userData.locale || "",
                shareSettings: userData.shareSettings,
            };
        });
    }

    async updateShareSettings(id: string, settings: IShareSettings): Promise<void> {
        const user = this.firestore.doc(this.getUserPath(id));
        return user.get().then(async (snapshot: DocumentSnapshot) => {
            if (!snapshot.exists) {
                throw new Error(`User with id ${id} doesn't exist.`);
            }
            return user.update({ shareSettings: settings }).then(() => Promise.resolve());
        });
    }

    async login(publicKey: string): Promise<string | undefined> {
        return this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), publicKey).then(
            async (user?: DocumentReference) => {
                if (!user) {
                    return undefined;
                }
                return user
                    .get()
                    .then((snapshot: DocumentSnapshot) => this.updateLoginData(user, snapshot))
                    .then(() => user.id);
            }
        );
    }

    async delete(id: string): Promise<void> {
        return this.requireUserBeforeExecute(id, async (userDocument: DocumentReference) => {
            // Find all definitions.
            return this.definitionProvider.list(id).then(async (definitions?: string[]) => {
                const deletes: Promise<void>[] = [];

                if (definitions) {
                    definitions.forEach((definitionId: string) => {
                        deletes.push(this.definitionProvider.delete(id, definitionId));
                    });
                }

                // Delete all workspaces.
                deletes.push(this.deleteCollection(this.getWorkspacesPath(id), 100));

                // Delete the user.
                deletes.push(userDocument.delete().then(() => Promise.resolve()));

                return Promise.all(deletes).then(() => {
                    /* Delete all files in a separate action to prevent file delete conflicts
                with individual definition deletes (resulting in 404 Not Found errors). */
                    return this.fileProvider.deleteUser(id);
                });
            });
        });
    }
}
