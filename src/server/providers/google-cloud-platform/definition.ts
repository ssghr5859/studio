import { inject, injectable } from "inversify";
import { DocumentData, DocumentReference, DocumentSnapshot, FieldValue, Timestamp } from "@google-cloud/firestore";
import { fileProviderSymbol, responseProviderSymbol, tokenAliasProviderSymbol } from "../../symbols";
import { IDefinitionProvider, IFileProvider, IResponseProvider, ITokenAliasProvider } from "..";
import { FirestoreProvider } from "./firestore";
import { IHookSettings, IServerDefinition, IServerDefinitionInput, ITrackers } from "../../entities/definitions/interface";
import { TRunners } from "../../entities/definitions/runners";
import { migrateL10n, migrateStyles } from "../../helpers/migration";
import { TL10n, TStyles } from "tripetto-runner-foundation";
import { IDefinition as ITripettoDefinition } from "tripetto";
import { calculateFingerprintAndStencil } from "tripetto-runner-foundation";

@injectable()
export class DefinitionProvider extends FirestoreProvider implements IDefinitionProvider {
    private readonly fileProvider: IFileProvider;
    private readonly tokenAliasProvider: ITokenAliasProvider;
    private readonly responseProvider: IResponseProvider;

    constructor(
        @inject(fileProviderSymbol) fileProvider: IFileProvider,
        @inject(tokenAliasProviderSymbol) tokenAliasProvider: ITokenAliasProvider,
        @inject(responseProviderSymbol) responseProvider: IResponseProvider
    ) {
        super();
        this.fileProvider = fileProvider;
        this.tokenAliasProvider = tokenAliasProvider;
        this.responseProvider = responseProvider;
    }

    private async saveFile(userId: string, definition: IServerDefinitionInput): Promise<void> {
        return this.fileProvider.writeDefinition(userId, definition.id, JSON.stringify(definition.data));
    }

    private incrementReadCount(definition: DocumentReference, readCount?: number): number {
        readCount = (readCount || 0) + 1;

        try {
            definition.update({ readCount });
        } catch {
            definition.update({ readCount });
        }

        return readCount;
    }

    private load(id: string, data: DocumentData): IServerDefinition {
        const styles = (data.styles && JSON.parse(data.styles)) || data.style || undefined;
        const l10n = (data.l10n && JSON.parse(data.l10n)) || undefined;

        return {
            id,
            name: data.name,
            publicKey: data.publicKey,
            runner: data.runner || "autoscroll",
            readTokenAlias: data.readTokenAlias || undefined,
            readCount: data.readCount || 0,
            styles: styles && migrateStyles(styles),
            l10n: migrateL10n(l10n, styles),
            templateTokenAlias: data.templateTokenAlias || undefined,
            fingerprint: data.fingerprint || undefined,
            stencil: data.stencil || undefined,
            actionables: data.actionables || undefined,
            hooks: data.hooks || undefined,
            trackers: data.trackers || undefined,
            tier: data.tier || undefined,
            created: (data.created as Timestamp).toMillis(),
            modified: (data.modified as Timestamp).toMillis(),
        };
    }

    private async appendDataToDefinition(definition: IServerDefinition, userId: string, definitionId: string): Promise<IServerDefinition> {
        return this.fileProvider.readDefinition(userId, definitionId).then((file?: string | Buffer) => {
            definition.data = (file && JSON.parse(file.toString())) || ({} as ITripettoDefinition);
            return definition;
        });
    }

    private async updateData(userId: string, definitionId: string, data: {}): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update(data)
                .then(() => Promise.resolve());
        });
    }

    async create(userId: string, runner: TRunners): Promise<IServerDefinition | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getDefinitionsPath(userId))
                .add({
                    runner,
                    ...this.getServerTimestampValues(),
                })
                .then((documentReference: DocumentReference) => this.setPublicKey(documentReference).then(() => documentReference.id))
                .then((id: string) => this.readById(userId, id, false));
        });
    }

    async readById(userId: string, definitionId: string, includeData: boolean): Promise<IServerDefinition | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .get()
                .then((definitionSnapshot: DocumentSnapshot) => {
                    if (!definitionSnapshot.exists) {
                        return Promise.resolve(undefined);
                    }
                    const data = definitionSnapshot.data();
                    if (!data) {
                        return undefined;
                    }
                    const definition = this.load(definitionId, data);

                    if (includeData) {
                        return this.appendDataToDefinition(definition, userId, definitionId);
                    }
                    return definition;
                });
        });
    }

    async readByPublicKey(
        userPublicKey: string,
        definitionPublicKey: string,
        includeData: boolean
    ): Promise<IServerDefinition | undefined> {
        return this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), userPublicKey).then(
            (user?: DocumentReference) => {
                if (!user) {
                    return undefined;
                }
                return this.getDocumentByPublicKey(user.collection(this.definitionCollectionName), definitionPublicKey).then(
                    (definitionReference?: DocumentReference) => {
                        if (!definitionReference) {
                            return undefined;
                        }
                        return definitionReference.get().then((snapshot: DocumentSnapshot) => {
                            const data = snapshot.data();
                            if (!data) {
                                return Promise.resolve(undefined);
                            }

                            const definition = this.load(definitionReference.id, data);

                            if (includeData) {
                                definition.readCount = this.incrementReadCount(definitionReference, definition.readCount);
                                return this.appendDataToDefinition(definition, user.id, definition.id);
                            }
                            return definition;
                        });
                    }
                );
            }
        );
    }

    async list(userId: string): Promise<string[] | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getDefinitionsPath(userId))
                .listDocuments()
                .then((documents: DocumentReference[]) => documents.map((value: DocumentReference) => value.id));
        });
    }

    async update(userId: string, definition: IServerDefinitionInput): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async (userDocument: DocumentReference) => {
            const result = definition.data && calculateFingerprintAndStencil(definition.data);

            return Promise.all([
                this.firestore.doc(this.getDefinitionPath(userId, definition.id)).update({
                    name: definition.name,
                    fingerprint: result?.fingerprint || undefined,
                    stencil: result?.stencil("exportables") || undefined,
                    actionables: result?.stencil("actionables") || undefined,
                    modified: FieldValue.serverTimestamp(),
                }),
                this.saveFile(userId, definition),
            ]).then(() => Promise.resolve());
        });
    }

    async updateRunner(userId: string, definitionId: string, runner: TRunners): Promise<void | undefined> {
        return this.updateData(userId, definitionId, { runner });
    }

    async updateStyles(userId: string, definitionId: string, styles: TStyles): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update({
                    styles: JSON.stringify(styles),
                    modified: FieldValue.serverTimestamp(),
                })
                .then(() => Promise.resolve());
        });
    }

    async updateL10n(userId: string, definitionId: string, l10n: TL10n): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update({
                    l10n: JSON.stringify(l10n),
                    modified: FieldValue.serverTimestamp(),
                })
                .then(() => Promise.resolve());
        });
    }

    async updateHooks(userId: string, definitionId: string, hooks: IHookSettings): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update({
                    hooks,
                    modified: FieldValue.serverTimestamp(),
                })
                .then(() => Promise.resolve());
        });
    }

    async updateTrackers(userId: string, definitionId: string, trackers: ITrackers): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update({
                    trackers,
                    modified: FieldValue.serverTimestamp(),
                })
                .then(() => Promise.resolve());
        });
    }

    async updateReadTokenAlias(userId: string, definitionId: string, readTokenAlias: string): Promise<void | undefined> {
        return this.updateData(userId, definitionId, { readTokenAlias });
    }

    async updateTemplateTokenAlias(userId: string, definitionId: string, templateTokenAlias: string): Promise<void | undefined> {
        return this.updateData(userId, definitionId, { templateTokenAlias });
    }

    async delete(userId: string, definitionId: string): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.readById(userId, definitionId, false).then((definition?: IServerDefinition) => {
                if (!definition) {
                    return;
                }

                return this.responseProvider.list(userId, definitionId).then((responses?: string[]) => {
                    return Promise.all([
                        // Delete all responses.
                        responses
                            ? responses.map((responseId: string) =>
                                  this.responseProvider.delete(userId, definitionId, definition.publicKey, responseId)
                              )
                            : [],

                        // Delete all snapshots.
                        this.fileProvider.deleteSnapshots(userId, definitionId),
                        this.deleteCollection(this.getSnapshotsPath(userId, definitionId), 100),

                        // Delete token alias.
                        definition.readTokenAlias ? this.tokenAliasProvider.delete(definition.readTokenAlias) : Promise.resolve(),

                        // Delete definition itself.
                        this.fileProvider.deleteDefinition(userId, definitionId),
                        this.firestore.doc(this.getDefinitionPath(userId, definitionId)).delete(),
                    ]).then(() => Promise.resolve());
                });
            });
        });
    }
}
