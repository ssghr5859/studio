import { inject, injectable } from "inversify";
import { IMetricProvider } from "..";
import { logProviderSymbol } from "../../symbols";
import { ILogProvider } from "../log";
import { IMetric } from "../metric";
import { FirestoreProvider } from "./firestore";
import { FieldValue } from "@google-cloud/firestore";

@injectable()
export class MetricProvider extends FirestoreProvider implements IMetricProvider {
    private readonly logProvider: ILogProvider;
    private readonly logName = `metrics`;
    constructor(@inject(logProviderSymbol) logProvider: ILogProvider) {
        super();
        this.logProvider = logProvider;
    }

    private deleteUndefinedProperties(metric: IMetric): IMetric {
        // Prevent Firestore errors like:
        // Value for argument "data" is not a valid Firestore document.
        // Cannot use "undefined" as a Firestore value (found in field deviceSize).

        const src = metric as IMetric & {
            // tslint:disable-next-line:no-any
            [key: string]: any;
        };

        Object.keys(src).forEach((key: string) => src[key] === undefined && delete src[key]);
        return src;
    }

    add(metric: IMetric): void {
        const action = `${metric.event}_${metric.subject}`;
        const logName = `${this.logName}_${action}`;
        this.logProvider.info(action, logName);

        metric = this.deleteUndefinedProperties(metric);

        const metricData = metric as { created?: FieldValue };
        metricData.created = FieldValue.serverTimestamp();

        try {
            this.firestore
                .collection(this.metricCollectionName)
                .add(metricData)
                // Swallow error
                .catch(() => Promise.resolve);
        } catch (err) {
            // Prevent errors like `Cannot use "undefined" as a Firestore value.` break the application.
            // This error doesn't result in a rejected Promise, hence the try catch notation.
            this.logProvider.error((err as Error).toString());
        }
    }
}
