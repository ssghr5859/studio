import { injectable } from "inversify";
import { FieldValue, QuerySnapshot } from "@google-cloud/firestore";
import { ITokenAlias, ITokenAliasProvider } from "../token-alias";
import { FirestoreProvider } from "./firestore";

@injectable()
export class TokenAliasProvider extends FirestoreProvider implements ITokenAliasProvider {
    constructor() {
        super();
    }

    async create(tokenAlias: ITokenAlias): Promise<void> {
        return this.firestore
            .collection(this.tokenAliasCollectionName)
            .add({
                created: FieldValue.serverTimestamp(),
                token: tokenAlias.token,
                alias: tokenAlias.alias,
            })
            .then(() => Promise.resolve());
    }

    async read(alias: string): Promise<string | undefined> {
        return this.firestore
            .collection(this.tokenAliasCollectionName)
            .where("alias", "==", alias.toUpperCase())
            .get()
            .then((snapshot: QuerySnapshot) => {
                if (snapshot.empty) {
                    return undefined;
                }

                const tokenAlias = snapshot.docs[0].data();
                return tokenAlias.token;
            });
    }

    async delete(alias: string): Promise<void> {
        return this.firestore
            .collection(this.tokenAliasCollectionName)
            .where("alias", "==", alias)
            .get()
            .then((snapshot: QuerySnapshot) => {
                if (snapshot.empty) {
                    return undefined;
                }

                const id = snapshot.docs[0].id;
                return this.firestore
                    .collection(this.tokenAliasCollectionName)
                    .doc(id)
                    .delete()
                    .then(() => Promise.resolve());
            });
    }
}
