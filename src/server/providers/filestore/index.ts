import superagent from "superagent";
import { inject } from "inversify";
import { IAttachmentProvider } from "../attachment";
import { logProviderSymbol, tokenProviderSymbol } from "../../symbols";
import { ILogProvider } from "../log";
import { ITokenProvider } from "..";
import { BaseController } from "../../controllers/base";
import { ENV, TRIPETTO_FILESTORE_URL } from "../../settings";
import { HEADER_ATTACHMENT_TOKEN } from "../../headers";

export class FilestoreProvider extends BaseController implements IAttachmentProvider {
    private logProvider: ILogProvider;

    constructor(@inject(tokenProviderSymbol) tokenProvider: ITokenProvider, @inject(logProviderSymbol) logProvider: ILogProvider) {
        super(tokenProvider);

        this.logProvider = logProvider;
    }

    async delete(name: string, user: string, definition: string): Promise<void> {
        const token = this.tokenProvider.generateAttachmentToken(name, user);

        return superagent
            .delete(TRIPETTO_FILESTORE_URL)
            .set(HEADER_ATTACHMENT_TOKEN, token)
            .then(
                (res: superagent.Response) => {
                    if (ENV === "development") {
                        console.log(token);
                    }
                },
                (error: Error) => {
                    this.logProvider.error(error);
                }
            );
    }

    async confirm(name: string, user: string, definition: string): Promise<void> {
        const token = this.tokenProvider.generateAttachmentToken(name, user);

        return superagent
            .patch(TRIPETTO_FILESTORE_URL)
            .set(HEADER_ATTACHMENT_TOKEN, token)
            .then(
                (res: superagent.Response) => {
                    if (res.status !== 200) {
                        this.logProvider.error(
                            `Received status ${res.status} while confirming attachment ${name} for user ${user} and definition ${definition}.`
                        );
                    }
                },
                (error: Error) => {
                    this.logProvider.error(error);
                }
            );
    }
}
