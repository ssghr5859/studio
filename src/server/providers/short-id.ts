/** Describes a short id provider. */
export interface IShortIdProvider {
    /** Generate a short id. */
    readonly generate: () => string;
}
