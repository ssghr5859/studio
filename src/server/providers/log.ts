/** Provider for logging messages. */
export interface ILogProvider {
    /* Log a debug message. */
    debug(data: {}): Promise<void>;
    /* Log an info message. */
    info(data: {}, logName?: string): Promise<void>;
    /* Log a warning message. */
    warning(data: {}): Promise<void>;
    /* Log an error message. */
    error(data: {}): Promise<void>;
    /* Log a critical message. */
    critical(data: {}): Promise<void>;
}
