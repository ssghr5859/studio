import * as jsonwebtoken from "jsonwebtoken";
import { ISignOptions, ITokenProvider } from "../token";
import { injectable } from "inversify";
import { TOKEN_AUTH_EXPIRES_IN, TOKEN_FILE_EXPIRES_IN, TOKEN_LOGIN_EXPIRES_IN, TRIPETTO_APP_PK } from "../../settings";
import { IAccessToken, IAttachmentToken, IAuthenticationToken, IRunnerToken, ITemplateToken } from "../../entities/tokens/interface";

@injectable()
export class JwtTokenProvider implements ITokenProvider {
    generate(payload: {}, signOptions: ISignOptions): string {
        const options: jsonwebtoken.SignOptions = signOptions;
        return jsonwebtoken.sign(payload, TRIPETTO_APP_PK, options);
    }

    generateAccessToken(publicKey: string): string {
        const payload: IAccessToken = {
            publicKey,
            type: "access",
        };
        return jsonwebtoken.sign(payload, TRIPETTO_APP_PK, { expiresIn: TOKEN_LOGIN_EXPIRES_IN });
    }

    generateAuthenticationToken(user: string): string {
        const payload: IAuthenticationToken = {
            user,
            type: "auth",
        };
        return jsonwebtoken.sign(payload, TRIPETTO_APP_PK, { expiresIn: TOKEN_AUTH_EXPIRES_IN });
    }

    generateRunnerToken(userPublicKey: string, definitionPublicKey: string, snapshot?: string): string {
        const payload: IRunnerToken = {
            user: userPublicKey,
            definition: definitionPublicKey,
            type: "collect",
            snapshot,
        };
        return jsonwebtoken.sign(payload, TRIPETTO_APP_PK, { noTimestamp: true });
    }

    generateTemplateToken(userPublicKey: string, definitionPublicKey: string): string {
        const payload: ITemplateToken = {
            user: userPublicKey,
            definition: definitionPublicKey,
            type: "template",
        };
        return jsonwebtoken.sign(payload, TRIPETTO_APP_PK, { noTimestamp: true });
    }

    generateAttachmentToken(fileId: string, user: string): string {
        const payload: IAttachmentToken = {
            name: fileId,
            user,
            type: "attachment",
        };
        return jsonwebtoken.sign(payload, TRIPETTO_APP_PK, { expiresIn: TOKEN_FILE_EXPIRES_IN });
    }
}
