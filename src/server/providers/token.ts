/** Describes a token provider. */
export interface ITokenProvider {
    /** Generates a token. */
    generate: (payload: {}, options: ISignOptions) => string;

    /** Generates access token. */
    generateAccessToken: (publicKey: string) => string;

    /** Generates authentication token. */
    generateAuthenticationToken: (user: string) => string;

    /** Generates template token. */
    generateTemplateToken: (userPublicKey: string, definitionPublicKey: string) => string;

    /** Generates runner token. */
    generateRunnerToken: (userPublicKey: string, definitionPublicKey: string, snapshot?: string) => string;

    /** Generates attachment token. */
    generateAttachmentToken: (fileId: string, user: string) => string;
}

/** Describes the sign options. */
export interface ISignOptions {
    /** Describes how soon the token expires. */
    expiresIn?: string | number;
    /** Describes whether or not to leave out the timestamp. */
    noTimestamp?: boolean;
}
