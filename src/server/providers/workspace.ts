import { IWorkspace, IWorkspaceInput } from "../entities/workspaces/interface";

/** Describes the workspace provider. */
export interface IWorkspaceProvider {
    /** Create a workspace. */
    readonly create: (userId: string) => Promise<string | undefined>;

    /** Read a workspace. */
    readonly read: (userId: string, workspaceId: string, includeData: boolean) => Promise<IWorkspace | undefined>;

    /** Read the root workspace. */
    readonly readRoot: (userId: string, includeData: boolean) => Promise<IWorkspace | undefined>;

    /** Update a workspace. */
    readonly update: (userId: string, workspace: IWorkspaceInput) => Promise<void>;

    /** Delete a workspace. */
    readonly delete: (userId: string, workspaceId: string) => Promise<void>;
}
