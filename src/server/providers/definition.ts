import { IHookSettings, IServerDefinition, IServerDefinitionInput, ITrackers } from "../entities/definitions/interface";
import { TRunners } from "../entities/definitions/runners";
import { TL10n, TStyles } from "tripetto-runner-foundation";

/** Describes the definition provider. */
export interface IDefinitionProvider {
    /** Create a definition. */
    readonly create: (userId: string, runner: TRunners) => Promise<IServerDefinition | undefined>;

    /** Read a definition by id. */
    readonly readById: (userId: string, definitionId: string, includeData: boolean) => Promise<IServerDefinition | undefined>;

    /** Read a definition by public key. */
    readonly readByPublicKey: (
        userPublicKey: string,
        definitionPublicKey: string,
        includeData: boolean
    ) => Promise<IServerDefinition | undefined>;

    /** List all definitions of a user. */
    readonly list: (userId: string) => Promise<string[] | undefined>;

    /** Update a definition. */
    readonly update: (userId: string, definition: IServerDefinitionInput) => Promise<void | undefined>;

    /** Update the runner type. */
    readonly updateRunner: (userId: string, definitionId: string, runner: TRunners) => Promise<void | undefined>;

    /** Update the styles. */
    readonly updateStyles: (userId: string, definitionId: string, styles: TStyles) => Promise<void | undefined>;

    /** Update the l10n settings. */
    readonly updateL10n: (userId: string, definitionId: string, l10n: TL10n) => Promise<void | undefined>;

    /** Update the hooks. */
    readonly updateHooks: (userId: string, definitionId: string, hooks: IHookSettings) => Promise<void | undefined>;

    /** Update the trackers. */
    readonly updateTrackers: (userId: string, definitionId: string, trackers: ITrackers) => Promise<void | undefined>;

    /** Update a token alias. */
    readonly updateReadTokenAlias: (userId: string, definitionId: string, tokenAlias: string) => Promise<void | undefined>;

    /** Update a template token alias. */
    readonly updateTemplateTokenAlias: (userId: string, definitionId: string, tokenAlias: string) => Promise<void | undefined>;

    /** Delete a definition. */
    readonly delete: (userId: string, id: string) => Promise<void | undefined>;
}
