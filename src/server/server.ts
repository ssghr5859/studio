import Express from "express";
import helmet from "helmet";
import CookieParser from "cookie-parser";
import { IController } from "./controllers";
import { ILogProvider } from "./providers";
import { randomBytes } from "crypto";
import { PACKAGE_TITLE, PACKAGE_VERSION } from "./package";
import { ENV, MAXIMUM_REQUEST_SIZE, URL } from "./settings";
import { RUN } from "./endpoints";

export class Server {
    private readonly errorViewPath = "pages/error";
    private logProvider: ILogProvider;
    public server: Express.Application;
    public port: number;

    static start(controllers: IController[], logProvider: ILogProvider, port?: string): void {
        const server = new Server(controllers, (port && parseInt(port, 10)) || 8080, logProvider);

        server.listen();
    }

    private constructor(controllers: IController[], port: number, logProvider: ILogProvider) {
        this.server = Express();
        this.port = port;
        this.logProvider = logProvider;

        this.initializeMiddlewares();
        this.initializeControllers(controllers);

        this.server.get("/ping", (req: Express.Request, res: Express.Response) => {
            return res.sendStatus(200);
        });

        this.server.use(
            (
                err: { name: string; message?: string; stack?: string; status?: number },
                req: Express.Request,
                res: Express.Response,
                next: Express.NextFunction
            ) => {
                switch (err.name) {
                    case "UnauthorizedError":
                        res.status(401);
                        res.render(`${this.errorViewPath}/unauthorized`);

                        break;
                    default:
                        this.logProvider.error({ name: err.name, message: err.message, stack: err.stack, status: err.status });

                        res.status(500);
                        res.render(`${this.errorViewPath}/server`);

                        break;
                }
            }
        );

        this.server.use((request: Express.Request, response: Express.Response) => {
            response.status(404);
            response.render(`${this.errorViewPath}/not-found`);
        });
    }

    private initializeMiddlewares(): void {
        this.server.use(
            helmet({
                frameguard: false,
                crossOriginEmbedderPolicy: false,
                crossOriginOpenerPolicy: true,
                crossOriginResourcePolicy: false,
                contentSecurityPolicy: {
                    useDefaults: false,
                    directives: {
                        defaultSrc: ["'self'"],
                        baseUri: ["'self'"],
                        blockAllMixedContent: [],
                        fontSrc: ["'self'", "https:", "data:"],
                        imgSrc: ["'self'", "https:", "data:"],
                        mediaSrc: ["'self'", "https:", "https:"],
                        frameSrc: ["'self'", "tripetto.app", "tripetto.com", "*.youtube.com", "*.youtube-nocookie.com", "*.vimeo.com"],
                        frameAncestors: [
                            (req: Express.Request) =>
                                req.path.indexOf(RUN + "/") === 0 || req.path.indexOf("/collect/") === 0 ? "https: http:" : "'none'",
                        ],
                        scriptSrc: [
                            "'unsafe-inline'",
                            "https:",
                            (req: Express.Request, res: Express.Response) =>
                                `'nonce-${(res.locals.cspNonce = randomBytes(16).toString("hex"))}'`,
                            "'strict-dynamic'",
                            ENV === "development" ? "localhost:35729" : "",
                        ],
                        scriptSrcAttr: ["'none'"],
                        connectSrc: [
                            "'self'",
                            "https://*.tripetto.app",
                            "https://www.google-analytics.com",
                            "https://*.ingest.sentry.io",
                            ENV === "development" ? "ws:" : "",
                            ENV === "development" ? "http://localhost:8001" : "",
                        ],
                        objectSrc: ["'none'"],
                        styleSrc: ["'self'", "'unsafe-inline'", "https:"],
                        upgradeInsecureRequests: [],
                        ...(ENV !== "development" && {
                            requireTrustedTypesFor: ["'script'"],
                            trustedTypes: [
                                "tripetto",
                                "tripetto#loader",
                                "tripetto#runner",
                                "dompurify",
                                "goog#html",
                                "default",
                                "'allow-duplicates'",
                            ],
                        }),
                    },
                    reportOnly: false,
                },
            })
        );

        this.server.use(CookieParser());
        this.server.use(Express.json({ limit: MAXIMUM_REQUEST_SIZE }));
        this.server.use(Express.static("static"));
        this.server.set("view engine", "ejs");
        this.server.set("trust proxy", true); // See https://expressjs.com/en/guide/behind-proxies.html
    }

    private initializeControllers(controllers: IController[]): void {
        controllers.forEach((controller: IController) => {
            this.server.use("/", controller.router);
        });
    }

    public listen(): void {
        this.server.listen(this.port, () => {
            console.log(`${PACKAGE_TITLE} v${PACKAGE_VERSION} listening on ${URL} (${ENV})`);
        });
    }
}
