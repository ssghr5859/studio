import { IAuthenticationToken } from "../../entities/tokens/interface";

export const authChecker = ({ context: { token } }: { context: { token: IAuthenticationToken } }): boolean => {
    if (!token) {
        return false;
    }

    return true;
};
