import express from "express";
import jwt from "express-jwt";
import ms from "ms";
import { ENV, TOKEN_AUTH_EXPIRES_IN, TOKEN_COOKIE_NAME, TOKEN_REFRESH_INTERVAL_IN_SECONDS, TRIPETTO_APP_PK } from "../settings";
import { inject, injectable } from "inversify";
import { tokenProviderSymbol } from "../symbols";
import { ITokenProvider } from "../providers/token";
import { ITokenRequest, IUserTokenRequest } from "./controller";
import { IAuthenticationToken } from "../entities/tokens/interface";
import { TokenTypes } from "../entities/tokens/type";

@injectable()
export abstract class BaseController {
    protected tokenProvider: ITokenProvider;

    constructor(@inject(tokenProviderSymbol) tokenProvider: ITokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    private authenticate(credentialsRequired: boolean, getToken: jwt.GetTokenCallback): express.RequestHandler {
        return jwt({
            algorithms: ["HS256"],
            secret: TRIPETTO_APP_PK,
            getToken,
            userProperty: "token",
            credentialsRequired,
        });
    }

    private getTokenAgeInSeconds(token: IAuthenticationToken | undefined): number | undefined {
        if (token && token.iat) {
            const now = Math.round(new Date().getTime() / 1000);
            return now - token.iat;
        }
        return undefined;
    }

    protected verifyTokenInHeader(name: string, credentialsRequired: boolean): express.RequestHandler {
        return this.authenticate(credentialsRequired, (request: express.Request) => request.header(name));
    }

    protected verifyTokenInQueryString(credentialsRequired: boolean): express.RequestHandler {
        return this.authenticate(credentialsRequired, (request: express.Request) => request.query.token);
    }

    protected verifyTokenType(expectedType: TokenTypes, credentialsRequired: boolean): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const request = req as ITokenRequest;
            if ((!request.token && credentialsRequired) || (request.token && request.token.type !== expectedType)) {
                res.sendStatus(401);
            }
            next();
        };
    }

    protected authenticateByCookie(credentialsRequired: boolean): express.RequestHandler {
        return this.authenticate(credentialsRequired, (request: express.Request) => request.cookies.token);
    }

    protected refreshToken(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const token = (req as IUserTokenRequest).token;
            const tokenAgeInSeconds = this.getTokenAgeInSeconds(token);
            if (tokenAgeInSeconds && tokenAgeInSeconds > TOKEN_REFRESH_INTERVAL_IN_SECONDS) {
                const refreshedToken = this.createAuthenticationToken(token.user);
                this.storeTokenInCookie(res, refreshedToken);
                console.log(`Token for user ${token.user} is refreshed ${tokenAgeInSeconds}s. after it was issued.`);
            }
            next();
        };
    }

    protected createAuthenticationToken(userId: string): string {
        return this.tokenProvider.generateAuthenticationToken(userId);
    }

    protected storeTokenInCookie(response: express.Response, token: string): void {
        response.cookie(TOKEN_COOKIE_NAME, token, {
            httpOnly: true,
            secure: this.inDevelopmentMode() === false,
            maxAge: ms(TOKEN_AUTH_EXPIRES_IN),
        });
    }

    protected inDevelopmentMode(): boolean {
        return ENV === "development";
    }
}
