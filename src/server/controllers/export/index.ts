import express from "express";
import { IController, IUserTokenRequest } from "../controller";
import { Stringifier, stringify } from "csv-stringify";
import { Export } from "tripetto-runner-foundation";
import { BaseController } from "../base";
import { responseProviderSymbol, tokenProviderSymbol } from "../../symbols";
import { inject, injectable } from "inversify";
import { IResponseProvider, ITokenProvider } from "../../providers";
import { IResponse } from "../../entities/responses";
import { EXPORT } from "../../endpoints";
import { attachmentUrl, isAttachment } from "../../helpers/attachment";
import { getLocale, localeIdentifier } from "../../helpers/locales";
import { formatDate, isDate } from "../../helpers/date";

@injectable()
export class ExportController extends BaseController implements IController {
    private readonly responseProvider: IResponseProvider;
    private readonly metaFieldHeaders = ["Date submitted", "Identification"];
    public router = express.Router();

    constructor(
        @inject(tokenProviderSymbol) tokenProvider: ITokenProvider,
        @inject(responseProviderSymbol) responseProvider: IResponseProvider
    ) {
        super(tokenProvider);
        this.responseProvider = responseProvider;

        this.router.get(`${EXPORT}/:definition/:stencil?`, this.authenticateByCookie(true), this.exportToCSV);
    }

    private get exportToCSV(): express.RequestHandler {
        return async (req: express.Request, res: express.Response) => {
            const userRequest = req as IUserTokenRequest;
            if (!userRequest.token) {
                res.send(401);
                return;
            }
            const definitionId = req.params.definition;
            if (!definitionId) {
                res.send(404);
                return;
            }

            const locale = localeIdentifier(req.header("Accept-Language"));
            const stencil = req.params.stencil;
            const start = Date.now();

            this.prepareCsvResponse(definitionId, stencil, res);

            const csvStringifier = this.createStringifier(res);

            const userId = userRequest.token.user;
            let responses = await this.getResponses(userId, definitionId, stencil);

            if (!responses || responses.length === 0) {
                csvStringifier.write(["There is no data"]);
                csvStringifier.end();
                return;
            }

            // Sort from new to old.
            responses = responses.sort((a: IResponse, b: IResponse) => b.created - a.created);
            let currentResponseIndex = 0;

            console.log(`LOG: CSV export in progress for ${responses.length} responses`);

            /* Handling responses per batch size helps fetching responses in parallel from repository and
             * prevents loading all responses in memory before writing to csv.
             */
            const batchSize = 100;
            let batchIndex = 1;

            while (responses.length) {
                const batch = responses.splice(0, batchSize);
                const promises = batch.map((response: IResponse) => this.responseProvider.read(userId, definitionId, response.id, true));

                const startTimeBatch = Date.now();
                const responsesWithData = await Promise.all(promises);
                const resolveTimeBatch = Date.now() - startTimeBatch;

                console.log(`LOG: Fetching batch #${batchIndex} took ${resolveTimeBatch} ms.`);

                for (const responseWithData of responsesWithData) {
                    if (responseWithData && responseWithData.data) {
                        const responseFields = JSON.parse(responseWithData.data) as Export.IExportables;

                        if (currentResponseIndex === 0) {
                            let headerLabels = this.metaFieldHeaders;

                            if (responseFields && responseFields.fields) {
                                headerLabels = headerLabels.concat(responseFields.fields.map((field) => field.name));
                            }
                            csvStringifier.write(headerLabels);
                        }

                        const exportedValues = this.getMetaValues(responseWithData, locale);
                        if (responseFields && responseFields.fields) {
                            for (const field of responseFields.fields) {
                                exportedValues.push(
                                    isDate(field)
                                        ? formatDate(field, "short", locale)
                                        : isAttachment(field)
                                        ? attachmentUrl(field)
                                        : field.string
                                );
                            }
                        }
                        csvStringifier.write(exportedValues);
                        currentResponseIndex++;
                    }
                }
                batchIndex++;
            }
            csvStringifier.end();

            const resolveTime = Date.now() - start;
            console.log(`LOG: CSV export took ${resolveTime} ms.`);
        };
    }

    private createStringifier(res: express.Response): Stringifier {
        return stringify({ delimiter: ";", quoted: true })
            .on("error", (err: {}) => console.error(err))
            .on("data", (chunk: {}) => res.write(chunk))
            .on("end", () => res.end());
    }

    /** Returns the responses of a definition and stencil. */
    private async getResponses(userId: string, definitionId: string, stencil?: string): Promise<IResponse[] | undefined> {
        let responses: IResponse[] | undefined;

        if (stencil) {
            responses = await this.responseProvider.readAllByStencil(userId, definitionId, stencil);

            if (!responses || responses.length === 0) {
                responses = await this.responseProvider.readAllByFingerprint(userId, definitionId, stencil);
            }
        } else {
            // Select the (legacy) responses without a fingerprint.
            responses = await this.responseProvider.readAllByDefinition(userId, definitionId, false);
            responses = responses && responses.filter((response: IResponse) => !response.fingerprint);
        }

        return responses;
    }

    private prepareCsvResponse(definitionId: string, stencil: string, response: express.Response): void {
        response.attachment(`tripetto-export-${definitionId}${stencil ? `-${stencil}` : ""}.csv`);
        response.setHeader("Content-Type", "text/csv;charset=utf-8;");

        /* UTF-8 issue;
            Als de header op text/html of application/json gezet wordt en de attachment
            uitgeschakeld, dan wordt de tekst netjes als UTF-8 in de browser getoond.
            De data wordt als UTF-8 opgeslagen in Google Cloud Storage (ook te zien als de ruwe data
            met console.log getoond wordt).

            De combinatie attachment en header Content-Type geeft echter niet het gewenste resultaat :(

            Arrgghh!!

            https://terminaln00b.ghost.io/excel-just-save-it-as-a-csv-noooooooo/
            https://stackoverflow.com/questions/17879198/adding-utf-8-bom-to-string-blob
            https://www.npmjs.com/package/file-saver#syntax

        */

        // Workaround
        response.write("\uFEFF");
    }

    private getMetaValues(response: IResponse, locale: string): string[] {
        return [getLocale(locale).dateTimeShort(response.created, false, locale), response.id];
    }
}
