import { BaseController } from "../base";
import { inject, injectable } from "inversify";
import { IController, IUserTokenRequest } from "../controller";
import { metricProviderSymbol, tokenProviderSymbol } from "../../symbols";
import * as Express from "express";
import { IMetricProvider, ITokenProvider } from "../../providers";
import { METRIC } from "../../endpoints";
import { IMetric, MetricEventType } from "../../providers/metric";

@injectable()
export class MetricController extends BaseController implements IController {
    private metricProvider: IMetricProvider;
    public router = Express.Router();

    constructor(@inject(tokenProviderSymbol) tokenProvider: ITokenProvider, @inject(metricProviderSymbol) metricProvider: IMetricProvider) {
        super(tokenProvider);
        this.metricProvider = metricProvider;

        this.router.post(METRIC, this.authenticateByCookie(false), this.postMetric);
    }

    private get postMetric(): Express.RequestHandler {
        return (req: Express.Request, res: Express.Response) => {
            const event = req.body.event as MetricEventType;
            const subject = req.body.subject;
            const subjectId = req.body.subjectId;
            const userRequest = req as IUserTokenRequest;

            const metric: IMetric = {
                event,
                subject,
                ip: req.ip,
            };
            if (subjectId) {
                metric.subjectId = subjectId;
            }
            if (userRequest.token) {
                metric.userId = userRequest.token.user;
            }
            this.metricProvider.add(metric);
            res.sendStatus(200);
        };
    }
}
