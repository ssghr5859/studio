// @ts-expect-error
import * as PackageData from "../package.json";

export const PACKAGE_NAME = PackageData.name || "";
export const PACKAGE_TITLE = PackageData.title || "";
export const PACKAGE_VERSION = PackageData.version || "";
