const fs = require("fs");
const tripetto = require("tripetto");
const banner = require("./banner.js");
const path = "./services/types/index.d.ts";

let types = fs.readFileSync(path, "utf8");

types = tripetto.Str.replace(types, "// Dependencies for this module:\n", "");
types = tripetto.Str.replace(types, "//   ../../../../tripetto\n", "");
types = tripetto.Str.replace(types, "//   ../../../../tripetto-runner-foundation\n", "");
types = tripetto.Str.replace(types, "/*! BANNER */", `/*! ${banner} */`);

fs.writeFileSync(path, types, "utf8");
