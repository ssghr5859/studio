const fs = require("fs");
const prettier = require("prettier");
const mkdirp = require("mkdirp");
const tripetto = require("tripetto");
const pkg = require("../../package.json");

function combine(inputFolder, outputFolder) {
    if (fs.existsSync(inputFolder)) {
        const files = fs.readdirSync(inputFolder) || [];

        mkdirp.sync(outputFolder);

        files.forEach(function (file) {
            if (fs.statSync(inputFolder + file).isFile()) {
                const isJSON = file.lastIndexOf(".json") === file.length - 5;

                if (isJSON) {
                    const inputFile = JSON.parse(fs.readFileSync(inputFolder + file, "utf8"));

                    if (inputFile && (inputFile[""] || (inputFile.length > 0 && inputFile[0][""]))) {
                        const inputData =
                            typeof inputFile === "object" && "length" in inputFile && inputFile.length > 0 ? inputFile : [inputFile];

                        if (fs.existsSync(outputFolder + file)) {
                            const length = inputData.length;
                            let outputData = JSON.parse(fs.readFileSync(outputFolder + file, "utf8"));
                            let merged = 0;

                            for (let i = 0; i < inputData.length; i++) {
                                if (JSON.stringify(outputData).indexOf(JSON.stringify(inputData[i])) === -1) {
                                    merged++;

                                    if (typeof outputData === "object" && "length" in outputData && outputData.length > 0) {
                                        outputData.push(inputData[i]);
                                    } else {
                                        outputData = [outputData, inputData[i]];
                                    }
                                }
                            }

                            if (merged > 0) {
                                fs.writeFileSync(
                                    outputFolder + file,
                                    prettier.format(JSON.stringify(outputData), {
                                        parser: "json",
                                    }),
                                    "utf8"
                                );

                                console.log(`combine: ${inputFolder + file} -> ${outputFolder + file} (merged ${merged} out of ${length})`);
                            } else {
                                console.log(`combine: ${inputFolder + file} -> ${outputFolder + file} (skipped, already merged)`);
                            }
                        } else {
                            fs.writeFileSync(
                                outputFolder + file,
                                prettier.format(JSON.stringify(inputData.length === 1 ? inputData[0] : inputData), {
                                    parser: "json",
                                }),
                                "utf8"
                            );

                            console.log(`combine: ${inputFolder + file} -> ${outputFolder + file} (created)`);
                        }
                    }
                }
            }
        });
    }
}

combine(`./node_modules/tripetto/translations/`, "./translations/");

tripetto.each(
    tripetto.filter(Object.keys(pkg.devDependencies), (pkg) => /^tripetto-runner-/.test(pkg)),
    (pkg) => {
        combine(`./node_modules/${pkg}/translations/`, "./translations/");
        combine(`./node_modules/${pkg}/builder/translations/`, "./translations/");
    }
);

tripetto.each(
    tripetto.filter(Object.keys(pkg.devDependencies), (pkg) => /^tripetto-runner-/.test(pkg)),
    (pkg) => {
        combine(`./node_modules/${pkg}/runner/translations/`, `./translations/${pkg}/`);
    }
);
